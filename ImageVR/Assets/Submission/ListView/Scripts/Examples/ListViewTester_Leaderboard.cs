﻿namespace Examples
{
	using UnityEngine;
	using UnityEngine.UI;
	using System.Collections;
	using Endgame;

	public class ListViewTester_Leaderboard : MonoBehaviour
	{
		public ListView ListView;
		private const int columnWidthCount = 3;
		private int columnCount
		{
			get
			{
				return ListView.Columns.Count;
			}
		}
		private int[] columnWidths = new int[columnWidthCount];
		private int[] columnWidthStates = null;
		private Button insertItemAtCurrentPositionButton;
		private Button removeItemAtCurrentPositionButton;
		private Button toggleColumnClickModeButton;
		private Button changeItemBackgroundColorButton;
		private Button changeItemTextColorButton;
		private Button changeControlBackgroundColorButton;
		private int itemAddedCount = 0;
		private int itemInsertedCount = 0;
		private bool clickingAColumnSorts = true;

		private Color defaultControlBackgroundColor;
		private Color defaultItemBackgroundColor = new Color(0, 0, 0, 0);
		private Color defaultItemTextColor;
		private Color spotifyGreen = new Color(0.50f, 0.72f, 0.01f);
		private Color spotifyRed = new Color(0.68f, 0.27f, 0.27f);

		public void Start()
		{
			// Get references to the buttons.
			insertItemAtCurrentPositionButton =
				GameObject.Find("/Canvas/Buttons/InsertItemAtCurrentPositionButton").GetComponent<Button>();
			removeItemAtCurrentPositionButton =
				GameObject.Find("/Canvas/Buttons/RemoveItemAtCurrentPositionButton").GetComponent<Button>();
			toggleColumnClickModeButton =
				GameObject.Find("/Canvas/Buttons/ToggleColumnClickModeButton").GetComponent<Button>();
			changeItemBackgroundColorButton =
				GameObject.Find("/Canvas/Buttons/ChangeItemBackgroundColorButton").GetComponent<Button>();
			changeItemTextColorButton =
				GameObject.Find("/Canvas/Buttons/ChangeItemTextColorButton").GetComponent<Button>();
			changeControlBackgroundColorButton =
				GameObject.Find("/Canvas/Buttons/ChangeControlBackgroundColorButton").GetComponent<Button>();

			// Add some test data (columns and items).
			AddTestData();

			// Add some events.
			// (Clicking on the first column header will sort by that column, and
			// clicking on any other column header will change that column's width
			// between default, sized to the header or sized to the longest item.)
			ListView.ColumnClick += OnColumnClick;

			// Initialise an array with some example column widths
			// that will be toggled between by clicking on the column header.
			// (-1 in Windows Forms means size to the longest item, and
			// -2 means size to the column header.)
			columnWidths[0] = 100;
			columnWidths[1] = -1;
			columnWidths[2] = -2;

			for (int index = 0; index < columnCount; index++)
			{
				columnWidthStates[index] = 0;
			}

			ListView.Columns[0].Width = 80;
			ListView.Columns[1].Width = 65;
			ListView.Columns[2].Width = 200;
			ListView.Columns[3].Width = 100;
			ListView.Columns[4].Width = 125;
			ListView.Columns[5].Width = 100;
			ListView.Columns[6].Width = 150;

			defaultControlBackgroundColor = ListView.BackColor;
			defaultItemTextColor = ListView.ForeColor;
		}

		private class ListViewItemComparer : IComparer
		{
			private int columnIndex = 0;

			public ListViewItemComparer()
			{
			}

			public ListViewItemComparer(int columnIndex)
			{
				this.columnIndex = columnIndex;
			}

			public int Compare(object object1, object object2)
			{
				ListViewItem listViewItem1 = object1 as ListViewItem;
				ListViewItem listViewItem2 = object2 as ListViewItem;
				string text1 = listViewItem1.SubItems[columnIndex].Text;
				string text2 = listViewItem2.SubItems[columnIndex].Text;
				return string.Compare(text1, text2);
			}
		}

		private void OnColumnClick(object sender, ListView.ColumnClickEventArgs e)
		{
			if (clickingAColumnSorts)
			{
				ListView listView = (ListView)sender;
				listView.ListViewItemSorter = new ListViewItemComparer(e.Column);
			}
			else
			{
				IncrementColumnWidthState(e.Column);
			}
		}

		private void IncrementColumnWidthState(int columnIndex)
		{
			columnWidthStates[columnIndex]++;
			if (columnWidthStates[columnIndex] >= columnWidthCount)
			{
				columnWidthStates[columnIndex] = 0;
			}

			int columnWidth = columnWidths[columnWidthStates[columnIndex]];
			ListView.Columns[columnIndex].Width = columnWidth;
		}

		public void Update()
		{
			// Some buttons require a selection, so disable them if there is no 
			// selection.
			bool isItemSelected = false;
			if (ListView != null)
			{
				if (ListView.SelectedIndices.Count > 0)
				{
					isItemSelected = true;
				}
			}

			insertItemAtCurrentPositionButton.interactable = isItemSelected;
			removeItemAtCurrentPositionButton.interactable = isItemSelected;
			changeItemBackgroundColorButton.interactable = isItemSelected;
			changeItemTextColorButton.interactable = isItemSelected;
			changeControlBackgroundColorButton.interactable = isItemSelected;
		}

		public void OnAddNewItemButtonClicked()
		{
			itemAddedCount++;
			AddListViewItem(string.Format("aDDed {0}", itemAddedCount));

			// Select the new item and scroll to it.
			ListView.SelectedIndices.Add(ListView.Items.Count - 1);
			ListView.SetVerticalScrollBarValue(1);
		}

		public void OnInsertItemAtCurrentPositionButtonClicked()
		{
			itemInsertedCount++;

			int selectedIndex = ListView.SelectedIndices[0];
			ListView.Items.Insert(selectedIndex, CreateListViewItemFromPlayerName(string.Format("iNSeRTed {0}", itemInsertedCount)));
		}

		public void OnRemoveItemAtCurrentPositionButtonClicked()
		{
			int selectedIndex = ListView.SelectedIndices[0];
			ListView.Items.RemoveAt(selectedIndex);
		}

		public void OnToggleColumnClickModeButtonClicked()
		{
			string text = "";

			if (clickingAColumnSorts)
			{
				clickingAColumnSorts = false;
				text = "Clicking a column header will change its width (click here to change)";
			}
			else
			{
				clickingAColumnSorts = true;
				text = "Clicking a column header will sort (click here to change)";
			}

			toggleColumnClickModeButton.GetComponentInChildren<Text>().text = text;
		}

		public void OnChangeItemBackgroundColorButtonClicked()
		{
			ListViewItem selectedItem = ListView.SelectedItems[0];

			if (selectedItem.BackColor == defaultItemBackgroundColor)
			{
				SetAllSubItemsBackgroundColor(selectedItem, spotifyRed);
			}
			else
			{
				SetAllSubItemsBackgroundColor(selectedItem, defaultItemBackgroundColor);
			}
		}

		public void OnChangeItemTextColorButtonClicked()
		{
			ListViewItem selectedItem = ListView.SelectedItems[0];
			if (selectedItem.ForeColor == defaultItemTextColor)
			{
				SetAllSubItemsTextColor(selectedItem, Color.cyan);
			}
			else
			{
				SetAllSubItemsTextColor(selectedItem, defaultItemTextColor);
			}
		}

		private void SetAllSubItemsBackgroundColor(ListViewItem item, Color color)
		{
			item.BackColor = color;

			foreach (ListViewItem.ListViewSubItem subItem in item.SubItems)
			{
				subItem.BackColor = color;
			}
		}

		private void SetAllSubItemsTextColor(ListViewItem item, Color color)
		{
			item.ForeColor = color;

			foreach (ListViewItem.ListViewSubItem subItem in item.SubItems)
			{
				subItem.ForeColor = color;
			}
		}

		public void OnChangeControlBackgroundColorButtonClicked()
		{
			if (ListView.BackColor == defaultControlBackgroundColor)
			{
				ListView.BackColor = spotifyGreen;
			}
			else
			{
				ListView.BackColor = defaultControlBackgroundColor;
			}
		}

		private void AddTestData()
		{
			if (ListView != null)
			{
				ListView.SuspendLayout();
				{
					AddColumn("RANK");
					AddColumn("LEVEL");
					AddColumn("PLAYER NAME");
					AddColumn("SCORE");
					AddColumn("TIME PLAYED");
					AddColumn("GAMES");
					AddColumn("AVG SCORE");

					columnWidthStates = new int[columnCount];

					AddListViewItem("Griddler");
					AddListViewItem("Kovsa");
					AddListViewItem("GrantTheAnt");
					AddListViewItem("TheKlaus");
					AddListViewItem("RUMble");
					AddListViewItem("Denzel");
					AddListViewItem("sVinX");
					AddListViewItem("tEd");
					AddListViewItem("ErkTic");
					AddListViewItem("RageBurns");
					AddListViewItem("IncredibleOrb");
					AddListViewItem("Skyline Spirit");
					AddListViewItem("eSTeeM");
					AddListViewItem("-=[A!M]bob");
					AddListViewItem("Shells");
					AddListViewItem("[sTyLER]");
					AddListViewItem("Piet");
					AddListViewItem("Innkvart");
					AddListViewItem("nilde");
					AddListViewItem("Boba Fett");
					AddListViewItem("Timaloy");
					AddListViewItem("eatbrain");
					AddListViewItem("TOMMA:-D");
				}
				ListView.ResumeLayout();
			}
		}

		private void AddColumn(string title)
		{
			bool odd = (ListView.Columns.Count & 1) > 0;

			Color headingColour1 = new Color(0.09f, 0.13f, 0.14f);
			Color headingColour2 = new Color(0.13f, 0.19f, 0.20f);

			ColumnHeader column = new ColumnHeader();
			column.BackColor = odd ? headingColour1 : headingColour2;
			column.Text = title;
			ListView.Columns.Add(column);
		}

		private int previousScore = 301540;
		private int previousRank = 1;

		private ListViewItem CreateListViewItemFromPlayerName(string playerName)
		{
			int level = Random.Range(30, 40);
			int score = previousScore;
			int timePlayedInMins = Random.Range(400, 800);
			int games = Random.Range(85, 120);
			int averageScore = score / games;
			int rank = previousRank;

			previousScore -= Random.Range(1, 1000);
			previousRank++;

			int timePlayedInHours = timePlayedInMins / 60;
			int timePlayedInMinsMod = timePlayedInMins % 60;

			string timePlayedString = string.Format("{0:00}h {1:00}m", timePlayedInHours, timePlayedInMinsMod);

			string[] subItemTexts = new string[]
		{
			rank.ToString(),
			level.ToString(),
			playerName,
			score.ToString(),
			timePlayedString,
			games.ToString(),
			averageScore.ToString()
		};

			ListViewItem item = new ListViewItem(subItemTexts);
			return item;
		}

		private void AddListViewItem(string playerName)
		{
			ListView.Items.Add(CreateListViewItemFromPlayerName(playerName));
		}
	}
}
