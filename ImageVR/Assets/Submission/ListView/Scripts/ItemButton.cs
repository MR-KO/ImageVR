﻿namespace Endgame
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using UnityEngine;
	using UnityEngine.UI;
	using UnityEngine.EventSystems;

	public class ItemButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
	{
		public Image BackgroundImage;
		public Image HorizontalBorderImage;
		public Image VerticalBorderImage;
		public Text Text;
		public Image Image;
		public RectTransform CustomControlParent;
		private int margin;
		public int Margin { get { return margin; } }

		public void Awake()
		{
			margin = (int)Text.rectTransform.offsetMin.x;
		}

		public ListViewItem ListViewItem
		{
			get;
			set;
		}

		public ListViewItem.ListViewSubItem ListViewSubItem
		{
			get;
			set;
		}

		private int horizontalGridLineSize = 0;
		public int HorizontalGridLineSize
		{
			get
			{
				return horizontalGridLineSize;
			}

			set
			{
				horizontalGridLineSize = value;

				if (horizontalGridLineSize > 0)
				{
					HorizontalBorderImage.gameObject.SetActive(true);
					HorizontalBorderImage.transform.localScale = new Vector3(HorizontalBorderImage.transform.localScale.x, horizontalGridLineSize, HorizontalBorderImage.transform.localScale.z);
				}
				else
				{
					HorizontalBorderImage.gameObject.SetActive(false);
				}
			}
		}

		private int verticalGridLineSize = 0;
		public int VerticalGridLineSize
		{
			get
			{
				return verticalGridLineSize;
			}

			set
			{
				verticalGridLineSize = value;

				if (verticalGridLineSize > 0)
				{
					VerticalBorderImage.gameObject.SetActive(true);

					VerticalBorderImage.transform.localScale = new Vector3(-verticalGridLineSize, VerticalBorderImage.transform.localScale.y, VerticalBorderImage.transform.localScale.z);
				}
				else
				{
					VerticalBorderImage.gameObject.SetActive(false);
				}
			}
		}

		public Color GridLineColor
		{
			get
			{
				return VerticalBorderImage.color;
			}

			set
			{
				VerticalBorderImage.color = value;
				HorizontalBorderImage.color = value;
			}
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			if (ListViewItem != null)
			{
				ListViewItem.Owner.OnItemMouseEnter(this);
			}
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			if (ListViewItem != null)
			{
				ListViewItem.Owner.OnItemMouseExit(this);
			}
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			if (ListViewItem != null)
			{
				ListViewItem.Owner.OnSubItemClicked(eventData, this);
			}
		}
	}
}
