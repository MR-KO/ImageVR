﻿namespace Endgame
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using UnityEngine;
	using UnityEngine.UI;

	public class ColumnPanel : MonoBehaviour
	{
		public ColumnHeader ColumnHeader
		{
			get;
			set;
		}

		public Button Button;
		public Text Text
		{
			get
			{
				return Button.GetComponentInChildren<Text>();
			}
		}
		public Image HorizontalBorderImage;
		public Image VerticalBorderImage;

		private int horizontalGridLineSize = 0;
		public int HorizontalGridLineSize
		{
			get
			{
				return horizontalGridLineSize;
			}

			set
			{
				horizontalGridLineSize = value;

				if (horizontalGridLineSize > 0)
				{
					HorizontalBorderImage.gameObject.SetActive(true);
					HorizontalBorderImage.transform.localScale = new Vector3(HorizontalBorderImage.transform.localScale.x, horizontalGridLineSize, HorizontalBorderImage.transform.localScale.z);
				}
				else
				{
					HorizontalBorderImage.gameObject.SetActive(false);
				}
			}
		}

		private int verticalGridLineSize = 0;
		public int VerticalGridLineSize
		{
			get
			{
				return verticalGridLineSize;
			}

			set
			{
				verticalGridLineSize = value;

				if (verticalGridLineSize > 0)
				{
					VerticalBorderImage.gameObject.SetActive(true);

					VerticalBorderImage.transform.localScale = new Vector3(-verticalGridLineSize, VerticalBorderImage.transform.localScale.y, VerticalBorderImage.transform.localScale.z);
				}
				else
				{
					VerticalBorderImage.gameObject.SetActive(false);
				}
			}
		}

		public Color GridLineColor
		{
			get
			{
				return VerticalBorderImage.color;
			}

			set
			{
				VerticalBorderImage.color = value;
				HorizontalBorderImage.color = value;
			}
		}

		public ItemPanel ItemPanel;

		public void Reset()
		{
			RectTransform items = ItemPanel.Items;
			items.sizeDelta = new Vector2(items.sizeDelta.x, 0);
		}
	}
}
