﻿namespace Endgame
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Runtime;
	using System.Runtime.Serialization;
	using System.Linq;
	using UnityEngine;

	public class ListViewItem : ICloneable/*, ISerializable*/
	{
		private ListView owner;
		private bool selected;
		private ListViewSubItemCollection subItems;
		public ListView Owner
		{
			get
			{
				return owner;
			}
		}

		public ListViewItem()
		{
			subItems = new ListViewSubItemCollection(this);
			subItems.Add("");
		}

		public ListViewItem(string text)
		{
			subItems = new ListViewSubItemCollection(this);
			subItems.Add("");
			Text = text;
		}

		public ListViewItem(string[] items)
		{
			subItems = new ListViewSubItemCollection(this);
			if (items.Length > 0)
			{
				subItems.AddRange(items);
			}
		}

		//public Font Font { get; set; }
		public int Index
		{
			get
			{
				int result = -1;
				if (ListView != null)
				{
					result = ListView.Items.IndexOf(this);
				}
				return result;
			}
		}

		public ListView ListView
		{
			get
			{
				return owner;
			}

			set
			{
				owner = value;
			}
		}

		public bool Selected
		{
			get
			{
				return selected;
			}

			set
			{
				selected = value;
				if (owner != null)
				{
					if (value)
					{
						owner.SelectedIndices.Add(Index);
					}
					else
					{
						owner.SelectedIndices.Remove(Index);
					}
				}
			}
		}

		public ListViewItem.ListViewSubItemCollection SubItems
		{
			get
			{
				return subItems;
			}
		}

		private string name;
		private object tag;

		public string Name
		{
			get
			{
				return name;
			}

			set
			{
				name = value;
				OnModified();
			}
		}

		public object Tag
		{
			get
			{
				return tag;
			}

			set
			{
				tag = value;
				OnModified();
			}
		}

		public string Text
		{
			get
			{
				return subItems[0].Text;
			}

			set
			{
				subItems[0].Text = value;
			}
		}

		public Color ForeColor
		{
			get
			{
				return subItems[0].ForeColor;
			}

			set
			{
				subItems[0].ForeColor = value;
			}
		}

		public Color BackColor
		{
			get
			{
				return subItems[0].BackColor;
			}

			set
			{
				subItems[0].BackColor = value;
			}
		}

		public Font Font
		{
			get
			{
				return subItems[0].Font;
			}

			set
			{
				subItems[0].Font = value;
			}
		}

		public int FontSize
		{
			get
			{
				return subItems[0].FontSize;
			}

			set
			{
				subItems[0].FontSize = value;
			}
		}

		public FontStyle FontStyle
		{
			get
			{
				return subItems[0].FontStyle;
			}

			set
			{
				subItems[0].FontStyle = value;
			}
		}

		private bool useItemStyleForSubItems = true;
		public bool UseItemStyleForSubItems
		{
			get
			{
				return useItemStyleForSubItems;
			}

			set
			{
				useItemStyleForSubItems = value;
				if (owner != null)
				{
					owner.RebuildHierarchy();
				}
			}
		}

		public string ImageKey
		{
			get
			{
				return subItems[0].ImageKey;
			}

			set
			{
				subItems[0].ImageKey = value;
			}
		}

		public int ImageIndex
		{
			get
			{
				return subItems[0].ImageIndex;
			}

			set
			{
				subItems[0].ImageIndex = value;
			}
		}

		public ImageList ImageList
		{
			get
			{
				return owner.SmallImageList;
			}
		}

		public virtual void Remove()
		{
			if (ListView != null)
			{
				ListView.Items.Remove(this);
			}
		}

		public void OnModified()
		{
			if (ListView != null)
			{
				ListView.UpdateItem(this);
			}
		}

		public object Clone()
		{
			ListViewItem clone = new ListViewItem();

			clone.owner = owner;
			clone.selected = selected;

			foreach (ListViewSubItem subItem in subItems)
			{
				ListViewSubItem newItem = new ListViewSubItem();
				newItem.ListViewItem = subItem.ListViewItem;
				newItem.Name = subItem.Name;
				newItem.Tag = subItem.Tag;
				newItem.Text = subItem.Text;
				clone.subItems.Add(newItem);
			}

			return clone;
		}

		public ItemButton ItemButtonInHierarchy
		{
			get;
			set;
		}

		public void SetSelectedFlagInternal(bool value)
		{
			selected = value;
		}

		public RectTransform CustomControl
		{
			get
			{
				return SubItems[0].CustomControl;
			}

			set
			{
				SubItems[0].CustomControl = value;
			}
		}

		public class ListViewSubItem
		{
			private ListViewItem owner;

			public ListViewSubItem()
			{
			}
			public ListViewSubItem(ListViewItem owner, string text)
			{
				this.owner = owner;
				this.text = text;
			}

			private Font font;
			public Font Font
			{
				get
				{
					Font font = this.font;
					if (font == null)
					{
						if (owner != null)
						{
							if (owner.owner != null)
							{
								font = owner.owner.DefaultItemFont;
							}
						}
					}

					return font;
				}
				set
				{
					font = value;
					OnModified();
				}
			}
			private string name;
			private object tag;
			private string text;

			private void OnModified()
			{
				if (owner != null)
				{
					owner.OnModified();
				}
			}

			public string Name
			{
				get
				{
					return name;
				}

				set
				{
					name = value;
					OnModified();
				}
			}

			public object Tag
			{
				get
				{
					return tag;
				}

				set
				{
					tag = value;
					OnModified();
				}
			}

			public string Text
			{
				get
				{
					return text;
				}

				set
				{
					text = value;
					OnModified();
				}
			}

			private Color foreColor = Color.black;
			internal bool hasForeColorBeenSet = false;
			public Color ForeColor
			{
				get
				{
					// Only use the subitem's fore colour if the user has set it 
					// explicitly. If it is still at the default value, use the 
					// control-wide fore colour instead. (This is how Windows Forms behaves.)
					ListViewSubItem subItem = this;
					if (owner.UseItemStyleForSubItems)
					{
						subItem = owner.subItems[0];
					}

					Color foreColor = subItem.foreColor;
					if (!subItem.hasForeColorBeenSet)
					{
						foreColor = owner.owner.ForeColor;
					}

					return foreColor;
				}

				set
				{
					foreColor = value;
					hasForeColorBeenSet = true;
					OnModified();
				}
			}

			private Color backColor = Color.white;
			internal bool hasBackColorBeenSet = false;
			public Color BackColor
			{
				get
				{
					// For subitem back colours, Windows Forms uses the
					// subitem's colour if it has been set, or failing that,
					// uses the ListView's back colour.
					// However Windows Forms does not support colours with alpha.
					// Since this control supports alpha colours, it makes
					// more sense to return a colour with alpha 0 in the
					// case where no subitem back colour has been set.
					// This will mean that for subitems with no back colour
					// set, the ListView's back colour will be seen.
					ListViewSubItem subItem = this;
					if (owner.UseItemStyleForSubItems)
					{
						subItem = owner.subItems[0];
					}

					Color backColor = subItem.backColor;
					if (!subItem.hasBackColorBeenSet)
					{
						//backColor = new Color(0, 0, 0, 0);
						backColor = owner.owner.DefaultItemBackgroundColor;
					}

					return backColor;
				}

				set
				{
					backColor = value;
					hasBackColorBeenSet = true;
					OnModified();
				}
			}

			private int fontSize = -1;
			public int FontSize
			{
				get
				{
					int fontSize = this.fontSize;
					if (fontSize == -1)
					{
						if (owner != null)
						{
							if (owner.owner != null)
							{
								fontSize = owner.owner.DefaultItemFontSize;
							}
						}
					}

					return fontSize;
				}

				set
				{
					fontSize = value;
					OnModified();
				}
			}

			private string imageKey = "";
			public string ImageKey
			{
				get
				{
					return imageKey;
				}

				set
				{
					imageKey = value;
					imageIndex = -1;
					OnModified();
				}
			}

			private int imageIndex = -1;
			public int ImageIndex
			{
				get
				{
					return imageIndex;
				}

				set
				{
					imageIndex = value;
					imageKey = "";
					OnModified();
				}
			}

			private bool hasFontStyleBeenSet = false;
			private FontStyle fontStyle;
			public FontStyle FontStyle
			{
				get
				{
					FontStyle fontStyle = this.fontStyle;
					if (!hasFontStyleBeenSet)
					{
						if (owner != null)
						{
							if (owner.owner != null)
							{
								fontStyle = owner.owner.DefaultItemFontStyle;
							}
						}
					}

					return fontStyle;
				}
				set
				{
					fontStyle = value;
					hasFontStyleBeenSet = true;
					OnModified();
				}
			}

			public ListViewItem ListViewItem
			{
				get
				{
					return owner;
				}

				set
				{
					owner = value;
				}
			}

			private RectTransform customControl;
			public RectTransform CustomControl
			{
				get
				{
					return customControl;
				}

				set
				{
					customControl = value;
					OnModified();
				}
			}
		}

		public class ListViewSubItemCollection : IList, ICollection, IEnumerable
		{
			private ListViewItem owner;
			private List<ListViewSubItem> subItems;

			public ListViewSubItemCollection(ListViewItem owner)
			{
				this.owner = owner;
				subItems = new List<ListViewSubItem>();
			}

			public int Count
			{
				get
				{
					return subItems.Count;
				}
			}

			public bool IsReadOnly { get { return false; } }

			public ListViewSubItem this[int index]
			{
				get
				{
					if (index < subItems.Count)
					{
						return subItems[index];
					}
					else
					{
						return new ListViewSubItem(owner, string.Empty);
					}
				}

				set
				{
					if (index < subItems.Count)
					{
						subItems[index] = value;
						ListView owner = this.owner.owner;
						if (owner != null)
						{
							owner.RebuildHierarchy();
						}
					}
				}
			}

			public virtual ListViewSubItem this[string key]
			{
				get
				{
					return subItems.Find(x => x.Name == key);
				}
			}

			private void AddInternal(ListViewSubItem subItem)
			{
				subItems.Add(subItem);
				ListView owner = this.owner.owner;
				if (owner != null)
				{
					owner.RebuildHierarchy();
				}
			}

			public ListViewSubItem Add(ListViewSubItem item)
			{
				AddInternal(item);
				return item;
			}

			public ListViewSubItem Add(string text)
			{
				ListViewSubItem item = new ListViewSubItem(owner, text);
				AddInternal(item);
				return item;
			}

			public void AddRange(ListViewSubItem[] items)
			{
				foreach (var item in items)
				{
					AddInternal(item);
				}
			}

			public void AddRange(string[] items)
			{
				foreach (var item in items)
				{
					Add(item);
				}
			}

			public void Clear()
			{
				subItems.Clear();
				subItems.Add(new ListViewSubItem(this.owner, ""));

				ListView owner = this.owner.owner;
				if (owner != null)
				{
					owner.RebuildHierarchy();
				}
			}

			public bool Contains(ListViewItem.ListViewSubItem subItem)
			{
				return subItems.Contains(subItem);
			}

			public virtual bool ContainsKey(string key)
			{
				return subItems.Any(x => x.Name == key);
			}

			public IEnumerator GetEnumerator()
			{
				return subItems.GetEnumerator();
			}

			public int IndexOf(ListViewItem.ListViewSubItem subItem)
			{
				return subItems.IndexOf(subItem);
			}

			public virtual int IndexOfKey(string key)
			{
				int result = -1;

				foreach (var listViewSubItem in subItems)
				{
					if (listViewSubItem.Name == key)
					{
						result = subItems.IndexOf(listViewSubItem);
					}
				}

				return result;
			}

			public void Insert(int index, ListViewItem.ListViewSubItem item)
			{
				if (index < subItems.Count)
				{
					subItems.Insert(index, item);
					item.ListViewItem = this.owner;

					ListView owner = this.owner.owner;
					if (owner != null)
					{
						owner.RebuildHierarchy();
					}
				}
			}

			private void RemoveInternal(ListViewSubItem subItem)
			{
				subItems.Remove(subItem);
				if (subItems.Count == 0)
				{
					subItems.Add(new ListViewSubItem(this.owner, ""));
				}

				ListView owner = this.owner.owner;
				if (owner != null)
				{
					owner.RebuildHierarchy();
				}
			}

			public void Remove(ListViewItem.ListViewSubItem item)
			{
				RemoveInternal(item);
			}

			public void RemoveAt(int index)
			{
				ListViewSubItem subItem = subItems[index];
				RemoveInternal(subItem);
			}

			public virtual void RemoveByKey(string key)
			{
				ListViewSubItem value = this[key];
				if (value != null)
				{
					RemoveInternal(value);
				}
			}

			public bool IsFixedSize { get { return false; } }

			object IList.this[int index]
			{
				get
				{
					return subItems[index];
				}

				set
				{
					throw new System.NotImplementedException();
				}
			}

			public int Add(object value)
			{
				if (value is ListViewSubItem)
				{
					ListViewSubItem listViewSubItem = (ListViewSubItem)value;
					AddInternal(listViewSubItem);
				}
				return subItems.Count;
			}

			public bool Contains(object value)
			{
				bool result = false;
				if (value is ListViewSubItem)
				{
					ListViewSubItem listViewSubItem = (ListViewSubItem)value;
					result = subItems.Contains(listViewSubItem);
				}
				return result;
			}

			public int IndexOf(object value)
			{
				int result = -1;
				if (value is ListViewSubItem)
				{
					ListViewSubItem listViewSubItem = (ListViewSubItem)value;
					result = subItems.IndexOf(listViewSubItem);
				}
				return result;
			}

			public void Insert(int index, object value)
			{
				if (value is ListViewSubItem)
				{
					ListViewSubItem listViewSubItem = (ListViewSubItem)value;
					Insert(index, listViewSubItem);
				}
			}

			public void Remove(object value)
			{
				if (value is ListViewSubItem)
				{
					ListViewSubItem listViewSubItem = (ListViewSubItem)value;
					RemoveInternal(listViewSubItem);
				}
			}

			public bool IsSynchronized { get { return false; } }

			public object SyncRoot { get { return null; } }

			public void CopyTo(System.Array array, int index)
			{
				int d = 0;
				for (int s = index; s < Count; s++)
				{
					array.SetValue(this[s], d);
					d++;
				}
			}
		}
	}
}
