﻿namespace Endgame
{
	using System;
	using System.Collections;
	using System.Reflection;
	using System.Runtime;
	using System.Runtime.Serialization;
	using UnityEngine;

	public class ColumnHeader : ICloneable
	{
		public const int DefaultWidth = 60;

		public ColumnHeader()
		{
			// Windows Forms seems to set this to 60 by default.
			Width = DefaultWidth;
		}

		public int Index
		{
			get
			{
				int result = -1;
				if (ListView != null)
				{
					result = ListView.Columns.IndexOf(this);
				}
				return result;
			}
		}

		private int width;

		public ListView ListView { get; set; }

		public string Name { get; set; }

		public object Tag { get; set; }

		private string text;
		public string Text
		{
			get
			{
				return text;
			}
			set
			{
				text = value;
				OnModified();
			}
		}

		private Font font;
		public Font Font
		{
			get
			{
				Font font = this.font;
				if (font == null)
				{
					if (ListView != null)
					{
						font = ListView.DefaultHeadingFont;
					}
				}

				return font;
			}
			set
			{
				font = value;
				OnModified();
			}
		}

		private int fontSize = -1;
		public int FontSize
		{
			get
			{
				int fontSize = this.fontSize;
				if (fontSize == -1)
				{
					if (ListView != null)
					{
						if (ListView != null)
						{
							fontSize = ListView.DefaultHeadingFontSize;
						}
					}
				}

				return fontSize;
			}

			set
			{
				fontSize = value;
				OnModified();
			}
		}

		private bool hasFontStyleBeenSet = false;
		private FontStyle fontStyle;
		public FontStyle FontStyle
		{
			get
			{
				FontStyle fontStyle = this.fontStyle;
				if (!hasFontStyleBeenSet)
				{
					if (ListView != null)
					{
						if (ListView != null)
						{
							fontStyle = ListView.DefaultHeadingFontStyle;
						}
					}
				}

				return fontStyle;
			}
			set
			{
				fontStyle = value;
				hasFontStyleBeenSet = true;
				OnModified();
			}
		}

		public int Width
		{
			get
			{
				return width;
			}

			set
			{
				width = value;

				OnModified();
			}
		}

		public object Clone()
		{
			ColumnHeader clone = new ColumnHeader();

			clone.ListView = ListView;
			clone.Name = Name;
			clone.Tag = Tag;
			clone.Text = Text;
			clone.Width = Width;

			return clone;
		}

		public ColumnPanel ColumnPanelInHierarchy
		{
			get;
			set;
		}

		private void OnModified()
		{
			if (ListView != null)
			{
				ListView.RebuildHierarchy();
			}
		}

		private bool hasForeColorBeenSet = false;
		private Color foreColor;
		public Color ForeColor
		{
			get
			{
				Color foreColor = this.foreColor;
				if (!hasForeColorBeenSet)
				{
					if (ListView != null)
					{
						foreColor = ListView.DefaultHeadingTextColor;
					}
				}

				return foreColor;
			}

			set
			{
				foreColor = value;
				hasForeColorBeenSet = true;
				OnModified();
			}
		}

		private bool hasBackColorBeenSet = false;
		private Color backColor;
		public Color BackColor
		{
			get
			{
				Color backColor = this.backColor;
				if (!hasBackColorBeenSet)
				{
					if (ListView != null)
					{
						backColor = ListView.DefaultHeadingBackgroundColor;
					}
				}

				return backColor;
			}

			set
			{
				backColor = value;
				hasBackColorBeenSet = true;
				OnModified();
			}
		}
	}
}
