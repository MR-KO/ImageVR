﻿namespace Endgame
{
	using System;
	using System.Collections;
	using System.Collections.Specialized;
	using System.Collections.Generic;
	using System.Reflection;
	using UnityEngine;

	public sealed class ImageList
	{
		private ImageList.ImageCollection images;

		//public ImageList();
		//public ImageList(IContainer container);
		//public ColorDepth ColorDepth { get; set; }
		//public IntPtr Handle { get; }
		//public bool HandleCreated { get; }

		public delegate void ImageListChanged(ImageList sender);
		public event ImageListChanged OnChanged;

		public ImageList()
		{
			images = new ImageCollection();
			images.Owner = this;
			WasImageSizeSet = false;
		}

		private void RaiseOnChangedEvent()
		{
			if (OnChanged != null)
			{
				OnChanged(this);
			}
		}

		public bool WasImageSizeSet { get; set; }
		public ImageList.ImageCollection Images { get { return images; } }
		private Vector2 imageSize;
		public Vector2 ImageSize
		{
			get
			{
				return imageSize;
			}

			set
			{
				imageSize = value;
				WasImageSizeSet = true;
				RaiseOnChangedEvent();
			}
		}
		//public ImageListStreamer ImageStream { get; set; }
		public object Tag { get; set; }
		//public Color TransparentColor { get; set; }
		//public event EventHandler RecreateHandle;

		//protected override void Dispose(bool disposing);
		//public void Draw(Graphics g, Point pt, int index);
		//public void Draw(Graphics g, int x, int y, int index);
		//public void Draw(Graphics g, int x, int y, int width, int height, int index);
		//public override string ToString();

		public sealed class ImageCollection : IList, ICollection, IEnumerable
		{
			private class Entry
			{
				public Entry(string key, Sprite sprite)
				{
					Key = key;
					Sprite = sprite;
				}

				public Entry(Sprite sprite)
					: this(null, sprite)
				{
				}

				public string Key;
				public Sprite Sprite;
			}
			public ImageList Owner { get; set; }

			private List<Entry> imageList = new List<Entry>();

			public int Count { get { return imageList.Count; } }
			public bool Empty { get { return Count == 0; } }
			//public bool IsReadOnly { get; }
			//public StringCollection Keys { get { return images.Keys; } }

			public Sprite this[int index]
			{
				get
				{
					Sprite result = null;
					if (index < imageList.Count)
					{
						result = imageList[index].Sprite;
					}
					return result;
				}

				set
				{
					imageList[index] = new Entry(value);
					Owner.RaiseOnChangedEvent();
				}
			}

			object IList.this[int index]
			{
				get
				{
					object result = null;
					if (index < imageList.Count)
					{
						result = imageList[index].Sprite;
					}
					return result;
				}

				set
				{
					if (index < imageList.Count)
					{
						Sprite sprite = value as Sprite;
						if (sprite != null)
						{
							imageList[index].Sprite = sprite;
							Owner.RaiseOnChangedEvent();
						}
					}
				}
			}

			public Sprite this[string key]
			{
				get
				{
					Sprite result = null;
					var findResult = imageList.Find(entry => entry.Key == key);
					if (findResult != null)
					{
						result = findResult.Sprite;
					}
					return result;
				}
			}

			//public void Add(Icon value);
			public void Add(Sprite value)
			{
				imageList.Add(new Entry(value));
				Owner.RaiseOnChangedEvent();
			}

			public int Add(object value)
			{
				Sprite sprite = value as Sprite;
				int result = -1;

				if (sprite != null)
				{
					Add(sprite);
					result = Count - 1;
				}
				return result;
			}

			public void Insert(int index, object value)
			{
				Sprite sprite = value as Sprite;
				if (sprite != null)
				{
					imageList.Insert(index, new Entry(sprite));
					Owner.RaiseOnChangedEvent();
				}
			}

			//public int Add(Image value, Color transparentColor);
			//public void Add(string key, Icon icon);
			public void Add(string key, Sprite image)
			{
				Entry result = imageList.Find(entry => entry.Key == key);
				if (result != null)
				{
					result.Sprite = image;
				}
				else
				{
					imageList.Add(new Entry(key, image));
				}
				Owner.RaiseOnChangedEvent();
			}

			public void AddRange(Sprite[] images)
			{
				foreach (var image in images)
				{
					imageList.Add(new Entry(image));
				}
				Owner.RaiseOnChangedEvent();
			}

			//public int AddStrip(Image value);
			public void Clear()
			{
				imageList.Clear();
				Owner.RaiseOnChangedEvent();
			}

			public bool Contains(Sprite image)
			{
				var result = imageList.Find(entry => entry.Sprite == image);
				return result != null;
			}

			public bool Contains(object image)
			{
				Sprite sprite = image as Sprite;

				if (sprite != null)
				{
					return Contains(sprite);
				}
				return false;
			}

			public bool ContainsKey(string key)
			{
				var result = imageList.Find(entry => entry.Key == key);
				return result != null;
			}

			public IEnumerator GetEnumerator()
			{
				return imageList.GetEnumerator();
			}

			public int IndexOf(Sprite image)
			{
				int result = 0;
				bool found = false;
				foreach (var entry in imageList)
				{
					if (entry.Sprite == image)
					{
						found = true;
						break;
					}
					result++;
				}
				if (!found)
				{
					result = -1;
				}
				return result;
			}

			public int IndexOf(object image)
			{
				Sprite sprite = image as Sprite;

				if (sprite != null)
				{
					return IndexOf(sprite);
				}
				return -1;
			}

			public int IndexOfKey(string key)
			{
				int result = 0;
				bool found = false;
				foreach (var entry in imageList)
				{
					if (entry.Key == key)
					{
						found = true;
						break;
					}
					result++;
				}
				if (!found)
				{
					result = -1;
				}
				return result;
			}

			public void Remove(Sprite image)
			{
				int index = IndexOf(image);
				if (index != -1)
				{
					RemoveAt(index);
				}
			}

			public void Remove(object value)
			{
				Sprite sprite = value as Sprite;
				if (sprite != null)
				{
					Remove(sprite);
				}
			}

			public void RemoveAt(int index)
			{
				imageList.RemoveAt(index);
				Owner.RaiseOnChangedEvent();
			}

			public void RemoveByKey(string key)
			{
				int index = IndexOfKey(key);
				if (index != -1)
				{
					RemoveAt(index);
				}
			}

			public void SetKeyName(int index, string name)
			{
				imageList[index].Key = name;
				Owner.RaiseOnChangedEvent();
			}

			public bool IsReadOnly { get { return false; } }
			public bool IsFixedSize { get { return false; } }
			public object SyncRoot { get { return null; } }
			public bool IsSynchronized { get { return false; } }

			public void CopyTo(System.Array array, int index)
			{
				int d = 0;
				for (int s = index; s < Count; s++)
				{
					array.SetValue(this[s], d);
					d++;
				}
			}
		}
	}
}
