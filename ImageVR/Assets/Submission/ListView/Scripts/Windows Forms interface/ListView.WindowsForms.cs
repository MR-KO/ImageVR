﻿namespace Endgame
{
	using UnityEngine;
	using UnityEngine.UI;
	using UnityEngine.EventSystems;
	using UnityEngine.Events;
	using System.Collections;
	using System.Collections.Generic;
	using System.Linq;
	using System.Reflection;

	public partial class ListView : MonoBehaviour
	{
		private ColumnHeaderCollection columnHeaders;
		private ListViewItemCollection items;
		private SelectedIndexCollection selectedIndices;
		private SelectedListViewItemCollection selectedItems;
		private IComparer listViewItemSorter = null;

		// FIXME: These are in the global namespace in WinForms, 
		// so should they really be here?
		public class ColumnClickEventArgs : System.EventArgs
		{
			public ColumnClickEventArgs(int column)
			{
				this.column = column;
			}

			private int column = -1;

			public int Column
			{
				get
				{
					return column;
				}
			}
		}

		public delegate void ColumnClickEventHandler(object sender, ColumnClickEventArgs e);

		public ColumnHeaderCollection Columns
		{
			get
			{
				if (columnHeaders == null)
				{
					Debug.LogError("ListView: Columns has not yet been initialised and is null. Columns is initialised in Awake. If you're calling this from Start, you'll need to move it to Awake instead.");
				}

				return columnHeaders;
			}
		}

		//public ListViewItem FocusedItem { get; set; }

		//public bool FullRowSelect { get; set; }

		public ListViewItemCollection Items
		{
			get
			{
				if (items == null)
				{
					Debug.LogError("ListView: Items has not yet been initialised and is null. Items is initialised in Awake. If you're calling this from Start, you'll need to move it to Awake instead.");
				}

				return items;
			}
		}

		public IComparer ListViewItemSorter
		{
			get
			{
				return listViewItemSorter;
			}
			set
			{
				listViewItemSorter = value;
				SortItems();
			}
		}

		//public bool MultiSelect { get; set; }

		public SelectedIndexCollection SelectedIndices { get { return selectedIndices; } }

		public SelectedListViewItemCollection SelectedItems { get { return selectedItems; } }

		//public SortOrder Sorting { get; set; }

		//public ListViewItem TopItem { get; set; }

		public event System.EventHandler ItemActivate = null;

		public event System.EventHandler SelectedIndexChanged = null;

		public event ColumnClickEventHandler ColumnClick = null;

		//public event ListViewItemSelectionChangedEventHandler ItemSelectionChanged;

		public event ListViewItemMouseHoverEventHandler ItemMouseHover = null;

		public delegate void GotFocusEventHandler(object sender, System.EventArgs e);
		public event GotFocusEventHandler GotFocus = null;
		public delegate void LostFocusEventHandler(object sender, System.EventArgs e);
		public event LostFocusEventHandler LostFocus = null;

		// New event handlers (additional to those provided by Windows Forms).
		public delegate void SubItemClickedEventHandler(UnityEngine.EventSystems.PointerEventData pointerEventData, ListViewItem.ListViewSubItem subItem);
		public event SubItemClickedEventHandler SubItemClicked = null;
		public delegate void ItemChangedEventHandler(ListViewItem listViewItem);
		public event ItemChangedEventHandler ItemChanged = null;
		public delegate void ItemBecameVisibleEventHandler(ListViewItem item);
		public event ItemBecameVisibleEventHandler ItemBecameVisible = null;
		public delegate void ItemBecameInvisibleEventHandler(ListViewItem item);
		public event ItemBecameInvisibleEventHandler ItemBecameInvisible = null;

		private Color foreColor;
		public Color ForeColor
		{
			get
			{
				return foreColor;
			}

			set
			{
				foreColor = value;
				RebuildHierarchy();
			}
		}

		private Color backColor;
		public Color BackColor
		{
			get
			{
				return backColor;
			}

			set
			{
				backColor = value;
				RebuildHierarchy();
			}
		}

		public ListViewItem FindItemWithText(string text)
		{
			ListViewItem result = null;
			foreach (ListViewItem item in items)
			{
				if (item.Text == text)
				{
					result = item;
				}
			}
			return result;
		}

		//public ListViewItem FindItemWithText(string text, bool includeSubItemsInSearch, int startIndex)

		//public ListViewItem FindItemWithText(string text, bool includeSubItemsInSearch, int startIndex, bool isPrefixSearch);

		public class ColumnHeaderCollection : IList, ICollection, IEnumerable
		{
			private ListView owner;
			private List<ColumnHeader> columnHeaders = new List<ColumnHeader>();

			public ColumnHeaderCollection(ListView owner)
			{
				this.owner = owner;
			}

			public int Count { get { return columnHeaders.Count; } }
			public bool IsReadOnly { get { return false; } }
			public bool IsFixedSize { get { return false; } }
			object IList.this[int index]
			{
				get
				{
					return columnHeaders[index];
				}

				set
				{
					ColumnHeader columnHeader = value as ColumnHeader;
					if ((index < columnHeaders.Count) && (columnHeader != null))
					{
						columnHeaders[index] = columnHeader;
						owner.RebuildHierarchy();
					}
				}
			}

			public virtual ColumnHeader this[int index] { get { return columnHeaders[index]; } }

			public virtual ColumnHeader this[string key] { get { return columnHeaders.Find(x => x.Name == key); } }

			private void AddInternal(ColumnHeader columnHeader)
			{
				// Add the column header to the collection.
				columnHeaders.Add(columnHeader);
				columnHeader.ListView = owner;

				owner.RebuildHierarchy();
			}

			public virtual int Add(object value)
			{
				int result = -1;
				ColumnHeader columnHeader = value as ColumnHeader;
				if (columnHeader != null)
				{
					AddInternal(columnHeader);
					result = columnHeaders.Count - 1;
				}
				return result;
			}

			public virtual int Add(ColumnHeader value)
			{
				AddInternal(value);
				return columnHeaders.Count - 1;
			}

			public virtual ColumnHeader Add(string text)
			{
				ColumnHeader columnHeader = new ColumnHeader();
				columnHeader.Text = text;
				AddInternal(columnHeader);
				return columnHeader;
			}

			public virtual ColumnHeader Add(string text, int width)
			{
				ColumnHeader columnHeader = new ColumnHeader();
				columnHeader.Text = text;
				columnHeader.Width = width;
				AddInternal(columnHeader);
				return columnHeader;
			}

			public virtual ColumnHeader Add(string key, string text)
			{
				ColumnHeader columnHeader = new ColumnHeader();
				columnHeader.Name = key;
				columnHeader.Text = text;
				AddInternal(columnHeader);
				return columnHeader;
			}

			public virtual ColumnHeader Add(string key, string text, int width)
			{
				ColumnHeader columnHeader = new ColumnHeader();
				columnHeader.Name = key;
				columnHeader.Text = text;
				columnHeader.Width = width;
				AddInternal(columnHeader);
				return columnHeader;
			}

			public virtual void AddRange(ColumnHeader[] values)
			{
				foreach (ColumnHeader columnHeader in values)
				{
					AddInternal(columnHeader);
				}
			}

			public virtual void Clear()
			{
				columnHeaders.Clear();
				owner.RebuildHierarchy();
			}

			public bool Contains(object value)
			{
				bool result = false;
				ColumnHeader columnHeader = value as ColumnHeader;
				if (columnHeader != null)
				{
					result = Contains(columnHeader);
				}
				return result;
			}

			public bool Contains(ColumnHeader value)
			{
				return columnHeaders.Contains(value);
			}

			public virtual bool ContainsKey(string key)
			{
				return columnHeaders.Any(x => x.Name == key);
			}

			public IEnumerator GetEnumerator()
			{
				return columnHeaders.GetEnumerator();
			}

			public int IndexOf(object value)
			{
				int result = -1;
				ColumnHeader columnHeader = value as ColumnHeader;
				if (columnHeader != null)
				{
					result = IndexOf(columnHeader);
				}
				return result;
			}

			public int IndexOf(ColumnHeader value)
			{
				return columnHeaders.IndexOf(value);
			}

			public virtual int IndexOfKey(string key)
			{
				int result = -1;

				foreach (ColumnHeader columnHeader in columnHeaders)
				{
					if (columnHeader.Name == key)
					{
						result = columnHeaders.IndexOf(columnHeader);
					}
				}

				return result;
			}

			public void Insert(int index, object value)
			{
				if (value is ColumnHeader)
				{
					InsertInternal(index, value as ColumnHeader);
				}
			}

			private void InsertInternal(int index, ColumnHeader value)
			{
				columnHeaders.Insert(index, value);
				value.ListView = owner;
				owner.RebuildHierarchy();
			}

			public void Insert(int index, ColumnHeader value)
			{
				InsertInternal(index, value);
			}

			public void Insert(int index, string text)
			{
				ColumnHeader columnHeader = new ColumnHeader();
				columnHeader.Text = text;
				InsertInternal(index, columnHeader);
			}

			public void Insert(int index, string text, int width)
			{
				ColumnHeader columnHeader = new ColumnHeader();
				columnHeader.Text = text;
				columnHeader.Width = width;
				InsertInternal(index, columnHeader);
			}

			public void Insert(int index, string key, string text)
			{
				ColumnHeader columnHeader = new ColumnHeader();
				columnHeader.Name = key;
				columnHeader.Text = text;
				InsertInternal(index, columnHeader);
			}

			public void Insert(int index, string key, string text, int width)
			{
				ColumnHeader columnHeader = new ColumnHeader();
				columnHeader.Name = key;
				columnHeader.Text = text;
				columnHeader.Width = width;
				InsertInternal(index, columnHeader);
			}

			private void RemoveInternal(ColumnHeader columnHeader)
			{
				columnHeaders.Remove(columnHeader);
				owner.RebuildHierarchy();
			}

			public void Remove(object value)
			{
				ColumnHeader columnHeader = value as ColumnHeader;
				if (columnHeader != null)
				{
					RemoveInternal(columnHeader);
				}
			}

			public virtual void Remove(ColumnHeader column)
			{
				RemoveInternal(column);
			}

			public virtual void RemoveAt(int index)
			{
				ColumnHeader columnHeader = columnHeaders[index];
				RemoveInternal(columnHeader);
			}

			public virtual void RemoveByKey(string key)
			{
				ColumnHeader value = this[key];
				if (value != null)
				{
					RemoveInternal(value);
				}
			}

			public bool IsSynchronized { get { return false; } }

			public object SyncRoot { get { return null; } }

			public void CopyTo(System.Array array, int index)
			{
				int d = 0;
				for (int s = index; s < Count; s++)
				{
					array.SetValue(this[s], d);
					d++;
				}
			}
		}

		private ImageList smallImageList;
		public ImageList SmallImageList
		{
			get
			{
				return smallImageList;
			}

			set
			{
				ImageList imageList = value;
				smallImageList = imageList;
				imageList.OnChanged += OnImageListChanged;
				RebuildHierarchy();
			}
		}

		public class ListViewItemCollection : IList, ICollection, IEnumerable
		{
			private ListView owner;
			private List<ListViewItem> items;

			public ListViewItemCollection(ListView owner)
			{
				this.owner = owner;
				items = new List<ListViewItem>();
			}

			public int Count { get { return items.Count; } }

			public bool IsFixedSize { get { return false; } }

			public bool IsReadOnly { get { return false; } }

			object IList.this[int index]
			{
				get
				{
					return items[index];
				}

				set
				{
					ListViewItem listViewItem = value as ListViewItem;
					if (listViewItem != null)
					{
						if (index < items.Count)
						{
							// Add the item to the collection.
							items[index] = listViewItem;
							listViewItem.ListView = owner;

							owner.RebuildHierarchy();

							if (listViewItem.Selected)
							{
								owner.SelectedIndices.Add(listViewItem.Index);
							}
						}
					}
				}
			}

			public virtual ListViewItem this[int index]
			{
				get
				{
					return items[index];
				}
				set
				{
					if (index < items.Count)
					{
						// Add the item to the collection.
						items[index] = value;
						value.ListView = owner;

						owner.RebuildHierarchy();

						if (value.Selected)
						{
							owner.SelectedIndices.Add(value.Index);
						}
					}
				}
			}

			public virtual ListViewItem this[string key]
			{
				get
				{
					return items.Find(x => x.Name == key);
				}
			}

			private void AddInternal(ListViewItem listViewItem)
			{
				if (owner.Columns.Count == 0)
				{
					Debug.LogWarning("ListView: Adding an item without any columns - nothing will appear. (Add a column first using Listview.Columns.Add).");
				}

				// Add the item to the collection.
				items.Add(listViewItem);
				listViewItem.ListView = owner;

				owner.RebuildHierarchy();

				if (listViewItem.Selected)
				{
					owner.SelectedIndices.Add(listViewItem.Index);
				}
			}

			public int Add(object value)
			{
				int result = -1;
				ListViewItem item = value as ListViewItem;
				if (item != null)
				{
					Add(item);
					result = items.Count - 1;
				}
				return result;
			}

			public virtual ListViewItem Add(ListViewItem value)
			{
				AddInternal(value);
				return value;
			}

			public virtual ListViewItem Add(string text)
			{
				ListViewItem listViewItem = new ListViewItem(text);
				AddInternal(listViewItem);
				return listViewItem;
			}

			public void AddRange(ListView.ListViewItemCollection items)
			{
				foreach (ListViewItem item in items)
				{
					AddInternal(item);
				}
			}

			public void AddRange(ListViewItem[] items)
			{
				foreach (ListViewItem item in items)
				{
					AddInternal(item);
				}
			}

			public virtual void Clear()
			{
				owner.SuspendLayout();
				{
					owner.SelectedIndices.Clear();
					items.Clear();
					owner.SetHorizontalScrollBarValue(0);
					owner.SetVerticalScrollBarValue(0);
				}
				owner.ResumeLayout();
			}

			public bool Contains(object value)
			{
				bool result = false;
				ListViewItem item = value as ListViewItem;
				if (item != null)
				{
					result = Contains(item);
				}
				return result;
			}

			public bool Contains(ListViewItem item)
			{
				return items.Contains(item);
			}

			public virtual bool ContainsKey(string key)
			{
				return items.Any(x => x.Name == key);
			}

			public ListViewItem[] Find(string key, bool searchAllSubItems)
			{
				return items.FindAll(x => x.Name == key).ToArray();
			}

			public IEnumerator GetEnumerator()
			{
				return items.GetEnumerator();
			}

			public int IndexOf(object value)
			{
				int result = -1;
				ListViewItem item = value as ListViewItem;
				if (item != null)
				{
					result = IndexOf(value);
				}
				return result;
			}

			public int IndexOf(ListViewItem item)
			{
				return items.IndexOf(item);
			}

			public virtual int IndexOfKey(string key)
			{
				int result = -1;

				foreach (ListViewItem listViewItem in items)
				{
					if (listViewItem.Name == key)
					{
						result = items.IndexOf(listViewItem);
					}
				}

				return result;
			}

			public void Insert(int index, object value)
			{
				if (value is ListViewItem)
				{
					InsertInternal(index, value as ListViewItem);
				}
			}

			private void InsertInternal(int index, ListViewItem item)
			{
				ListViewItem selectedItem = null;
				if (owner.SelectedIndices.Count > 0)
				{
					selectedItem = owner.SelectedItems[0];
				}

				// If an item is already selected, remove the selection temporarily.
				if (selectedItem != null)
				{
					owner.SelectedIndices.RemoveAndUpdateSelection(selectedItem.Index, raiseEvent: false);
				}

				// Insert the new item.
				items.Insert(index, item);
				item.ListView = owner;

				// If the new item should be selected, select it.
				if (item.Selected)
				{
					owner.SelectedIndices.AddAndUpdateSelection(item.Index, raiseEvent: false);
				}
				// Otherwise, reselect the previously selected item (by using its updated index).
				else if (selectedItem != null)
				{
					owner.SelectedIndices.AddAndUpdateSelection(selectedItem.Index, raiseEvent: false);
				}

				owner.RebuildHierarchy();
			}

			public ListViewItem Insert(int index, ListViewItem item)
			{
				InsertInternal(index, item);
				return item;
			}

			public ListViewItem Insert(int index, string text)
			{
				ListViewItem listViewItem = new ListViewItem(text);
				InsertInternal(index, listViewItem);
				return listViewItem;
			}

			private void RemoveInternal(ListViewItem item)
			{
				ListViewItem selectedItem = null;
				int selectedItemIndex = -1;
				bool selectedItemRemoved = false;

				// If an item is currently selected...
				if (owner.SelectedIndices.Count > 0)
				{
					// Get the selected item and its index.
					selectedItem = owner.SelectedItems[0];
					selectedItemIndex = selectedItem.Index;

					// Set a flag to indicate whether the selected item will be removed.
					selectedItemRemoved = item.Index == selectedItemIndex;
				}

				// If an item is already selected, remove the selection temporarily.
				if (selectedItem != null)
				{
					owner.SelectedIndices.RemoveAndUpdateSelection(selectedItem.Index, raiseEvent: false);
				}

				// Remove the item.
				items.Remove(item);

				int newSelectedItemIndex = -1;

				// If the selected item was removed, select the item now at its index.
				if (selectedItemRemoved)
				{
					if (items.Count > 0)
					{
						newSelectedItemIndex = Mathf.Min(Mathf.Max(selectedItemIndex, 0), items.Count - 1);
						owner.SelectedIndices.AddAndUpdateSelection(newSelectedItemIndex, raiseEvent: false);
					}
				}
				// Otherwise, if there was a selected item, but it was not the item being removed,
				// reselect it (by using its updated index).
				else if (selectedItem != null)
				{
					newSelectedItemIndex = selectedItem.Index;
					owner.SelectedIndices.AddAndUpdateSelection(newSelectedItemIndex, raiseEvent: false);
				}

				if (newSelectedItemIndex != selectedItemIndex)
				{
					owner.OnSelectedIndexChanged(new System.EventArgs());
				}

				owner.RebuildHierarchy();
			}

			public void Remove(object value)
			{
				ListViewItem item = value as ListViewItem;
				if (item != null)
				{
					RemoveInternal(item);
				}
			}

			public virtual void Remove(ListViewItem item)
			{
				RemoveInternal(item);
			}

			public virtual void RemoveAt(int index)
			{
				ListViewItem listViewItem = items[index];
				RemoveInternal(listViewItem);
			}

			public virtual void RemoveByKey(string key)
			{
				ListViewItem value = this[key];
				if (value != null)
				{
					RemoveInternal(value);
				}
			}

			public bool IsSynchronized { get { return false; } }

			public object SyncRoot { get { return null; } }

			public void CopyTo(System.Array array, int index)
			{
				int d = 0;
				for (int s = index; s < Count; s++)
				{
					array.SetValue(this[s], d);
					d++;
				}
			}

			private class Comparer : IComparer<ListViewItem>
			{
				private IComparer comparer;
				public Comparer(IComparer comparer)
				{
					this.comparer = comparer;
				}

				public int Compare(ListViewItem x, ListViewItem y)
				{
					return comparer.Compare(x, y);
				}
			}

			public void Sort(IComparer listViewItemSorter)
			{
				items.Sort(new Comparer(listViewItemSorter));
			}
		}

		public class SelectedIndexCollection : IList, ICollection, IEnumerable
		{
			private ListView owner;
			private List<int> selectedIndices;

			public SelectedIndexCollection(ListView owner)
			{
				this.owner = owner;
				selectedIndices = new List<int>();
			}

			public int Count { get { return selectedIndices.Count; } }

			public int this[int index] { get { return selectedIndices[index]; } }

			private void AddInternal(int itemIndex)
			{
				selectedIndices.Add(itemIndex);
			}

			public void AddAndUpdateSelection(int itemIndex, bool raiseEvent)
			{
				// TODO: If MultiSelect is true, don't clear here.
				int indexToDeselect = -1;

				if (selectedIndices.Count > 0)
				{
					indexToDeselect = selectedIndices[0];
				}

				if (indexToDeselect != itemIndex)
				{
					ClearAndUpdateSelection(raiseEvent: false);

					AddInternal(itemIndex);

					bool updateGameObjects = !owner.LayoutSuspended;

					owner.OnSelectionAddedInternal(itemIndex, updateGameObjects: updateGameObjects);

					if (raiseEvent)
					{
						owner.OnSelectedIndexChanged(new System.EventArgs());
					}
				}
			}

			public int Add(int itemIndex)
			{
				AddAndUpdateSelection(itemIndex, raiseEvent: true);
				return selectedIndices.Count;
			}

			private void ClearInternal()
			{
				selectedIndices.Clear();
			}

			private void ClearAndUpdateSelection(bool raiseEvent)
			{
				if (owner != null)
				{
					owner.OnSelectionClearedInternal();
				}
				ClearInternal();

				if (raiseEvent)
				{
					owner.OnSelectedIndexChanged(new System.EventArgs());
				}
			}

			public void Clear()
			{
				ClearAndUpdateSelection(raiseEvent: true);
			}

			public bool Contains(int selectedIndex)
			{
				return selectedIndices.Contains(selectedIndex);
			}

			public IEnumerator GetEnumerator()
			{
				return selectedIndices.GetEnumerator();
			}

			public int IndexOf(int selectedIndex)
			{
				return selectedIndices.IndexOf(selectedIndex);
			}

			public void RemoveAndUpdateSelection(int index, bool raiseEvent)
			{
				if (!selectedIndices.Contains(index))
				{
					return;
				}

				if (owner != null)
				{
					owner.OnSelectionRemovedInternal(index);
				}
				selectedIndices.Remove(index);

				if (raiseEvent)
				{
					owner.OnSelectedIndexChanged(new System.EventArgs());
				}
			}

			public void Remove(int itemIndex)
			{
				RemoveAndUpdateSelection(itemIndex, raiseEvent: true);
			}

			public bool IsFixedSize { get { return false; } }

			public bool IsReadOnly { get { return false; } }

			object IList.this[int index]
			{
				get
				{
					return selectedIndices[index];
				}

				set
				{
					throw new System.NotImplementedException();
				}
			}

			public int Add(object value)
			{
				if (value is int)
				{
					int index = (int)value;
					AddAndUpdateSelection(index, raiseEvent: true);
				}
				return selectedIndices.Count;
			}

			public bool Contains(object value)
			{
				bool result = false;
				if (value is int)
				{
					int index = (int)value;
					result = selectedIndices.Contains(index);
				}
				return result;
			}

			public int IndexOf(object value)
			{
				int result = -1;
				if (value is int)
				{
					int index = (int)value;
					result = selectedIndices.IndexOf(index);
				}
				return result;
			}

			public void Insert(int index, object value)
			{
				if (value is int)
				{
					// TODO: If MultiSelect is true, perform an insert.
					// (When it is false, the operation is the same as Add.)
					Clear();

					int itemIndex = (int)value;
					AddAndUpdateSelection(itemIndex, raiseEvent: true);
				}
			}

			public void Remove(object value)
			{
				if (value is int)
				{
					int index = (int)value;
					RemoveAndUpdateSelection(index, raiseEvent: true);
				}
			}

			public void RemoveAt(int itemIndex)
			{
				int index = selectedIndices[itemIndex];
				RemoveAndUpdateSelection(index, raiseEvent: true);
			}

			public bool IsSynchronized { get { return false; } }

			public object SyncRoot { get { return null; } }

			public void CopyTo(System.Array array, int index)
			{
				int d = 0;
				for (int s = index; s < Count; s++)
				{
					array.SetValue(this[s], d);
					d++;
				}
			}
		}

		private interface ISelectedListViewItemCollectionInternals
		{
			void ClearInternal();
			void AddInternal(ListViewItem listViewItem);
			void RemoveInternal(ListViewItem listViewItem);			
		}

		public class SelectedListViewItemCollection : IList, ICollection, IEnumerable, ISelectedListViewItemCollectionInternals
		{
			//private ListView owner;
			List<ListViewItem> selectedItems;

			public SelectedListViewItemCollection(ListView owner)
			{
				//this.owner = owner;
				selectedItems = new List<ListViewItem>();
			}

			public int Count { get { return selectedItems.Count; } }

			public ListViewItem this[int index] { get { return selectedItems[index]; } }

			public virtual ListViewItem this[string key] { get { return selectedItems.Find(x => x.Name == key); } }

			void IList.Clear()
			{
				selectedItems.Clear();
			}

			public bool Contains(ListViewItem item)
			{
				return selectedItems.Contains(item);
			}

			public virtual bool ContainsKey(string key)
			{
				return selectedItems.Any(x => x.Name == key);
			}

			public IEnumerator GetEnumerator()
			{
				return GetEnumerator();
			}

			public int IndexOf(ListViewItem item)
			{
				return selectedItems.IndexOf(item);
			}

			public virtual int IndexOfKey(string key)
			{
				int result = -1;

				foreach (ListViewItem listViewItem in selectedItems)
				{
					if (listViewItem.Name == key)
					{
						result = selectedItems.IndexOf(listViewItem);
					}
				}

				return result;
			}

			public bool IsFixedSize { get { return false; } }

			public bool IsReadOnly { get { return false; } }

			object IList.this[int index]
			{
				get
				{
					return selectedItems[index];
				}

				set
				{
					throw new System.NotImplementedException();
				}
			}

			// AddInternal, RemoveInternal and ClearInternal should not be accessible to 
			// general code, because this collection needs to mirror the
			// SelectedIndices collection.
			// (I think this is achieved in Windows Forms via the internal
			// modifier, but that requires the code to be in a different assembly.)
			// Since this is not possible, the ListView class calls these private members
			// using reflection.
			void ISelectedListViewItemCollectionInternals.ClearInternal()
			{
				selectedItems.Clear();
			}

			void ISelectedListViewItemCollectionInternals.AddInternal(ListViewItem listViewItem)
			{
				selectedItems.Add(listViewItem);
			}

			void ISelectedListViewItemCollectionInternals.RemoveInternal(ListViewItem listViewItem)
			{
				selectedItems.Remove(listViewItem);
			}

			int IList.Add(object value)
			{
				if (value is ListViewItem)
				{
					ListViewItem listViewItem = (ListViewItem)value;
					ISelectedListViewItemCollectionInternals internals = this;
					internals.AddInternal(listViewItem);
				}
				return selectedItems.Count;
			}

			public bool Contains(object value)
			{
				bool result = false;
				if (value is ListViewItem)
				{
					ListViewItem listViewItem = (ListViewItem)value;
					result = selectedItems.Contains(listViewItem);
				}
				return result;
			}

			public int IndexOf(object value)
			{
				int result = -1;
				if (value is ListViewItem)
				{
					ListViewItem listViewItem = (ListViewItem)value;
					result = selectedItems.IndexOf(listViewItem);
				}
				return result;
			}

			void IList.Insert(int index, object value)
			{
				throw new System.NotImplementedException();
			}

			void IList.Remove(object value)
			{
				if (value is ListViewItem)
				{
					ListViewItem listViewItem = (ListViewItem)value;
					ISelectedListViewItemCollectionInternals internals = this;
					internals.RemoveInternal(listViewItem);
				}
			}

			void IList.RemoveAt(int index)
			{
				ListViewItem listViewItem = selectedItems[index];
				ISelectedListViewItemCollectionInternals internals = this;
				internals.RemoveInternal(listViewItem);
			}

			public bool IsSynchronized { get { return false; } }

			public object SyncRoot { get { return null; } }

			public void CopyTo(System.Array array, int index)
			{
				int d = 0;
				for (int s = index; s < Count; s++)
				{
					array.SetValue(this[s], d);
					d++;
				}
			}
		}

		private void OnImageListChanged(ImageList sender)
		{
			RebuildHierarchy();
		}

		public void Select()
		{
			EventSystem.current.SetSelectedGameObject(gameObject);
		}

		public bool ContainsFocus()
		{
			return EventSystem.current.currentSelectedGameObject == gameObject;
		}
	}
}
