﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.IO;
using System.Collections;
using cakeslice;

public class ImageScript : MonoBehaviour, IGlow
{
    UnityEvent MouseIn;
    UnityEvent MouseOut;

	public ClusterBase cluster;
	public bool inController = false;

	public void Highlight()
    {
        var outline = GetComponent<Outline>();
        outline.enabled = true;
    }

    public void UnHighlight()
    {
        var outline = GetComponent<Outline>();
		outline.enabled = false;
    }
}
