﻿
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

public class PinAsyncLoader : JobItem
{
	// The pin that we're going to load its image for
	public Pin pin;

	// Whether or not to resize the image
	public bool resize;

	// Produced output
	public Image image;
	public UnityEngine.Color[] pixels = null;
	public int width = -1, height = -1;


	// This is executed on a separate thread
	protected override void DoWork()
	{
		if (pin.imageTexture == null)
		{
			return;
		}

		//fileData = File.Exists(pin.image.path) ? File.ReadAllBytes(pin.image.path) : null;
		// Get representing image for this pin...
		image = pin.GetRepresentingImage();

		if (image != null && File.Exists(image.path))
		{
			// Load image into bitmap
			Bitmap bmp = new Bitmap(image.path);

			// Create Color[] array
			width = bmp.Width;
			height = bmp.Height;
			pixels = new UnityEngine.Color[width * height];

			// Pre-allocate color objects so we don't have to do that later on
			for (int i = 0; i < width * height; i++)
			{
				pixels[i] = new UnityEngine.Color(0.0f, 0.0f, 0.0f, 0.0f);
			}

			// Fill it with the correct colors... Use much faster unsafe method
			// of 7-13 ms instead of slow bmp.GetPixel method of 6000 ms...
			// Taken from: http://csharpexamples.com/fast-image-processing-c/
			unsafe
			{
				BitmapData bitmapData = bmp.LockBits(new Rectangle(0, 0, width, height),
					ImageLockMode.ReadWrite, bmp.PixelFormat);
				int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(
					bmp.PixelFormat) / 8;
				//bool hasAlphaComponent = bytesPerPixel == 4;
				int heightInPixels = bitmapData.Height;
				int widthInBytes = bitmapData.Width * bytesPerPixel;
				byte* ptrFirstPixel = (byte*)bitmapData.Scan0;
				int index = 0;
				float inv255 = 1.0f / 255.0f; // For performance

				// Pixel array should go from bottom to top...
				for (int y = heightInPixels - 1; y >= 0; y--)
				{
					byte* currentLine = ptrFirstPixel + (y * bitmapData.Stride);

					for (int x = 0; x < widthInBytes; x = x + bytesPerPixel, index++)
					{
						// TODO: Check for alpha component? Version below...
						pixels[index].b = currentLine[x] * inv255;
						pixels[index].g = currentLine[x + 1] * inv255;
						pixels[index].r = currentLine[x + 2] * inv255;
						pixels[index].a = 1.0f;

						//if (hasAlphaComponent)
						//{
						//	pixels[index].a = currentLine[x + 3] * inv255;
						//}
						//else
						//{
						//	pixels[index].a = 1.0f;
						//}
					}
				}

				bmp.UnlockBits(bitmapData);
			}

			pin.FinishFileLoadingState();

			// Resize it, if needed
			if (resize)
			{
				pin.StartResizingState();
				UnityEngine.Color[] newPixels = Util.Resize(pixels, width, height,
					Util.resizeWidth, Util.resizeHeight, false);
				pixels = newPixels;
			}

			bmp.Dispose();
		}
	}

	// This is executed on the main thread
	public override void OnFinished()
	{
		if (image == null)
		{
			UnityEngine.Debug.Log("Tried to load image for pin but no active ones exist!");
			return;
		}

		if (pixels == null)
		{
			UnityEngine.Debug.Log("Image not found at path: " + image.path);
			return;
		}
		
		// Apply color array to texture
		if (resize)
		{
			Util.ApplyResize(pin.imageTexture, pixels,
				Util.resizeWidth, Util.resizeHeight, 0);
		}
		else
		{
			Util.ApplyResize(pin.imageTexture, pixels,
				width, height, 0);
		}

		pin.EndLoadingState();
	}
}
