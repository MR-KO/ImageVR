﻿using UnityEngine;
using System.Collections;
using cakeslice;


public enum PinState { Not_Loaded, File_Loading, File_Loaded, Resizing, Loaded, Revealed };

public class Pin : MonoBehaviour, IGlow
{
	public Map map;

	public PinState state = PinState.Not_Loaded;
	private bool visible = true;

	public bool imageRevealed = false;

	public Shader imageShader;
	public Texture2D imageTexture = null;

	public bool revealWhenClose = true;
	public bool hideWhenFar = true;

	public GameObject imagePrefab;
	public GameObject instantiatedImagePrefab = null;

	// TODO: Add displaying of a number indicating how many images there are
	// in the cluster of this pin... Including game object
	public ClusterBase cluster;

	// Used for cluster info
	public GameObject textPrefab;
	public GameObject instantiatedTextPrefab = null;

	// Used for the Maduromap
	public GameObject instantiatedLinePrefab = null;

	void Start()
	{
		Zfix();
	}

	public void Zfix()
	{
		float offset = Random.Range(0, 0.001f);
		Vector3 pos = transform.position;
		transform.position = new Vector3(pos.x, pos.y, pos.z + offset);// + map.random.)
	}


	public void Highlight()
	{
		var outline = GetComponent<Outline>();
		outline.enabled = true;
	}

	public void UnHighlight()
	{
		var outline = GetComponent<Outline>();
		outline.enabled = false;

	}

	public void SetVisible()
	{
		visible = true;
	}

	public void SetInvisible()
	{
		visible = false;
	}

	public bool isVisible()
	{
		return visible;
	}

	public void SetBeginState()
	{
		state = PinState.Not_Loaded;
	}

	public void StartLoadingState()
	{
		state = PinState.File_Loading;
	}

	public void FinishFileLoadingState()
	{
		state = PinState.File_Loaded;
	}

	public void StartResizingState()
	{
		state = PinState.Resizing;
	}

	public void EndLoadingState()
	{
		state = PinState.Loaded;
	}

	public Image GetRepresentingImage()
	{
		// Get first active image in the cluster as representing...
		return cluster.stack.GetActiveImage(0);
	}

	public void Reveal()
	{
		// Check if we actually need to reveal this pin...
		if (!isVisible())
		{
			return;
		}

		// Pin should be visible...
		if (!imageRevealed && revealWhenClose)
		{
			if (state != PinState.Loaded)
			{
				// We don't load directly... instead use a separate thread for that!
				return;
			}

			Image image = GetRepresentingImage();

			if (image == null)
			{
				Debug.Log("Cluster has no active images, this pin shouldnt be visible!!!");
				return;
			}

			GameObject imgPrefab = Instantiate(imagePrefab);
			instantiatedImagePrefab = imgPrefab;
			//imgPrefab.layer = Map.IMAGE_BILLBOARD_LAYER;
			imageRevealed = true;
			state = PinState.Revealed;
			SetScale();

			imgPrefab.transform.position = transform.position;

			//imgPrefab.transform.LookAt(map.mainCamera.transform.position);

			//imgPrefab.transform.parent = this.transform;
			imgPrefab.transform.SetParent(transform, true);

			// Add cluster's cluster of images to it
			ImageScript imscr = imgPrefab.GetComponent<ImageScript>();
			imscr.cluster = cluster;

			imgPrefab.GetComponent<Renderer>().material.mainTexture = imageTexture;
			this.GetComponent<Renderer>().enabled = false;
			gameObject.layer = Map.IMAGE_BILLBOARD_LAYER;
			
		}
	}

	public void SetScale()
	{
		// Set cluster size correctly
		cluster.SetClusterSize();

		// Scale billboard
		if (imageRevealed)
		{
			if (instantiatedImagePrefab != null)
			{
				instantiatedImagePrefab.transform.localScale = ClusterScale.GetClusterScale(cluster.size) * 0.4f;
			}
		}
		else
		{
			// Scale pin
			this.gameObject.transform.localScale = ClusterScale.GetClusterScale(cluster.size);
		}

		this.gameObject.transform.localScale *= 0.1f;
	}


	public void UnReveal()
	{
		// Image reveal should be undone!
		if (imageRevealed && hideWhenFar)
		{
			imageRevealed = false;
			state = PinState.Loaded;

			// Destroy image prefab thing...
			Destroy(instantiatedImagePrefab.GetComponent<ImageScript>());
			Destroy(instantiatedImagePrefab);
			instantiatedImagePrefab = null;

			this.GetComponent<Renderer>().enabled = true;
			gameObject.layer = Map.PIN_LAYER;
		}
	}

	public void Hide()
	{
		// Don't hide the pin if it should be visible...
		if (isVisible())
		{
			return;
		}

		SetInvisible();
		UnReveal();
		gameObject.SetActive(false);
		enabled = false;
		GetComponent<Renderer>().enabled = false;
		GetComponent<BoxCollider>().enabled = false;

		// We will need to load the image again, so make sure we set the correct state...
		state = PinState.Not_Loaded;

		// Also destroy imgprefab
		if (instantiatedImagePrefab != null)
		{
			Destroy(imageTexture);
			Destroy(instantiatedImagePrefab.GetComponent<ImageScript>());
			Destroy(instantiatedImagePrefab);
			instantiatedImagePrefab = null;
		}

		// Disable rendering of linePrefab, if exists
		if (instantiatedLinePrefab != null)
		{
			instantiatedLinePrefab.GetComponent<Renderer>().enabled = false;
		}
	}

	public void Show()
	{
		// Don't show the pin if it shouldnt be visible or if its revealed...
		if (!isVisible() || imageRevealed)
		{
			return;
		}

		SetVisible();
		imageRevealed = false;
		gameObject.SetActive(true);
		enabled = true;
		GetComponent<Renderer>().enabled = true;
		GetComponent<BoxCollider>().enabled = true;

		// Enable rendering of linePrefab, if exists
		if (instantiatedLinePrefab != null)
		{
			instantiatedLinePrefab.GetComponent<Renderer>().enabled = true;
		}
	}

	public void OnDisable()
	{
		Destroy(imageTexture);

		if (instantiatedImagePrefab != null)
		{
			ImageScript imageScript = instantiatedImagePrefab.GetComponent<ImageScript>();
			Destroy(imageScript);
			Destroy(instantiatedImagePrefab);
		}
	}
}

public class ImageData
{
    public Shader shader;
    public Texture texture;

    public ImageData(Shader imageShader, Texture imageTexture)
    {
        shader = imageShader;
        texture = imageTexture;
    }
}
