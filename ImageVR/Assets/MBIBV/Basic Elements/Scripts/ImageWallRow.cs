﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using cakeslice;
using System.IO;
using System.Text.RegularExpressions;

public class ImageWallRow
{
	public static List<Tag> tagList = null;

	public ClusterDate dateCluster;
	private int stackIndex = 0;

	public bool movedLeft = false;
	public bool movedRight = false;
	public bool underflowed = false;
	public bool overflowed = false;

	private bool useCurvedRow = false;
	private static float CURVED_ROW_RADIUS = 0.75f;
	private static float CURVED_ROW_SPACING = (float) (Math.PI / 2.0f);
	private bool rowActive = true;
	private bool isMainRow = false;
	private Vector3 infoTextBaseLocalPos;

	public bool fullyInitialized = false;

	// For displaying shit...
	public GameObject[] imageObjects;
	private Texture2D[] imageTextures;

	private GameObject dateTextObject;
	private GameObject infoTextObject;
	
	private GameObject leftArrow;
	private GameObject rightArrow;
	private Texture2D textureLeftArrow;
	private Texture2D textureRightArrow;

	private static Shader transparentShader = null;

	public static readonly int NUM_IMAGES_VISIBLE_PER_SIDE = 4;
	public static readonly int NUM_IMAGES_VISIBLE = NUM_IMAGES_VISIBLE_PER_SIDE * 2 + 1;
	public static readonly int CENTER_INDEX = NUM_IMAGES_VISIBLE_PER_SIDE; // Just for better readable code...

	public static readonly float IMAGE_SCALE = 0.4f;
	public static readonly float IMAGE_SCALE_SMALLER = 0.075f;
	public static readonly float ARROW_SCALE = IMAGE_SCALE - IMAGE_SCALE_SMALLER * (NUM_IMAGES_VISIBLE_PER_SIDE) * 1.1f;
	public static readonly float TEXT_SCALE = 0.002f; // was 0.0015f;

	public static readonly Vector3 textScale = new Vector3(TEXT_SCALE, TEXT_SCALE, TEXT_SCALE);
	public static readonly Vector3 arrowScale = new Vector3(
		ARROW_SCALE, ARROW_SCALE, ARROW_SCALE); // Was IMAGE_SCALE_SMALLER

	public ImageWallRow(GameObject imagePrefab, GameObject textPrefab,
						ClusterDate newDateCluster, bool useCurvedRow = false)
	{
		// Setup shader and cluster shit
		if (transparentShader == null)
		{
			transparentShader = Shader.Find("Unlit/Transparent Cutout");
		}

		this.useCurvedRow = useCurvedRow;
		dateCluster = newDateCluster;
		rowActive = true;
		movedLeft = movedRight = false;
		underflowed = overflowed = false;
	}

	public void CreateObjects(GameObject imagePrefab, GameObject textPrefab)
	{
		imageObjects = new GameObject[NUM_IMAGES_VISIBLE];
		imageTextures = new Texture2D[NUM_IMAGES_VISIBLE];

		for (int i = 0; i < NUM_IMAGES_VISIBLE; i++)
		{
			imageObjects[i] = MonoBehaviour.Instantiate(imagePrefab);
			imageTextures[i] = new Texture2D(2, 2, TextureFormat.BC7, false);
		}

		// Setup arrows and text
		leftArrow = MonoBehaviour.Instantiate(imagePrefab);
		rightArrow = MonoBehaviour.Instantiate(imagePrefab);
		dateTextObject = MonoBehaviour.Instantiate(textPrefab);
		infoTextObject = MonoBehaviour.Instantiate(textPrefab);

		// Set info text to be left-aligned
		TextMesh textMesh = infoTextObject.GetComponent<TextMesh>();
		textMesh.alignment = TextAlignment.Left;
		textMesh.anchor = TextAnchor.LowerLeft;

		// Load arrow textures...
		textureLeftArrow = new Texture2D(2, 2, TextureFormat.BC7, false);
		textureRightArrow = new Texture2D(2, 2, TextureFormat.BC7, false);
		string arrowPath = Application.dataPath + "/";
		bool success = Util.LoadPNGWithExistingTexture2D(textureLeftArrow, 
			arrowPath + "LeftArrow.png", false);

		if (!success)
		{
			Debug.Log("Failed to load left arrow image via Util!");
			return;
		}
		else
		{
			Renderer renderer = leftArrow.GetComponent<Renderer>();

			if (renderer != null)
			{
				renderer.material.mainTexture = textureLeftArrow;
				// Change shader into transparent one:
				// Legacy Shaders/Transparent/Cutout/Diffuse
				renderer.material.shader = transparentShader;
			}
		}

		success = Util.LoadPNGWithExistingTexture2D(textureRightArrow, 
			arrowPath + "RightArrow.png", false);

		if (!success)
		{
			Debug.Log("Failed to load right arrow image via Util!");
			return;
		}
		else
		{
			Renderer renderer = rightArrow.GetComponent<Renderer>();

			if (renderer != null)
			{
				renderer.material.mainTexture = textureRightArrow;
				// Change shader into transparent one:
				// Legacy Shaders/Transparent/Cutout/Diffuse
				renderer.material.shader = transparentShader;
			}
		}

		// Set date text
		SetTexts();
		fullyInitialized = true;
	}

	public static float GetImageXPos(int index, bool useCurvedRow = false)
	{
		float baseXPos = IMAGE_SCALE * (index - CENTER_INDEX);
		float shiftedXPos = -IMAGE_SCALE_SMALLER * (index - CENTER_INDEX) * 0.65f;

		if (useCurvedRow)
		{
			float angle = -(baseXPos + shiftedXPos) * CURVED_ROW_SPACING;
			return (float) (CURVED_ROW_RADIUS * Math.Cos(angle + Math.PI / 2.0f));
		} 
		else
		{
			return baseXPos + shiftedXPos;
		}		
	}

	public static float GetImageYPos(int rowPosFromCenter)
	{
		float yDelta = (rowPosFromCenter < 0) ? -0.25f : 0.0f;
		float yDiffImage = 1.35f * rowPosFromCenter * (IMAGE_SCALE - IMAGE_SCALE_SMALLER);
		return IMAGE_SCALE * 0.5f + yDiffImage + yDelta;
	}

	public static float GetImageZPos(int index, bool useCurvedRow = false)
	{
		if (useCurvedRow)
		{
			float baseZPos = IMAGE_SCALE * (index - CENTER_INDEX);
			float shiftedZPos = -IMAGE_SCALE_SMALLER * (index - CENTER_INDEX) * 0.65f;
			float angle = -(baseZPos + shiftedZPos) * CURVED_ROW_SPACING;
			return (float) (CURVED_ROW_RADIUS * Math.Sin(angle + Math.PI / 2.0f)) - 0.2f;
		}
		else
		{
			return 0.2f;
		}
	}

	private void SetImageScales(bool mainRow)
	{
		isMainRow = mainRow;

		float scaleMultiplier = mainRow ? 1.0f : 0.6f;

		for (int i = 0; i < NUM_IMAGES_VISIBLE; ++i)
		{

			int smallMultiplier = Math.Abs(i - CENTER_INDEX);
			float imageScale = IMAGE_SCALE - IMAGE_SCALE_SMALLER * smallMultiplier;
			float scale = imageScale * scaleMultiplier;
			imageObjects[i].transform.localScale = new Vector3(scale, scale, scale);
		}
	}

	public void SetBaseRowPositionAndScale(Transform viveTransform)
	{
		for (int i = 0; i < NUM_IMAGES_VISIBLE; ++i)
		{
			imageObjects[i].layer = Map.IMAGE_BILLBOARD_LAYER; // TODO: Keep this???? Or not...

			imageObjects[i].transform.position = viveTransform.position;

			imageObjects[i].transform.SetParent(viveTransform, true);
			imageObjects[i].transform.localRotation = Quaternion.Euler(0, 0, 0);
			imageObjects[i].transform.Translate(new Vector3(
				GetImageXPos(i, useCurvedRow),
				IMAGE_SCALE * 0.5f,
				GetImageZPos(i, useCurvedRow)));
		}

		// Assume we are always the main row, initially, for scales...
		bool isMainRowBackup = isMainRow;
		SetImageScales(true);
		isMainRow = isMainRowBackup;

		// TODO: Keep this? Or not...
		dateTextObject.layer = Map.IMAGE_BILLBOARD_LAYER;
		infoTextObject.layer = Map.IMAGE_BILLBOARD_LAYER;
		leftArrow.layer = Map.IMAGE_BILLBOARD_LAYER;
		rightArrow.layer = Map.IMAGE_BILLBOARD_LAYER;
		
		dateTextObject.transform.localScale = textScale;
		dateTextObject.transform.position = viveTransform.position;
		infoTextObject.transform.localScale = textScale;
		infoTextObject.transform.position = viveTransform.position;

		// Don't rotate this thing with the image, but instead
		// Keep it horizontally aligned below the image...
		dateTextObject.transform.SetParent(imageObjects[CENTER_INDEX].transform, true);
		dateTextObject.transform.localRotation = Quaternion.Euler(0, 0, 0);

		infoTextObject.transform.SetParent(imageObjects[CENTER_INDEX].transform, true);
		infoTextObject.transform.localRotation = Quaternion.Euler(0, 0, 0);

		// Also adjust arrow positions
		leftArrow.transform.localScale = arrowScale;
		leftArrow.transform.position = viveTransform.position;
		rightArrow.transform.localScale = arrowScale;
		rightArrow.transform.position = viveTransform.position;

		// Adjust arrow positions to left and right positions
		leftArrow.transform.SetParent(viveTransform, true);
		leftArrow.transform.localRotation = Quaternion.Euler(0, 0, 0);
		
		rightArrow.transform.SetParent(viveTransform, true);
		rightArrow.transform.localRotation = Quaternion.Euler(0, 0, 0);
	}

	public void UpdateRowPositionAndScale(int rowPosFromCenter)
	{
		// Position images correctly...

		for (int i = 0; i < NUM_IMAGES_VISIBLE; ++i)
		{			
			imageObjects[i].transform.localPosition = new Vector3(
				GetImageXPos(i, useCurvedRow),
				GetImageYPos(rowPosFromCenter),
				GetImageZPos(i, useCurvedRow));
		}

		// And set the proper scale of the image
		SetImageScales(rowPosFromCenter == 0);
		
		// Great trial-and-error calculation to get it just right...
		float yDelta = rowPosFromCenter.Clamp(-1, 1) * 0.45f;
		float yDiffText = -1.35f * rowPosFromCenter * (IMAGE_SCALE - IMAGE_SCALE_SMALLER) + yDelta;
		float yPos = -1.75f * IMAGE_SCALE + yDiffText;

		// Finally, position the text and arrow objects properly
		dateTextObject.transform.localPosition = new Vector3(
			0.0f,
			yPos,
			0.15f);	

		infoTextBaseLocalPos = new Vector3(
			-1.5f * IMAGE_SCALE,
			yPos - 0.15f,
			0.15f);

		infoTextObject.transform.localPosition = infoTextBaseLocalPos;

		leftArrow.transform.localPosition = new Vector3(
			-0.25f + GetImageXPos(-1, useCurvedRow),
			//IMAGE_SCALE * 0.5f + yDiffImage * 0.95f, 
			GetImageYPos(rowPosFromCenter),
			GetImageZPos(-1, useCurvedRow));

		rightArrow.transform.localPosition = new Vector3(
			0.25f + GetImageXPos(imageObjects.Length, useCurvedRow),
			//IMAGE_SCALE * 0.5f + yDiffImage * 1.05f, 
			GetImageYPos(rowPosFromCenter),
			GetImageZPos(imageObjects.Length, useCurvedRow));

		// TODO: Adjust arrow scales if its not the main row...
	}

	private void SetInfoTextPosition()
	{
		// Count number of newlines so we know what position the info text needs to be...
		if (isMainRow)
		{
			TextMesh textMesh = infoTextObject.GetComponent<TextMesh>();
			int numNewLines = Regex.Matches(textMesh.text, "\n").Count;
			infoTextObject.transform.localPosition = infoTextBaseLocalPos +
				new Vector3(0.0f, -0.1f * (numNewLines + 1), 0.0f);
		}
	}

	public void SetActiveRow(bool active, int numActive = -1)
	{
		// Adjust image scales properly...
		rowActive = active;

		dateTextObject.SetActive(active);
		infoTextObject.SetActive(active);

		// Also set all images and objects to the correct active state
		MarkImagesActive(0, NUM_IMAGES_VISIBLE, 1, active);
		
		leftArrow.SetActive(active);
		rightArrow.SetActive(active);

		if (active)
		{
			if (numActive == -1)
			{
				numActive = dateCluster.stack.GetNumActiveImages();
			}
			
			EnsureValidActiveImages(numActive);

			leftArrow.SetActive(stackIndex >= NUM_IMAGES_VISIBLE_PER_SIDE + 1);
			rightArrow.SetActive(stackIndex + NUM_IMAGES_VISIBLE_PER_SIDE < numActive - 1);
		}
	}

	public void MoveRight()
	{
		// Handle overflow
		++stackIndex;
		EnsureValidStackIndex();
		movedRight = true;
	}

	public void MoveLeft()
	{
		--stackIndex;
		EnsureValidStackIndex();
		movedLeft = true;
	}

	public void EnsureValidStackIndex(int numActive = -1)
	{
		// If numActive wasn't given, get it ourselves
		if (numActive == -1)
		{
			numActive = dateCluster.stack.GetNumActiveImages();
		}

		// Ensure the stack index is <= 0 and < numActive and also
		// determine if underflow or overflow happened
		if (stackIndex < 0)
		{
			underflowed = true;
			stackIndex = numActive - 1;
		}

		if (stackIndex >= numActive)
		{
			overflowed = true;
			stackIndex = 0;
		}
	}

	public bool AtStartOrEndOfRow(int numActive = -1)
	{
		// If numActive wasn't given, get it ourselves
		if (numActive == -1)
		{
			numActive = dateCluster.stack.GetNumActiveImages();
		}

		return stackIndex == 0 || stackIndex == numActive - 1;
	}

	public void SetTexts(int numActive = -1)
	{
		// If numActive wasn't given, get it ourselves
		if (numActive == -1)
		{
			numActive = dateCluster.stack.GetNumActiveImages();
		}

		// Also show the number of images if the row is not active...
		string dateText = dateCluster.date;

		if (!isMainRow)
		{
			dateText += " (" + numActive + ")";
		}
		else
		{
			dateText += " (" + (stackIndex + 1) + " / " + numActive + ")";
		}

		dateTextObject.GetComponent<TextMesh>().text = dateText;

		// Adjust info text for first selected image if we're the main row...
		string infoText = "";

		if (isMainRow)
		{
			// TODO: Add more info, such as GPS lat/long, concepts, etc?
			Image currentImage = dateCluster.stack.GetActiveImage(stackIndex);

			infoText += "Time: " + currentImage.dateTime.Substring(11) + "\n";

			//infoText += "\nFile: " + Path.GetFileName(currentImage.path) + "\n";

			// TODO: Change the python code to generate empty-string names for locations
			// that don't have a name associated, instead of "Nameless location", and then
			// recreate the SQLite database and update this value below...
			if (currentImage.locName != "Nameless location")
			{
				infoText += "GPS name: " + currentImage.locName + "\n";
			}

			double locLat = Math.Round(currentImage.locLat, 4);
			double locLong = Math.Round(currentImage.locLong, 4);
			infoText += "GPS lat/long: " + locLat + ", " + locLong;

			// TODO: Add tags/concepts names?
			List<Tag> tags = Tag.GetTags(tagList, currentImage.tags);
			infoText += "\nTags: " + Tag.ListOfTagsToString(tags);
		}

		infoTextObject.GetComponent<TextMesh>().text = infoText;
	}

	public void SetActiveImage(int arrayIndex, bool active)
	{
		imageObjects[arrayIndex].SetActive(active);
	}

	public void MarkImagesActive(int start, int end, int step, bool active)
	{
		for (int arrayIndex = start; arrayIndex != end; arrayIndex += step)
		{
			SetActiveImage(arrayIndex, active);
		}
	}

	public void EnsureValidActiveImages(int numActive = -1)
	{
		if (!rowActive)
		{
			MarkImagesActive(0, NUM_IMAGES_VISIBLE, 1, false); 
			return;
		}

		if (numActive == -1)
		{
			numActive = dateCluster.stack.GetNumActiveImages();
		}

		for (int arrayIndex = 0; arrayIndex < NUM_IMAGES_VISIBLE; ++arrayIndex)
		{
			int newStackIndex = stackIndex - CENTER_INDEX + arrayIndex;
			bool active = newStackIndex >= 0 && newStackIndex < numActive;
			SetActiveImage(arrayIndex, active);
		}
	}

	private int GetFirstInactiveIndex(int start, int end, int step)
	{
		int arrayIndex = -1;

		for (int i = start; i != end; i += step)
		{
			if (!imageObjects[i].activeSelf)
			{
				arrayIndex = i;
				break;
			}
		}

		return arrayIndex;
	}
	
	public void ReassignTextures()
	{
		for (int i = 0; i < imageObjects.Length; ++i)
		{
			imageObjects[i].GetComponent<Renderer>().material.mainTexture = imageTextures[i];
		}
	}

	public void LoadMultipleNewImages(int start, int end, int step, 
										int numActive = -1, bool markInactive = true)
	{
		if (numActive == -1)
		{
			numActive = dateCluster.stack.GetNumActiveImages();
		}

		// Mark everything inactive, just to be sure
		if (markInactive)
		{
			MarkImagesActive(0, NUM_IMAGES_VISIBLE - 1, 1, false);
		}

		// Load the center image and the right half with images...
		for (int arrayIndex = start; arrayIndex != end; arrayIndex += step)
		{
			int newStackIndex = stackIndex - CENTER_INDEX + arrayIndex;
			LoadNewImage(numActive, newStackIndex, arrayIndex);
		}
	}

	public void LoadNewImage(int numActive, int stackIndex, int arrayIndex)
	{
		if (stackIndex >= 0 && stackIndex < numActive 
		&& arrayIndex >= 0 && arrayIndex < NUM_IMAGES_VISIBLE)
		{
			// Image is active, display it
			Image image = dateCluster.stack.GetActiveImage(stackIndex);
			bool success = Util.LoadPNGWithExistingTexture2D(imageTextures[arrayIndex],
				image.path, false);

			if (!success || imageTextures[arrayIndex] == null)
			{
				Debug.Log("Error loading texture!");
			}
			else
			{
				SetActiveImage(arrayIndex, true);
			}
		}
		else
		{
			// Image is not active, don't display it...
			SetActiveImage(arrayIndex, false);
		}
	}

	public Image GetCurrentImage()
	{
		return dateCluster.stack.GetActiveImage(stackIndex);
	}

	public void UpdateImageRow()
	{
		// Get number of active images, so we only load those...
		int numActive = dateCluster.stack.GetNumActiveImages();
		EnsureValidStackIndex(numActive);

		// If we're loading just 1 new image, set this to a valid index
		int arrayIndexToLoad = -1;

		// Update textures...
		if (movedLeft)
		{
			if (underflowed)
			{
				// We underflowed, this means stackIndex is now at numActive - 1
				stackIndex = numActive - 1;

				// So we are now at the right end of the image row...
				// Load only left half of the images
				LoadMultipleNewImages(0, CENTER_INDEX + 1, 1, numActive, true);
			}
			else
			{
				// We moved left, so we need to rotate the texture array to the right
				Util.RotateTextureArrayRight(imageTextures);
				
				// Load new texture on the first inactive left side or far left...
				arrayIndexToLoad = GetFirstInactiveIndex(CENTER_INDEX, -1, -1);
				arrayIndexToLoad = (arrayIndexToLoad == -1) ? 0 : arrayIndexToLoad;
			}			
		}

		if (movedRight)
		{
			if (overflowed)
			{
				// We overflowed, this means stackIndex is now at 0
				stackIndex = 0;

				// So we are now at the left end of the image row
				// Load only left half of the images
				LoadMultipleNewImages(CENTER_INDEX, NUM_IMAGES_VISIBLE, 1, numActive, true);
			}
			else
			{
				// We moved right, so we need to rotate the texture array to the left
				Util.RotateTextureArrayLeft(imageTextures);

				// Load new texture on the first inactive right side or far right...
				arrayIndexToLoad = GetFirstInactiveIndex(CENTER_INDEX, NUM_IMAGES_VISIBLE, 1);
				arrayIndexToLoad = (arrayIndexToLoad == -1) ? NUM_IMAGES_VISIBLE - 1 : arrayIndexToLoad;
			}			
		}

		// Make sure that the left and right arrows are set active accordingly...
		leftArrow.SetActive(rowActive && stackIndex >= NUM_IMAGES_VISIBLE_PER_SIDE + 1);
		rightArrow.SetActive(rowActive && stackIndex + NUM_IMAGES_VISIBLE_PER_SIDE < numActive - 1);

		// Load new image if we have to
		if (arrayIndexToLoad != -1)
		{
			int newStackIndex = stackIndex - CENTER_INDEX + arrayIndexToLoad;
			LoadNewImage(numActive, newStackIndex, arrayIndexToLoad);
		}		

		// Ensure the right images are active etc, and reset states
		EnsureValidActiveImages(numActive);
		ReassignTextures();
		SetTexts(numActive);
		SetInfoTextPosition();
		movedLeft = movedRight = false;
		underflowed = overflowed = false;
	}

	public void Destroy()
	{
		if (imageObjects != null && imageTextures != null)
		{
			for (int i = 0; i < imageObjects.Length; ++i)
			{
				MonoBehaviour.Destroy(imageObjects[i]);
				MonoBehaviour.Destroy(imageTextures[i]);
			}
		}
		
		MonoBehaviour.Destroy(dateTextObject);
		MonoBehaviour.Destroy(infoTextObject);
		MonoBehaviour.Destroy(leftArrow);
		MonoBehaviour.Destroy(rightArrow);
		MonoBehaviour.Destroy(dateTextObject);
		MonoBehaviour.Destroy(textureLeftArrow);
		MonoBehaviour.Destroy(textureRightArrow);
	}
}
