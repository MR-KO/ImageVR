﻿using UnityEngine;
using System.Collections;

public struct ImageStruct
{
	// TODO: Create Variable Positioning according to zoom levels
	public Vector2 pos { get; set; } // tile positions in order (14,16,18)

	public ClusterLocation cluster { get; set; }


    /// <summary>
    /// Image stored with positions
    /// </summary>
    /// <param name="pos">x and y tile at zoom 19</param>
    public ImageStruct(Vector2 pos, ClusterLocation cluster)
    {
        this.pos = pos;
		this.cluster = cluster;
    }
}