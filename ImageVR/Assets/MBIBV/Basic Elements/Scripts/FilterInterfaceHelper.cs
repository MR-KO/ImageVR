﻿using UnityEngine;
using System.Collections.Generic;
using Endgame;

// Base class to handle listViews...
public class ListViewHandler
{
	[Header("ListView")]
	[Tooltip("listview for this object")]
	public ListView listView;

	public int selectedIndex = 0;

	public void AddColumn(string text, int width = -1)
	{
		ColumnHeader columnHeader = new ColumnHeader();
		columnHeader.Text = text;
		columnHeader.Width = width; // Set column width to longest item...
		listView.Columns.Add(columnHeader);
	}

	protected ListViewItem CreateItem(Object obj)
	{
		string[] itemTexts = new string[]
			{
				(obj == null) ? "null" : obj.ToString()
			};

		ListViewItem item = new ListViewItem(itemTexts);
		item.Tag = obj;
		return item;
	}

	public object GetSelectedItem()
	{
		// Try getting the selected item by selectedItems
		if (listView.SelectedItems.Count <= 0)
		{
			Debug.Log("No item selected! selectedIndex = " + selectedIndex);
			return null;
		}

		// Try getting the selected item by selectedIndices...
		if (listView.SelectedIndices.Count <= 0)
		{
			Debug.Log("Nothing selected!");
			return null;
		}

		//selectedIndex = this.listView.SelectedIndices[0];

		if (selectedIndex >= 0 && selectedIndex < listView.Items.Count)
		{
			return listView.Items[selectedIndex].Tag;
		}

		return null;
	}

	public void SelectItem(bool clearPreviousSelections = true)
	{
		if (selectedIndex < 0 || selectedIndex >= listView.Items.Count)
		{
			Debug.Log("Invalid index of " + selectedIndex + " given to SelectItem!");
			return;
		}

		// Unselect previous items and select new one
		if (clearPreviousSelections)
		{
			ClearSelections();
		}

		ListViewItem item = listView.Items[selectedIndex];

		if (item == null)
		{
			Debug.Log("Item at selectedIndex " + selectedIndex + " is null!");
			return;
		}

		item.Selected = true;

		// Following is not needed, already done implicitly by setting item.Selected
		// listView.SelectedIndices.Add(item.Index); // And not selectedIndex...

		// Position scrollbar...
		//listView.SetVerticalScrollBarValue((float)selectedIndex / listView.Items.Count);
		listView.ScrollToEnsureItemVisible(selectedIndex);
	}

	public void ClearSelections()
	{
		listView.SelectedIndices.Clear();
	}

	public virtual void UpdateSelectedItem()
	{

	}
}



public class TagListFilterHandler : ListViewHandler
{
	public List<Tag> tagListOccurrenceSorted = null;
	public List<Tag> tagListAlphabeticallySorted = null;
	public bool useOccurrenceSort = true;

	public static string ACTIVE_TEXT = "Yes";
	public static string INACTIVE_TEXT = "No";

	public void Initialize(List<Tag> tagListSortedAlphabetically, List<Tag> tagListSortedByOccurrences)
	{
		// Setup ListView and add all items to it
		listView.SuspendLayout();

		// Add columns for the Tag/Concept name and enabled/disabled status
		AddColumn("Tag name", -1);
		AddColumn("Active?", 70);

		tagListOccurrenceSorted = tagListSortedByOccurrences;
		tagListAlphabeticallySorted = tagListSortedAlphabetically;
		SetListViewAlphabetically(false);
		listView.ResumeLayout();
	}

	public void SetListViewAlphabetically(bool suspendLayout = true)
	{
		useOccurrenceSort = false;

		if (suspendLayout)
		{
			listView.SuspendLayout();
		}

		listView.Items.Clear();

		foreach (Tag tag in tagListAlphabeticallySorted)
		{
			listView.Items.Add(CreateItem(tag));
		}

		if (suspendLayout)
		{
			listView.ResumeLayout();
		}
	}

	public void SetListViewOccurrences(bool suspendLayout = true)
	{
		useOccurrenceSort = true;

		if (suspendLayout)
		{
			listView.SuspendLayout();
		}

		listView.Items.Clear();

		foreach (Tag tag in tagListOccurrenceSorted)
		{
			listView.Items.Add(CreateItem(tag));
		}

		if (suspendLayout)
		{
			listView.ResumeLayout();
		}
	}

	protected ListViewItem CreateItem(Tag tag)
	{
		string[] itemTexts = new string[]
			{
				tag.name + " (" + tag.occurrences + ")",
				tag.enabled ? ACTIVE_TEXT : INACTIVE_TEXT
			};

		// TODO: Update filter interface item color based on tag.enabled...
		ListViewItem listViewItem = new ListViewItem(itemTexts);
		listViewItem.Tag = tag;
		return listViewItem;
	}

	public new Tag GetSelectedItem()
	{
		object obj = base.GetSelectedItem();

		if (obj != null)
		{
			return obj as Tag;
		}

		return null;
	}

	public int FindTagThatStartsWith(string letter)
	{
		if (useOccurrenceSort)
		{
			return tagListOccurrenceSorted.FindIndex(x => x.name.StartsWith(letter));
		}
		else
		{
			return tagListAlphabeticallySorted.FindIndex(x => x.name.StartsWith(letter));
		}
	}

	public override void UpdateSelectedItem()
	{
		//if (selectedIndex >= 0 && selectedIndex < tagListSorted.Count)
		if (listView.SelectedItems.Count <= 0)
		{
			Debug.Log("No item selected! selectedIndex = " + selectedIndex);
			return;
		}

		//Debug.Log("Got selectedIndex: " + selectedIndex);
		ListViewItem item = listView.SelectedItems[0];

		if (item == null)
		{
			Debug.Log("Got null item at index " + selectedIndex + "!");
			return;
		}

		Tag tag = GetSelectedItem();

		if (tag == null)
		{
			Debug.Log("No item selected!");
			return;
		}

		UpdateAllTags();
		SelectItem();
	}

	public void UpdateAllTags(bool suspendLayout = true)
	{
		// "Update" tags by deleting them and re-adding them...
		if (useOccurrenceSort)
		{
			SetListViewOccurrences(suspendLayout);
		}
		else
		{
			SetListViewAlphabetically(suspendLayout);
		}
	}
}



public class AToZListHandler : ListViewHandler
{
	public static string[] letters = {
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L",
		"M", "N", "O", "P", "R", "S", "T", "U", "V", "W", "Y",};

	public void Initialize()
	{
		// Setup ListView and add all items to it
		listView.SuspendLayout();

		// Add columns for the Tag/Concept name and enabled/disabled status
		AddColumn("Index", 100); // TODO: FIne-tune magic number

		foreach (string letter in letters)
		{
			listView.Items.Add(CreateItem(letter));
		}

		listView.ResumeLayout();
	}

	protected ListViewItem CreateItem(string letter)
	{
		string[] itemTexts = new string[] { letter };

		// TODO: Update filter interface item color based on tag.enabled...
		ListViewItem listViewItem = new ListViewItem(itemTexts);
		listViewItem.Tag = letter;
		return listViewItem;
	}

	public new string GetSelectedItem()
	{
		object obj = base.GetSelectedItem();

		if (obj != null)
		{
			return obj as string;
		}

		return null;
	}
}

public class ExtraOptionsHandler : ListViewHandler
{
	public static string taglessOptionShow = "Show images without tags";
	public static string taglessOptionHide = "Hide images without tags";

	public static string allTagsOptionShow = "Show all images with tags";
	public static string allTagsOptionHide = "Hide all images with tags";

	public enum Option { EnableDisableAllTags, EnableDisableNoTags,
		AlphabeticalSort, OccurrenceSort, None };
	public static string[] optionTexts = {
		allTagsOptionHide,
		taglessOptionHide,
		"Sort tags alphabetically",
		"Sort tags by occurrences",
		"HERPDERPMURT"
	};

	public static Option[] options = { Option.EnableDisableAllTags,
		Option.EnableDisableNoTags, Option.AlphabeticalSort, Option.OccurrenceSort };

	public static Tag allTags = null;
	public static Tag noTags = null;

	public void Initialize()
	{
		// Setup ListView and add all items to it
		listView.SuspendLayout();

		// Add columns for the Tag/Concept name and enabled/disabled status
		AddColumn("Preferences", 185);

		foreach (Option option in options)
		{
			listView.Items.Add(CreateItem(optionTexts[(int)option]));
		}

		SetNoTagsText();
		SetAllTagsText();

		listView.ResumeLayout();
	}

	protected ListViewItem CreateItem(string option)
	{
		string[] itemTexts = new string[] { option };

		// TODO: Update filter interface item color based on tag.enabled...
		// TODO: Update item text for tagless options?
		ListViewItem listViewItem = new ListViewItem(itemTexts);

		if (option == optionTexts[0])
		{
			listViewItem.Tag = allTags;
		}
		else if (option == optionTexts[1])
		{
			listViewItem.Tag = noTags;
		}
		else
		{
			listViewItem.Tag = option;
		}

		return listViewItem;
	}

	public new Option GetSelectedItem()
	{
		object obj = base.GetSelectedItem();

		if (obj != null)
		{
			string text = obj as string;

			if (text != null)
			{
				// Get option based on selected text...
				for (int i = 0; i < optionTexts.Length - 1; i++)
				{
					if (optionTexts[i] == text)
					{
						return options[i];
					}
				}
			}

			// Handle special case where it's a Tag
			Tag tag = obj as Tag;

			if (tag != null)
			{
				if (tag.id == Map.TAG_ID_REPRESENTING_EVERYTHING)
				{
					return Option.EnableDisableAllTags;
				}

				if (tag.id == Map.TAG_ID_REPRESENTING_NO_TAGS)
				{
					return Option.EnableDisableNoTags;
				}
			}
		}

		return Option.None;
	}

	public void SetNoTagsText()
	{
		foreach (ListViewItem item in listView.Items)
		{
			if ((Tag)item.Tag == noTags)
			{
				if (noTags.enabled)
				{
					item.Text = taglessOptionHide;
				}
				else
				{
					item.Text = taglessOptionShow;
				}

				return;
			}
		}
	}

	public void SetAllTagsText()
	{
		foreach (ListViewItem item in listView.Items)
		{
			if ((Tag)item.Tag == allTags)
			{
				if (allTags.enabled)
				{
					item.Text = allTagsOptionHide;
				}
				else
				{
					item.Text = allTagsOptionShow;
				}

				return;
			}
		}
	}
}
