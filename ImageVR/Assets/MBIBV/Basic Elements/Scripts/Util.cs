﻿using UnityEngine;

using System;
using System.IO;


public static class Util
{
	public static int resizeWidth = 128;
	public static int resizeHeight = 128;

	public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
	{
		if (val.CompareTo(min) < 0)
		{
			return min;
		}
		else
		{
			if (val.CompareTo(max) > 0)
			{
				return max;
			}
			else
			{
				return val;
			}
		}		
	}

	public static float ClampFloat(float val, float min, float max)
	{
		if (val.CompareTo(min) < 0)
		{
			return min;
		}
		else
		{
			if (val.CompareTo(max) > 0)
			{
				return max;
			}
			else
			{
				return val;
			}
		}
	}

	public static float ClampFloat2(float val, float min, float max)
	{
		if (val < min)
		{
			return min;
		}
		else
		{
			return (val > max) ? max : val;
		}
	}

	private static Color ColorLerpUnclamped(Color c1, Color c2, float value)
	{
		return new Color(c1.r + (c2.r - c1.r) * value,
						  c1.g + (c2.g - c1.g) * value,
						  c1.b + (c2.b - c1.b) * value,
						  c1.a + (c2.a - c1.a) * value);
	}

	public static bool LoadPNGWithExistingTexture2D(Texture2D tex, string filePath, bool resize)
	{
		if (tex == null)
		{
			return false;
		}

		if (File.Exists(filePath))
		{
			byte[] fileData = File.ReadAllBytes(filePath);

			// BC7 is a DX11-level compression of textures (8 bits per pixel 
			// instead of 32 for RGBA, common for PNGs).
			// Also, mipmapping is not needed for these textures.
			tex.LoadImage(fileData); //..this will auto-resize the texture dimensions

			if (resize)
			{
				int oldWidth = tex.width;
				int oldHeight = tex.height;
				Color[] pixels = tex.GetPixels();
				Color[] newPixels = Util.Resize(pixels, 
					oldWidth, oldHeight, Util.resizeWidth, Util.resizeHeight, true);
				Util.ApplyResize(tex, newPixels, Util.resizeWidth, Util.resizeHeight);
			}

			return true;
		}

		return false;
	}

	public static Texture2D LoadPNG(string filePath, bool resize)
	{
		Texture2D tex = null;

		if (File.Exists(filePath))
		{
			byte[] fileData = File.ReadAllBytes(filePath);

			// BC7 is a DX11-level compression of textures (8 bits per pixel 
			// instead of 32 for RGBA, common for PNGs).
			// Also, mipmapping is not needed for these textures.
			tex = new Texture2D(2, 2, TextureFormat.BC7, false); 
			tex.LoadImage(fileData); //..this will auto-resize the texture dimensions

			if (resize)
			{
				int oldWidth = tex.width;
				int oldHeight = tex.height;
				Color[] pixels = tex.GetPixels();
				Color[] newPixels = Util.Resize(pixels, 
					oldWidth, oldHeight, Util.resizeWidth, Util.resizeHeight, true);
				Util.ApplyResize(tex, newPixels, Util.resizeWidth, Util.resizeHeight);
			}
		}

		return tex;
	}

	public static void ApplyResize(Texture2D texture, Color[] newPixels, 
									int newWidth, int newHeight, int mipmaps = 0)
	{
		texture.Resize(newWidth, newHeight);
		texture.SetPixels(newPixels, mipmaps);
		texture.Apply(mipmaps > 0, true);
	}
	
	// Only works on ARGB32, RGB24 and Alpha8 textures that are marked readable
	public static Color[] Resize(Color[] texColors, 
									int oldWidth, int oldHeight, 
									int newWidth, int newHeight, bool useBilinear)
	{
		float ratioX;
		float ratioY;

		Color[] newColors = new Color[newWidth * newHeight];

		if (useBilinear)
		{
			ratioX = 1.0f / ((float)newWidth / (oldWidth - 1));
			ratioY = 1.0f / ((float)newHeight / (oldHeight - 1));
		}
		else
		{
			ratioX = ((float)oldWidth) / newWidth;
			ratioY = ((float)oldHeight) / newHeight;
		}

		int w = oldWidth;
		int w2 = newWidth;

		if (useBilinear)
		{
			for (var y = 0; y < newHeight; y++)
			{
				int yFloor = (int)Mathf.Floor(y * ratioY);
				var y1 = yFloor * w;
				var y2 = (yFloor + 1) * w;
				var yw = y * w2;

				for (var x = 0; x < w2; x++)
				{
					int xFloor = (int)Mathf.Floor(x * ratioX);
					var xLerp = x * ratioX - xFloor;
					newColors[yw + x] = ColorLerpUnclamped(
						ColorLerpUnclamped(texColors[y1 + xFloor], 
							texColors[y1 + xFloor + 1], xLerp),								
						ColorLerpUnclamped(texColors[y2 + xFloor], 
							texColors[y2 + xFloor + 1], xLerp),
						y * ratioY - yFloor);
				}
			}
		}
		else
		{
			for (var y = 0; y < newHeight; y++)
			{
				var thisY = (int)(ratioY * y) * w;
				var yw = y * w2;
				for (var x = 0; x < w2; x++)
				{
					newColors[yw + x] = texColors[(int)(thisY + ratioX * x)];
				}
			}
		}

		// Skip this...
		//tex.Resize(newWidth, newHeight);
		//tex.SetPixels(newColors);
		//tex.Apply();

		texColors = null;
		return newColors;
	}

	public static void RotateTextureArrayLeft(Texture2D[] array)
	{
		Texture2D first = array[0];

		for (int i = 1; i < array.Length; ++i)
		{
			array[i - 1] = array[i];
		}

		array[array.Length - 1] = first;
	}

	public static void RotateTextureArrayRight(Texture2D[] array)
	{
		Texture2D last = array[array.Length - 1];

		for (int i = array.Length - 1; i > 0; --i)
		{
			array[i] = array[i - 1];
		}
		
		array[0] = last;
	}
}