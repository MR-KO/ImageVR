﻿using UnityEngine;
using System.Collections.Generic;

public class ImageStack
{
	public List<Image> images;

	public ImageStack(int capacity = 1)
	{
		images = new List<Image>(capacity);
	}

	public List<Image> GetActiveImages()
	{
		return Image.GetActiveImages(images);
	}

	public int GetNumActiveImages()
	{
		List<Image> temp = GetActiveImages();
		return (temp == null) ? 0 : temp.Count;
	}

	public int GetNumImages()
	{
		return images.Count;
	}

	public Image GetImage(int index = 0)
	{
		return (images.Count > index) ? images[index] : null;
	}

	public Image GetActiveImage(int index = 0)
	{
		List<Image> activeImages = GetActiveImages();

		if (activeImages == null || activeImages.Count <= index)
		{
			return null;
		}

		return activeImages[index];
	}
}
