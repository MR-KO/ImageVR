﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
public class ReverseNormals : MonoBehaviour 
{
	public bool inverted;
	public GameObject pinPrefab;
	float sphereRadius;
	public Texture worldmap;
	public Texture invertedmap;

	void Start ()
	{
		sphereRadius = 10f;

		if (inverted) {
			GetComponent<Renderer>().material.mainTexture = worldmap;
			FlipTiles ();
            gameObject.AddComponent<MeshCollider>();
        } 
		else 
		{
			GetComponent<Renderer>().material.mainTexture = worldmap;
		}
		//LatLongTest ();
	}

	public void FlipTiles()
	{
		MeshFilter filter = GetComponent (typeof(MeshFilter)) as MeshFilter;
		if (filter != null) {
			Mesh mesh = filter.mesh;

			Vector3[] normals = mesh.normals;
			for (int i = 0; i < normals.Length; i++)
				normals [i] = -normals [i];
			mesh.normals = normals;

			for (int m = 0; m < mesh.subMeshCount; m++) {
				int[] triangles = mesh.GetTriangles (m);
				for (int i = 0; i < triangles.Length; i += 3) {
					int temp = triangles [i + 0];
					triangles [i + 0] = triangles [i + 1];
					triangles [i + 1] = temp;
				}
				mesh.SetTriangles (triangles, m);
			}
		}
		transform.localRotation = Quaternion.Euler (180, 0, 0);
	}

	public void LatLongTest()
	{

		// London
		GameObject p = GameObject.Instantiate (pinPrefab);
		p.transform.position = LatLongToWorld (0, 50, sphereRadius);
		p.GetComponent<Renderer> ().material.color = Color.green;
		// Rotate normal
		Vector3 normal = p.transform.position;
		p.transform.rotation = Quaternion.FromToRotation (-Vector3.forward, normal);


		// NY
		p = GameObject.Instantiate (pinPrefab);
		p.transform.position = LatLongToWorld (50, 0, sphereRadius);
		normal = p.transform.position;
		p.transform.rotation = Quaternion.FromToRotation (-Vector3.forward, normal);
		p.GetComponent<Renderer> ().material.color = Color.red;
		// Canberra Australia
		p = GameObject.Instantiate (pinPrefab);
		p.transform.position = LatLongToWorld (50,50, sphereRadius);
		p.GetComponent<Renderer> ().material.color = Color.cyan;
		normal = p.transform.position;
		p.transform.rotation = Quaternion.FromToRotation (-Vector3.forward, normal);

		p = GameObject.Instantiate (pinPrefab);
		p.transform.position = LatLongToWorld (0, 0, sphereRadius);
		p.GetComponent<Renderer> ().material.color = Color.yellow;
		normal = p.transform.position;
		p.transform.rotation = Quaternion.FromToRotation (-Vector3.forward, normal);
	}

	public static Vector2 FromVector3(Vector3 position, float radius)
		{
		float lat = Mathf.Acos(position.y / radius); //theta
			float lon = Mathf.Atan(position.x / position.z); //phi
		return new Vector2(lat, lon);
		}

    //public static Vector3 LatLongToWorld(double latitude, double longitude, float radius)
    //{
    //	float thetha = (float)latitude*Mathf.Deg2Rad;
    //	float phi = (float)longitude*Mathf.Deg2Rad;

    //	float lat = (float)latitude*Mathf.Deg2Rad;
    //	float lon = (float)longitude*Mathf.Deg2Rad;
    //	float x = radius * Mathf.Cos(lat)*Mathf.Cos(lon);
    //	float z = radius * Mathf.Cos(lat)*Mathf.Sin(lon);
    //	float y = radius * Mathf.Sin(lat);
    //	return new Vector3(x,y,z);
    //} 

    public static Vector3 LatLongToWorld(double latitude, double longitude, float radius, bool inverted = true)
    {
        if (inverted)
        {
            longitude = -longitude;
        }
        float lat = (float)latitude * Mathf.Deg2Rad;
        float lon = (float)longitude * Mathf.Deg2Rad;
        float x = radius * Mathf.Cos(lat) * Mathf.Cos(lon);
        float z = radius * Mathf.Cos(lat) * Mathf.Sin(lon);
        float y = radius * Mathf.Sin(lat);

        //if (inverted)
        //{
        //    y = -y;
        //}
        return new Vector3(x, y, z);
    }

		//float x = radius * Mathf.Sin (thetha) * Mathf.Cos (phi);
		//float y = radius * Mathf.Sin (phi);
		//float z = radius * Mathf.Cos (thetha) * Mathf.Cos (phi);

}

