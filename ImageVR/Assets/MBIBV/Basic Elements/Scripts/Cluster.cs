using System.Collections.Generic;
using UnityEngine;

public class ClusterHandler
{
	// First level is a dictionary of location clusters, with they key being
	// "latitude, longitude" values as a string.
	public Dictionary<string, ClusterLocation> locationClusters;

	// Creates a full clustering thing
	public ClusterHandler(List<Image> images, int numDecimals = 6, int numLevels = 3)
	{
		locationClusters = new Dictionary<string, ClusterLocation>();

		bool useDateLvl = numLevels >= 1;
		bool useHourLvl = numLevels >= 2;
		bool useTenMinLvl = numLevels >= 3;

		foreach (Image image in images)
		{
			// Get the latitude and longitude
			double latitude = image.locLat;
			double longitude = image.locLong;

			// Only round down if numDecimals is in a valid range
			if (numDecimals >= 0 && numDecimals <= 8)
			{
				latitude = System.Math.Round(image.locLat, numDecimals);
				longitude = System.Math.Round(image.locLong, numDecimals);
			}

			// Check if a corresponding cluster exists
			string key = "" + latitude + ", " + longitude;
			ClusterLocation foundLocation = null;

			// If not found, create it
			if (!locationClusters.TryGetValue(key, out foundLocation))
			{
				foundLocation = new ClusterLocation(latitude, longitude);
				locationClusters.Add(key, foundLocation);
			}

			// Add image to the found location cluster
			foundLocation.stack.images.Add(image);

			// Skip date level if not needed
			if (!useDateLvl) {
				continue;
			}

			// Get date and find corresponding date cluster in found location cluster
			string date = image.dateTime.Substring(0, 10);
			ClusterDate foundDate = null;

			if (!foundLocation.dateClusters.TryGetValue(date, out foundDate))
			{
				foundDate = new ClusterDate(date);
				foundLocation.dateClusters.Add(date, foundDate);
			}

			// Add image to the found date cluster
			foundDate.stack.images.Add(image);

			// Skip hour level if not needed
			if (!useHourLvl) {
				continue;
			}

			// Get hour and find corresponding hour cluster in found date cluster
			string hour = image.dateTime.Substring(11, 2);
			ClusterHour foundHour = null;

			if (!foundDate.hourClusters.TryGetValue(hour, out foundHour))
			{
				foundHour = new ClusterHour(hour);
				foundDate.hourClusters.Add(hour, foundHour);
			}

			// Add image to the found hour cluster
			foundHour.stack.images.Add(image);

			// Skip ten minute level if not needed
			if (!useTenMinLvl) {
				continue;
			}

			// Get minute and find corresponding 10-minute period cluster
			int minute = System.Int32.Parse((image.dateTime.Substring(14, 2)));
			key = ClusterTenMin.GetTenMin(minute);
			ClusterTenMin foundTenMin = null;

			if (!foundHour.tenMinClusters.TryGetValue(key, out foundTenMin))
			{
				foundTenMin = new ClusterTenMin(key);
				foundHour.tenMinClusters.Add(key, foundTenMin);
			}

			// Add image to the found ten minute cluster
			foundTenMin.stack.images.Add(image);
		}
	}

	public void SetAllClusterSizes()
	{
		foreach (KeyValuePair<string, ClusterLocation> kvpLoc in locationClusters)
		{
			kvpLoc.Value.SetClusterSize();

			foreach (KeyValuePair<string, ClusterDate> kvpDate in kvpLoc.Value.dateClusters)
			{
				kvpDate.Value.SetClusterSize();

				foreach (KeyValuePair<string, ClusterHour> kvpHour in kvpDate.Value.hourClusters)
				{
					kvpHour.Value.SetClusterSize();

					foreach (KeyValuePair<string, ClusterTenMin> kvpTenMin in kvpHour.Value.tenMinClusters)
					{
						kvpTenMin.Value.SetClusterSize();
					}
				}
			}
		}

		return;
	}

	public int GetNumClusters()
	{
		return locationClusters.Count;
	}
}


public enum ClusterSize { BigFuckingCluster, Large, Normal, Small, Tiny };


public static class ClusterScale
{
	public static Vector3 BigFuckingScale 	= new Vector3(0.8f, 0.8f, 0.8f);
	public static Vector3 LargeScale 		= new Vector3(0.6f, 0.6f, 0.6f);
	public static Vector3 NormalScale 		= new Vector3(0.4f, 0.4f, 0.4f);
	public static Vector3 SmallScale 		= new Vector3(0.25f, 0.25f, 0.25f);
	public static Vector3 TinyScale 		= new Vector3(0.15f, 0.15f, 0.15f);

	public static Vector3 GetClusterScale(ClusterSize size)
	{
		switch (size)
		{
			case ClusterSize.BigFuckingCluster:
				return BigFuckingScale;

			case ClusterSize.Large:
				return LargeScale;

			case ClusterSize.Normal:
				return NormalScale;

			case ClusterSize.Small:
				return SmallScale;

			case ClusterSize.Tiny:
			default:
				return TinyScale;
		}
	}
}


interface IClusterHierarchy
{
	Dictionary<string, ClusterBase> GetNextClusters();
	string GetKey();
	int GetNumClusters();
}


public class ClusterBase : IClusterHierarchy
{
	public ClusterSize size;
	public ImageStack stack { get; set; }
	public static int[] ClusterSizePerStackSize = { 250, 100, 50, 10, 1 }; // >= read respectively for each ClusterSize

	public static ClusterSize[] sizes = {
		ClusterSize.BigFuckingCluster,
		ClusterSize.Large,
		ClusterSize.Normal,
		ClusterSize.Small,
		ClusterSize.Tiny
	};

	public ClusterBase()
	{
		stack = new ImageStack();
	}

	// TODO: Make this dynamic by calling this function often and setting its
	// size according to GetNumActiveImages() ?
	public void SetClusterSize()
	{
		int numImages = -1;

		if (stack != null)
		{
			// Determine cluster size based on number of images in cluster
			numImages = stack.GetNumActiveImages();

			foreach (ClusterSize clusterSize in ClusterBase.sizes)
			{
				if (numImages >= ClusterSizePerStackSize[(int)clusterSize])
				{
					size = clusterSize;
					return;
				}
			}
		}

		Debug.Log("Hmmm, this is interesting... numImages = " + numImages);
		size = ClusterSize.Tiny;
	}

	public virtual Dictionary<string, ClusterBase> GetNextClusters()
	{
		return null;
	}

	public virtual string GetKey()
	{
		return "";
	}

	public virtual int GetNumClusters()
	{
		return 0;
	}
}


public class ClusterLocation : ClusterBase
{
	public double locLat { get; set; }
	public double locLong { get; set; }
	public Dictionary<string, ClusterDate> dateClusters;

	public ClusterLocation(double latitude, double longitude) : base()
	{
		locLat = latitude;
		locLong = longitude;
		dateClusters = new Dictionary<string, ClusterDate>();
	}

	public override int GetNumClusters()
	{
		return dateClusters.Count;
	}
	
	public override string GetKey()
	{
		return "" + locLat + ", " + locLong;
	}

	public override Dictionary<string, ClusterBase> GetNextClusters()
	{
		Dictionary<string, ClusterBase> nextClusters = new Dictionary<string, ClusterBase>(dateClusters.Count);

		foreach (KeyValuePair<string, ClusterDate> kvp in dateClusters)
		{
			nextClusters.Add(kvp.Key, (ClusterBase)kvp.Value);
		}

		return nextClusters;
	}
}

public class ClusterDate : ClusterBase
{
	public string date { get; set; }
	public Dictionary<string, ClusterHour> hourClusters;

	public ClusterDate(string date) : base()
	{
		this.date = date;
		hourClusters = new Dictionary<string, ClusterHour>();
	}

	public override int GetNumClusters()
	{
		return hourClusters.Count;
	}

	public override string GetKey()
	{
		return date;
	}

	public override Dictionary<string, ClusterBase> GetNextClusters()
	{
		Dictionary<string, ClusterBase> nextClusters = new Dictionary<string, ClusterBase>(hourClusters.Count);

		foreach (KeyValuePair<string, ClusterHour> kvp in hourClusters)
		{
			nextClusters.Add(kvp.Key, (ClusterBase)kvp.Value);
		}

		return nextClusters;
	}
}

public class ClusterHour : ClusterBase
{
	public string hour { get; set; }
	public Dictionary<string, ClusterTenMin> tenMinClusters;

	public ClusterHour(string hour) : base()
	{
		this.hour = hour;
		tenMinClusters = new Dictionary<string, ClusterTenMin>();
	}

	public override int GetNumClusters()
	{
		return tenMinClusters.Count;
	}

	public override string GetKey()
	{
		return hour;
	}

	public override Dictionary<string, ClusterBase> GetNextClusters()
	{
		Dictionary<string, ClusterBase> nextClusters = new Dictionary<string, ClusterBase>(tenMinClusters.Count);

		foreach (KeyValuePair<string, ClusterTenMin> kvp in tenMinClusters)
		{
			nextClusters.Add(kvp.Key, (ClusterBase)kvp.Value);
		}

		return nextClusters;
	}
}

public class ClusterTenMin : ClusterBase
{
	public string minute { get; set; }

	public ClusterTenMin(string minute) : base()
	{
		this.minute = minute;
	}

	public static string GetTenMin(int minute)
	{
		if (minute >= 0 && minute <= 9) {
			return "0 - 9";
		}

		if (minute >= 10 && minute <= 19) {
			return "10 - 19";
		}

		if (minute >= 20 && minute <= 29) {
			return "20 - 29";
		}

		if (minute >= 30 && minute <= 39) {
			return "30 - 39";
		}

		if (minute >= 40 && minute <= 49) {
			return "40 - 49";
		}

		if (minute >= 50 && minute <= 59) {
			return "50 - 59";
		}

		return "Invalid minute!";
	}

	public override string GetKey()
	{
		return minute;
	}

	public override int GetNumClusters()
	{
		return 0;
	}

	public override Dictionary<string, ClusterBase> GetNextClusters()
	{
		return null;
	}
}
