using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum ImageWallState { Loading, Done, PlayedSound };

public class ImageWallNav
{
	// TODO: Change ClusterLocation into ClusterBase to allow for
	// visualizing different levels of clusters in the wall nav?
	public ClusterLocation cluster;
	public ImageWallRow[] imageRows;
	private bool useCurvedImageWall = false;
	public int currentRowIndex = 0; // 0 = visual bottom row
	public bool underflowed = false;
	public bool overflowed = false;
	public ImageWallState state = ImageWallState.Loading;

	// TODO: Add and fix top and bottom arrows!!
	private GameObject topArrow;
	private GameObject bottomArrow;
	private Texture2D textureTopArrow;
	private Texture2D textureBottomArrow;

	public static readonly int NUM_ROWS_VISIBLE_PER_SIDE = 1;
	public static readonly int NUM_ROWS_VISIBLE = NUM_ROWS_VISIBLE_PER_SIDE * 2 + 1;
	//public static readonly int CENTER_ROW_INDEX = NUM_ROWS_VISIBLE_PER_SIDE; // Just for better readable code

	public static readonly Vector3 arrowScale = new Vector3(0.1f, 0.1f, 0.1f);
	private static Shader transparentShader = null;


	public ImageWallNav(GameObject imagePrefab, GameObject textPrefab,
						ClusterLocation locCluster, bool useCurvedWall = false)
	{
		// Setup shader and cluster shit
		if (transparentShader == null)
		{
			transparentShader = Shader.Find("Unlit/Transparent Cutout");
		}

		useCurvedImageWall = useCurvedWall;
		cluster = locCluster;

		// Determine how many clusters have active images...
		int numActiveClusters = 0;

		foreach (KeyValuePair<string, ClusterDate> kvpDate in cluster.dateClusters)
		{
			if (kvpDate.Value.stack.GetNumActiveImages() > 0) {
				numActiveClusters++;
			}
		}

		// Only show clusters that have active images...
		imageRows = new ImageWallRow[numActiveClusters];

		int rowIndex = 0;

		// TODO: Sort based on kvpDate.Key?
		foreach (KeyValuePair<string, ClusterDate> kvpDate in cluster.dateClusters)
		{
			// Skip clusters that have no active images!
			if (kvpDate.Value.stack.GetNumActiveImages() > 0)
			{
				imageRows[rowIndex] = new ImageWallRow(imagePrefab, textPrefab,
					kvpDate.Value, useCurvedImageWall);
				++rowIndex;
			}
		}

		state = ImageWallState.Loading;
	}

	public IEnumerator FinalizeSetup(GameObject imagePrefab, GameObject textPrefab,
									Transform viveTransform)
	{
		// Load arrow textures
		// Setup arrows and text
		topArrow = MonoBehaviour.Instantiate(imagePrefab);
		bottomArrow = MonoBehaviour.Instantiate(imagePrefab);
		textureTopArrow = new Texture2D(2, 2, TextureFormat.BC7, false);
		textureBottomArrow = new Texture2D(2, 2, TextureFormat.BC7, false);

		// Load arrow textures...
		string arrowPath = Application.dataPath + "/";
		bool success = Util.LoadPNGWithExistingTexture2D(textureTopArrow,
			arrowPath + "TopArrow.png", false);

		if (!success)
		{
			Debug.Log("Failed to load top arrow image via Util!");
		}
		else
		{
			Renderer renderer = topArrow.GetComponent<Renderer>();

			if (renderer != null)
			{
				renderer.material.mainTexture = textureTopArrow;
				// Change shader into transparent one:
				// Legacy Shaders/Transparent/Cutout/Diffuse
				renderer.material.shader = transparentShader;
			}
		}

		success = Util.LoadPNGWithExistingTexture2D(textureBottomArrow,
			arrowPath + "BottomArrow.png", false);

		if (!success)
		{
			Debug.Log("Failed to load bottom arrow image via Util!");
		}
		else
		{
			Renderer renderer = bottomArrow.GetComponent<Renderer>();

			if (renderer != null)
			{
				renderer.material.mainTexture = textureBottomArrow;
				// Change shader into transparent one:
				// Legacy Shaders/Transparent/Cutout/Diffuse
				renderer.material.shader = transparentShader;
			}
		}

		topArrow.layer = Map.IMAGE_BILLBOARD_LAYER;
		bottomArrow.layer = Map.IMAGE_BILLBOARD_LAYER;

		topArrow.transform.SetParent(viveTransform, true);
		bottomArrow.transform.SetParent(viveTransform, true);

		topArrow.transform.position = viveTransform.position;
		bottomArrow.transform.position = viveTransform.position;

		topArrow.transform.localScale = arrowScale;
		bottomArrow.transform.localScale = arrowScale;

		// Old crap: ImageWallRow.IMAGE_SCALE * (NUM_ROWS_VISIBLE_PER_SIDE + 0.5f), 0.2f)
		topArrow.transform.localPosition = new Vector3(
			ImageWallRow.GetImageXPos(ImageWallRow.CENTER_INDEX, useCurvedImageWall),
			ImageWallRow.GetImageYPos(NUM_ROWS_VISIBLE_PER_SIDE + 1) - 0.2f,
			ImageWallRow.GetImageZPos(ImageWallRow.CENTER_INDEX, useCurvedImageWall) - 0.2f);
		bottomArrow.transform.localPosition = new Vector3(
			ImageWallRow.GetImageXPos(ImageWallRow.CENTER_INDEX, useCurvedImageWall),
			ImageWallRow.GetImageYPos(-NUM_ROWS_VISIBLE_PER_SIDE - 1) + 0.2f,
			ImageWallRow.GetImageZPos(ImageWallRow.CENTER_INDEX, useCurvedImageWall) - 0.2f);

		// Old crap: 0.0f, -ImageWallRow.IMAGE_SCALE * (NUM_ROWS_VISIBLE_PER_SIDE + 0.5f), 0.2f));

		// We set the arrows to inactive because they will be corrected later in EnsureValidRows()
		topArrow.SetActive(false);
		bottomArrow.SetActive(false);

		// Finish creating row objects
		for (int rowIndex = imageRows.Length - 1; rowIndex >= 0; --rowIndex)
		{
			// Create the row's game objects and set their base position
			yield return null;
			ImageWallRow row = imageRows[rowIndex];
			int numActive = row.dateCluster.stack.GetNumActiveImages();

			row.CreateObjects(imagePrefab, textPrefab);
			row.SetActiveRow(false); // For now
			yield return null;

			row.SetBaseRowPositionAndScale(viveTransform);
			yield return null;

			// Load first few images into the row... Normal version
			for (int arrayIndex = ImageWallRow.CENTER_INDEX; arrayIndex < ImageWallRow.NUM_IMAGES_VISIBLE; ++arrayIndex)
			{
				// Load image into the row but don't show it yet...
				int stackIndex = 0 - ImageWallRow.CENTER_INDEX + arrayIndex;
				row.LoadNewImage(numActive, stackIndex, arrayIndex);
				row.ReassignTextures();
				yield return null;
			}

			// Update the entire row by setting the text correctly etc...
			row.UpdateImageRow();
		}

		// Ensures valid rows visible and scaled etc, and also
		// handles top and bottom arrows properly
		EnsureValidRows();
		yield return null;

		imageRows[0].UpdateImageRow();
		yield return null;

		state = ImageWallState.Done;
	}

	private void EnsureValidRows()
	{
		for (int rowIndex = 0; rowIndex < imageRows.Length; ++rowIndex)
		{
			bool active = Math.Abs(rowIndex - currentRowIndex) <= NUM_ROWS_VISIBLE_PER_SIDE;
			imageRows[rowIndex].SetActiveRow(active);
			imageRows[rowIndex].UpdateRowPositionAndScale(rowIndex - currentRowIndex);
		}

		// Check if there are more rows below us
		if (currentRowIndex - NUM_ROWS_VISIBLE_PER_SIDE > 0)
		{
			bottomArrow.SetActive(true);
		}
		else
		{
			// There are not, disable the bottom arrow
			bottomArrow.SetActive(false);
		}

		// Check if there are more rows above us
		if (imageRows.Length - 1 - currentRowIndex > NUM_ROWS_VISIBLE_PER_SIDE)
		{
			topArrow.SetActive(true);
		}
		else
		{
			topArrow.SetActive(false);
		}
	}

	public void MoveLeft()
	{
		imageRows[currentRowIndex].MoveLeft();
	}

	public void MoveRight()
	{
		imageRows[currentRowIndex].MoveRight();
	}

	public void MoveUp()
	{
		// Handle overflow
		if (currentRowIndex >= imageRows.Length - 1)
		{
			currentRowIndex = 0;
		}
		else
		{
			++currentRowIndex;
		}

		EnsureValidRows();
	}

	public void MoveDown()
	{
		// Handle underflow
		if (currentRowIndex <= 0)
		{
			currentRowIndex = imageRows.Length - 1;
		}
		else
		{
			--currentRowIndex;
		}

		EnsureValidRows();
	}

	public bool AtStartOrEndOfCurrentImageRow()
	{
		return imageRows[currentRowIndex].AtStartOrEndOfRow();
	}

	public Image GetCurrentImage()
	{
		return imageRows[currentRowIndex].GetCurrentImage();
	}

	public IEnumerator UpdateImageWall()
	{
		for (int i = 0; i < imageRows.Length; ++i)
		{
			imageRows[i].UpdateImageRow();
			yield return null;
		}
	}

	public void Destroy()
	{
		foreach (ImageWallRow row in imageRows)
		{
			row.Destroy();
		}

		MonoBehaviour.Destroy(topArrow);
		MonoBehaviour.Destroy(bottomArrow);
		MonoBehaviour.Destroy(textureTopArrow);
		MonoBehaviour.Destroy(textureBottomArrow);
}
}
