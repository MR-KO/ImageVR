﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CI.TaskParallel;

public class RevealSphere : MonoBehaviour
{
    public float revealRadius = 5f;
    public float revealSize;

    [Tooltip("Images always facing the camera")]
    public bool billboard = true;

	private Collider[] collisions = null;

	// To load pin images asynchronously
	private JobQueue<PinAsyncLoader> pinQueue;
	private static int MAX_QUEUED_JOBS = 25; // Max number of active jobs to keep pinQueue short...
	private int numPinThreads = 2; // TODO: Should be something like num cores - 1
								// Test results:
								// 1, 2 - Perfect, no lag
								// 3 - Acceptable
								// 4 to 8 - Laggy 100% CPU utilization, don't do this...

	void OnEnable()
	{
		// create new queues with # threads
		pinQueue = new JobQueue<PinAsyncLoader>(numPinThreads);
	}

	// Use this for initialization
	void Start()
	{
		UnityTask.InitialiseDispatcher();
	}

	void FillPinLoadingQueue(List<Pin> pins)
	{
		for (int i = 0; i < pins.Count; ++i)
		{
			if (pins[i].state == PinState.Not_Loaded)
			{
				// Queue at most a few images per frame update, to keep jobqueue short
				// and pin loading "responsive" to location changes
				int queuedJobs = pinQueue.CountQueuedJobs();

				if (queuedJobs >= RevealSphere.MAX_QUEUED_JOBS)
				{
					return;
				}

				QueuePinLoading(pins[i], true);
			}
		}
	}

	public void QueuePinLoading(Pin pin, bool createTexture = false)
	{
		pin.StartLoadingState();

		if (createTexture)
		{
			pin.imageTexture = new Texture2D(Util.resizeWidth,
					Util.resizeHeight, TextureFormat.RGBA32, false);
		}

		pinQueue.AddJob(new PinAsyncLoader { pin = pin, resize = true });
	}

	// Update is called once per frame
	void Update()
	{
		// TODO: Prevent GC and shit and use Physics.OverlapSphereNonAlloc ?
		collisions = Physics.OverlapSphere(transform.position,
			   revealRadius);

		// Get all pin (game)objects from the main thread
		List<Pin> pins = new List<Pin>(collisions.Length);

		for (int i = 0; i < collisions.Length; ++i)
		{
			Pin p = collisions[i].gameObject.GetComponent<Pin>();

			if (p != null)
			{
				pins.Add(p);
			}
		}

		// Only load those pins who should be visible and aren't already loading/loaded...
		int length = pins.Count;
		pins = pins.FindAll(x => x.isVisible() == true);
		FillPinLoadingQueue(pins);

		// Reveal all loaded pins
		for (int i = 0; i < pins.Count; ++i)
		{
			pins[i].Reveal();
		}

		pinQueue.Update();
		RotatePins(collisions);
	}

	void RotatePins(Collider[] collisions)
	{
		if (billboard)
		{
			//Debug.Log("Billboard!");
			Vector3 cameraPos = Camera.main.transform.position;

			for (int i = 0; i < collisions.Length; ++i)
			{
				if (collisions[i] != null && collisions[i].gameObject != null)
				{
					ImageScript img = collisions[i].gameObject.GetComponent<ImageScript>();

					if (img != null && !img.inController)
					{
						img.transform.LookAt(cameraPos);
						// Old version:
						//img.transform.rotation = Quaternion.Euler(
						//	0 /*img.transform.rotation.eulerAngles.x*/,
						//	img.transform.rotation.eulerAngles.y + 180,
						//	img.transform.rotation.eulerAngles.z);

						// New version:
						img.transform.rotation = Quaternion.Euler(
							img.transform.rotation.eulerAngles.x + 180,
							img.transform.rotation.eulerAngles.y,
							img.transform.rotation.eulerAngles.z + 180);
					}
				}
			}
		}
	}

	void OnDisable()
	{
		// important to terminate the threads inside the queue.
		pinQueue.ShutdownQueue();
	}
}


