﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Endgame;

public class FilterInterface : MonoBehaviour {

	public enum ActiveMenu { AToZMenu, TagListMenu, ExtraOptionsMenu };

	public ActiveMenu currentActiveMenu = ActiveMenu.TagListMenu;

	[Header("Listviews for every menu")]
	public ListView listViewAToZ;
	public ListView listViewTagList;
	public ListView listViewExtraOptions;

	public AToZListHandler aToZHandler;
	public TagListFilterHandler tagListHandler;
	public ExtraOptionsHandler extraHandler;

	public ListViewHandler activeHandler;

	public void Initialize(List<Tag> tagListAlphabeticallySorted, List<Tag> tagListSortedByOccurrences)
	{
		aToZHandler = new AToZListHandler();
		aToZHandler.listView = listViewAToZ;

		tagListHandler = new TagListFilterHandler();
		tagListHandler.listView = listViewTagList;

		extraHandler = new ExtraOptionsHandler();
		extraHandler.listView = listViewExtraOptions;

		tagListHandler.Initialize(tagListAlphabeticallySorted, tagListSortedByOccurrences);
		aToZHandler.Initialize();
		extraHandler.Initialize();

		SwitchToMenu(ActiveMenu.TagListMenu);
	}

	public void SwitchToMenu(ActiveMenu newMenu)
	{
		currentActiveMenu = newMenu;

		// Clear other selections in the menus and select only in the active one
		switch (currentActiveMenu)
		{
			case ActiveMenu.AToZMenu:
				tagListHandler.ClearSelections();
				extraHandler.ClearSelections();
				aToZHandler.SelectItem();
				activeHandler = aToZHandler;
				break;

			case ActiveMenu.ExtraOptionsMenu:
				aToZHandler.ClearSelections();
				tagListHandler.ClearSelections();
				extraHandler.SelectItem();
				activeHandler = extraHandler;
				break;

			case ActiveMenu.TagListMenu:
			default:
				aToZHandler.ClearSelections();
				extraHandler.ClearSelections();
				tagListHandler.SelectItem();
				activeHandler = tagListHandler;
				break;
		}

		activeHandler.SelectItem();
	}

	public void GoToLeftMenu()
	{
		switch (currentActiveMenu)
		{
			case ActiveMenu.TagListMenu:
				SwitchToMenu(ActiveMenu.AToZMenu);
				break;

			case ActiveMenu.ExtraOptionsMenu:
				SwitchToMenu(ActiveMenu.TagListMenu);
				break;

			case ActiveMenu.AToZMenu:
			default:
				break;
		}
	}

	public void GoToRightMenu()
	{
		switch (currentActiveMenu)
		{
			case ActiveMenu.AToZMenu:
				SwitchToMenu(ActiveMenu.TagListMenu);
				break;

			case ActiveMenu.TagListMenu:
				SwitchToMenu(ActiveMenu.ExtraOptionsMenu);
				break;

			case ActiveMenu.ExtraOptionsMenu:
			default:
				break;
		}
	}

	public int GetCurrentIndexForActiveMenu()
	{
		return activeHandler.selectedIndex;
	}

	public void SetNewIndexForActiveMenu(int newIndex)
	{
		activeHandler.selectedIndex = newIndex;
	}

	public int GetMaxIndexForActiveMenu()
	{
		return activeHandler.listView.Items.Count - 1;
	}

	public void SelectItem()
	{
		System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
		activeHandler.SelectItem();
		stopwatch.Stop();
		//Debug.Log("Selecting item took " + stopwatch.ElapsedMilliseconds + " ms!");
	}

	public object GetSelectedItem()
	{
		System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
		var temp = activeHandler.GetSelectedItem();
		stopwatch.Stop();
		//Debug.Log("Getting selected item took " + stopwatch.ElapsedMilliseconds + " ms!");
		return temp;
	}

	public void UpdateSelectedTagStatus()
	{
		if (currentActiveMenu == ActiveMenu.TagListMenu)
		{
			System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
			tagListHandler.UpdateSelectedItem();
			stopwatch.Stop();
			//Debug.Log("Updating selected item took " + stopwatch.ElapsedMilliseconds + " ms!");
		}
	}

	// Handles special actions, assumes some item in some menu is already selected...
	//		- Enable/disable all tags
	// 		- Sort tag list alphabetically or by occurrences
	//		- Going to A-Z in left menu should also update tag list (and vica-versa...)
	public void HandleSpecialActionsForSelectItem()
	{
		// Check if we have the extra options menu active...
		if (currentActiveMenu == ActiveMenu.ExtraOptionsMenu)
		{
			ExtraOptionsHandler.Option selectedOption = extraHandler.GetSelectedItem();

			switch (selectedOption)
			{
				case ExtraOptionsHandler.Option.EnableDisableAllTags:
					extraHandler.SetAllTagsText();
					tagListHandler.UpdateAllTags();
					break;

				case ExtraOptionsHandler.Option.EnableDisableNoTags:
					extraHandler.SetNoTagsText();
					break;

				case ExtraOptionsHandler.Option.AlphabeticalSort:
					tagListHandler.SetListViewAlphabetically();
					break;

				case ExtraOptionsHandler.Option.OccurrenceSort:
					tagListHandler.SetListViewOccurrences();
					break;

				case ExtraOptionsHandler.Option.None:
				default:
					Debug.Log("None option!");
					break;
			}
		}

		// Check if we have the A to Z menu active, so we can synchronize the tag list
		if (currentActiveMenu == ActiveMenu.AToZMenu)
		{
			string selectedLetter = aToZHandler.GetSelectedItem().ToLower();

			// Find the first tag in the list that starts with this letter
			int index = tagListHandler.FindTagThatStartsWith(selectedLetter);

			if (index >= 0 && index < tagListHandler.listView.Items.Count)
			{
				// Clear the selection on the A-Z menu
				int aToZIndex = aToZHandler.selectedIndex;
				aToZHandler.ClearSelections();
				aToZHandler.selectedIndex = aToZIndex;

				// And switch to the tag list menu
				tagListHandler.selectedIndex = index;
				tagListHandler.SelectItem();
				SwitchToMenu(ActiveMenu.TagListMenu);
			}
			else
			{
				Debug.Log("Invalid tag index: " + index);
			}
		}

		// Check if we have the filtering menu active...
		if (currentActiveMenu == ActiveMenu.TagListMenu)
		{
			Tag tag = tagListHandler.GetSelectedItem();

			if (tag != null)
			{
				// Get the first letter of the tag and find its index
				string letter = tag.name.ToUpper()[0] + "";
				int index = -1;

				for (int i = 0; i < AToZListHandler.letters.Length; i++)
				{
					if (AToZListHandler.letters[i] == letter)
					{
						index = i;
						break;
					}
				}

				if (index >= 0)
				{
					aToZHandler.selectedIndex = index;
					aToZHandler.listView.ScrollToEnsureItemVisible(index);
				}
				else
				{
					Debug.Log("Invalid a-z index: " + index + " for letter: "
						+ letter + " of tag: " + tag);
				}
			}
			else
			{
				Debug.Log("Tag was null!");
			}
		}
	}
}


