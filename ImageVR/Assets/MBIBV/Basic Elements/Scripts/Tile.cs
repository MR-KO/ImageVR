﻿using UnityEngine;

public enum TileState { Not_Loaded, Loading_New_Zoom, Loaded };

public class Tile : MonoBehaviour
{
    public int index; // tiles from center tile
    public int x;
    public int y;
	public int currentZoom;
	public TileState state;
	
    public Material empty;
	public Texture2D tex = null;
	private Renderer quadRenderer = null;

	public void Setup(int x, int y, int zoom, int index)
	{
		this.x = x;
		this.y = y;
		currentZoom = zoom;
		this.index = index;
		state = TileState.Not_Loaded;
		
		//tex = new Texture2D(TileMap.mapwidth, TileMap.mapheight, TextureFormat.RGBA32, true);
	}

	public string GetTilePath(int zoom, bool getResourcesPath=false)
	{
		string file = string.Format("{0}/{1}-{2}", zoom, x, y);
		return getResourcesPath ? file : Application.dataPath + "/Resources/" + file + ".png";
	}

	public void UnloadTexture()
	{
		if (tex != null)
		{
			Resources.UnloadAsset(tex);
			tex = null;
		}
	}
	
	public void UpdateTexture()
	{
		if (quadRenderer == null)
		{
			quadRenderer = GetComponent<Renderer>();
		}

		if (tex == null)
		{
			quadRenderer.material = empty;
		}
		else
		{
			quadRenderer.material.mainTexture = tex;
		}

		state = TileState.Loaded;
	}

	public bool LoadTile(int zoom)
	{
		state = TileState.Loading_New_Zoom;
		currentZoom = zoom;
		string path = GetTilePath(zoom, true);
		tex = Resources.Load<Texture2D>(path);

		if (tex == null)
		{
			Debug.Log("tile not found " + path);
			return false;
		}

		UpdateTexture();
		return true;
	}

	public ResourceRequest StartLoadTileAsync(int zoom)
	{
		state = TileState.Loading_New_Zoom;
		currentZoom = zoom;
		string path = GetTilePath(zoom, true);
		return Resources.LoadAsync<Texture2D>(path);
	}

	public void FinishLoadTileAsync(ResourceRequest request)
	{
		UnloadTexture();
		tex = (Texture2D)request.asset;

		if (tex == null)
		{
			Debug.Log("Failed to load tex!");
		}

		UpdateTexture();
	}

	public void OnDisable()
	{
		UnloadTexture();
	}
}
