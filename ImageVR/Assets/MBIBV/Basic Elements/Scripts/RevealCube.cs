﻿using UnityEngine;
using System.Collections;

public class RevealCube : MonoBehaviour
{
    public float revealRadius = 10f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Collider[] collisions = Physics.OverlapBox(transform.position, new Vector3(0.5f, 0.5f, revealRadius), transform.rotation);

        int i = 0;
        while (i < collisions.Length)
        {
            Pin x = collisions[i].gameObject.GetComponent<Pin>();
            if (x != null)
            {
				x.Reveal();
            }
            //ImageScript img = collisions[i].gameObject.GetComponent<ImageScript>();
            //if (img != null)
            //{
            //    img.transform.LookAt(Camera.main.transform.position);
            //    img.transform.rotation = Quaternion.Euler(0/*img.transform.rotation.eulerAngles.x*/, img.transform.rotation.eulerAngles.y + 180, img.transform.rotation.eulerAngles.z);
            //}

            i++;
        }



    }
}
