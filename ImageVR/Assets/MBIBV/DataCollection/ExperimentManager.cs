using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System;

public enum ExperimentState { EnteringTestSubject, EnteringTaskNr, Ready, Testing, Paused };

public class ExperimentManager
{
	public Experiment current;
	public ExperimentState state;

	private int testSubjectNr;
	private int taskNr;
	private List<Tag> tagList;
	public bool outputResults = false;

	// Helper for GetInputNumber()
	private int inputScale;
	private int inputNr;
	private bool inputDone;

	// Used to skip the 10 ifs, and instead use Array.Find
	static private KeyCode[] numberCodes = { KeyCode.Alpha0, KeyCode.Alpha1,
		KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5,
		KeyCode.Alpha6, KeyCode.Alpha7, KeyCode.Alpha8, KeyCode.Alpha9};


	public ExperimentManager(List<Tag> tags)
	{
		tagList = tags;
		ResetToIdle();
	}

	public bool IsTesting()
	{
		return state == ExperimentState.Testing;
	}

	public bool IsPaused()
	{
		return state == ExperimentState.Paused;
	}

	private void ResetToIdle()
	{
		state = ExperimentState.EnteringTestSubject;
		testSubjectNr = 0;
		taskNr = -1;
		current = null;

		ResetGetInputNumber();
		Debug.Log("Enter test subject number, and then hit enter");
	}

	private void ResetGetInputNumber()
	{
		inputScale = 1;
		inputNr = 0;
		inputDone = false;
	}

	private void GetInputNumber()
	{
		int selectedNr = Array.FindIndex(ExperimentManager.numberCodes, x => Input.GetKeyDown(x));
		inputDone = Input.GetKeyDown(KeyCode.Return);

		if (!inputDone && selectedNr != -1)
		{
			inputNr += selectedNr * inputScale;
			inputScale *= 10;
			//Debug.Log("Entered number so far: " + ReverseInt(inputNr));
		}
	}

	private static int ReverseInt(int num)
	{
		// Shameless copy/paste from StackOverflow, because I'm currently
		// too lazy and tired to write this myself
		int result = 0;

		while (num > 0)
		{
			result = result * 10 + num % 10;
			num /= 10;
		}

		return result;
	}

	private void HandleSelectingTestSubject()
	{
		if (state == ExperimentState.EnteringTestSubject)
		{
			GetInputNumber();

			if (inputDone)
			{
				state = ExperimentState.EnteringTaskNr;
				testSubjectNr = ReverseInt(inputNr);
				ResetGetInputNumber();
				Debug.Log("Selected test subject " + testSubjectNr + "! Enter task number, and then hit enter");
			}
		}
	}

	private void SetupExperiment()
	{
		Debug.Log("Ready to start experiment " + taskNr + "! Press S to start it!");
		current = new Experiment(tagList, testSubjectNr, taskNr);
	}

	private void HandleSelectingExperiment()
	{
		if (state == ExperimentState.EnteringTaskNr)
		{
			GetInputNumber();

			if (inputDone)
			{
				state = ExperimentState.Ready;
				taskNr = ReverseInt(inputNr);
				ResetGetInputNumber();
				SetupExperiment();
			}
		}
	}

	private void HandleReadyExperiment()
	{
		if (state == ExperimentState.Ready && Input.GetKeyDown(KeyCode.S))
		{
			state = ExperimentState.Testing;
			Debug.Log("Started experiment! Press P to pause, R to reset or S to stop!");
			current.Start();
		}
	}

	private void HandleResettingExperiment()
	{
		if (state == ExperimentState.Testing || state == ExperimentState.Paused)
		{
			if (Input.GetKeyDown(KeyCode.R))
			{
				state = ExperimentState.Ready;
				Debug.Log("Resetting experiment to intial state, so you can try again...");
				current.Stop();
				SetupExperiment();
			}
		}
	}

	private void HandleStoppingExperiment()
	{
		if (state == ExperimentState.Testing || state == ExperimentState.Paused)
		{
			if (Input.GetKeyDown(KeyCode.S))
			{
				Debug.Log("Stopping experiment prematurely, nothing is saved!");
				current.Stop();
				ResetToIdle();
			}
		}
	}

	private void HandlePauseAndResume()
	{
		if (Input.GetKeyDown(KeyCode.P))
		{
			switch (state)
			{
				case ExperimentState.Testing:
					Debug.Log("Pausing experiment!");
					state = ExperimentState.Paused;
					current.Pause();
					break;

				case ExperimentState.Paused:
					Debug.Log("Resuming experiment!");
					state = ExperimentState.Testing;
					current.Resume();
					break;

				default:
					break;
			}
		}
	}

	public void UpdateExperimentStatus()
	{
		switch (state)
		{
			case ExperimentState.EnteringTestSubject:
				HandleSelectingTestSubject();
				break;

			case ExperimentState.EnteringTaskNr:
				HandleSelectingExperiment();
				break;

			case ExperimentState.Ready:
				HandleReadyExperiment();
				break;

			case ExperimentState.Testing:
			case ExperimentState.Paused:
				// Check if the experiment time is up
				if (current.ShouldStop())
				{
					Debug.Log("Experiment time is up, stopping experiment and saving results!");
					current.Stop();

					if (outputResults)
					{
						current.PrintResults();
					}

					current.Save();
					ResetToIdle();
				}
				else
				{
					// Check for pause, reset, stop etc
					HandlePauseAndResume();
					HandleResettingExperiment();
					HandleStoppingExperiment();
				}

				break;

			default:
				break;
		}
	}
}
