using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Diagnostics;
using System.Data;

public class Experiment
{
	public static readonly long MAX_EXPERIMENT_DURATION = 60 * 1000; // 60 seconds

	// TODO: Get these and note them!
	public static readonly long[] CORRECT_IMAGE_IDS_TASK_1 = { -1 };
	public static readonly long[] CORRECT_IMAGE_IDS_TASK_2 = { -1 };
	public static readonly long[] CORRECT_IMAGE_IDS_TASK_3 = { -1 };
	public static readonly long[] CORRECT_IMAGE_IDS_TASK_4 = { -1 };

	// Experiment specific configuration
	public int testSubjectNr;
	public int taskNr;
	public Stopwatch experimentStopwatch;

	// Countable things to keep track of
	public int nrOfStacksClicked;
	public int nrOfFilteringOperations;
	public int nrOfTeleportations;
	public double totalTeleportDistance;
	public int nrOfZoomOuts;
	public double zoomOutDistance;

	public int nrOfImageRowSwitchesUpPresses;
	public int nrOfImageRowSwitchesDownPresses;
	public int nrOfImageRowNextPresses;
	public int nrOfImageRowPreviousPresses;

	public int nrOfCorrectImagesFound;
	public int nrOfUniqueImagesVisited;
	public int nrOfTotalImagesVisited;

	// Special things to keep track of
	public List<Tuple<long, Tag>>  changedTags;
	public List<Tuple<long, Image>> uniqueImagesVisited;
	public List<Tuple<long, Image>> imagesFound;

	// Timeable things to keep track of, in ms
	public Stopwatch idlingStopwatch;
	public Stopwatch teleportingStopwatch;
	public Stopwatch navigatingStopwatch;
	public Stopwatch zoomingStopwatch;
	public Stopwatch filteringStopwatch;

	// For handling pausing correctly...
	private bool resumeIdling;
	private bool resumeTeleporting;
	private bool resumeZooming;
	private bool resumeNavigating;
	private bool resumeFiltering;

	// To get Tag objects back
	private List<Tag> tagList;

	public Experiment(List<Tag> tags, int testSubject, int task)
	{
		// General configuration
		tagList = tags;

		// Configuration for this experiment
		testSubjectNr = testSubject;
		taskNr = task;

		// Setup/initialize shit...
		nrOfStacksClicked = 0;
		nrOfFilteringOperations = 0;
		nrOfTeleportations = 0;
		totalTeleportDistance = 0.0;
		nrOfZoomOuts = 0;
		zoomOutDistance = 0.0;
		nrOfImageRowSwitchesUpPresses = 0;
		nrOfImageRowSwitchesDownPresses = 0;
		nrOfImageRowNextPresses = 0;
		nrOfImageRowPreviousPresses = 0;
		nrOfCorrectImagesFound = 0;
		nrOfUniqueImagesVisited = 0;
		nrOfTotalImagesVisited = 0;

		changedTags = new List<Tuple<long, Tag>>();
		uniqueImagesVisited = new List<Tuple<long, Image>>();
		imagesFound = new List<Tuple<long, Image>>();

		experimentStopwatch = null;

		idlingStopwatch = null;
		teleportingStopwatch = null;
		zoomingStopwatch = null;
		navigatingStopwatch = null;
		filteringStopwatch = null;

		// For correctly pausing/resuming special stopwatches
		resumeIdling = false;
		resumeTeleporting = false;
		resumeZooming = false;
		resumeNavigating = false;
		resumeFiltering = false;
	}

	private bool CorrectImageFound(Image image)
	{
		// Based on experiment task!
		long[] arrayToSearch = null;

		switch (taskNr)
		{
			case 1:
				arrayToSearch = CORRECT_IMAGE_IDS_TASK_1;
				break;
			case 2:
				arrayToSearch = CORRECT_IMAGE_IDS_TASK_2;
				break;
			case 3:
				arrayToSearch = CORRECT_IMAGE_IDS_TASK_3;
				break;
			case 4:
				arrayToSearch = CORRECT_IMAGE_IDS_TASK_4;
				break;
			default:
				break;
		}

		if (arrayToSearch != null)
		{
			// Maybe correct image found!
			return Array.Exists<long>(arrayToSearch, x => x == image.id);
		}

		return false;
	}

	public void AddImage(Image newImage)
	{
		if (newImage != null)
		{
			nrOfTotalImagesVisited++;

			// Look for it in the uniqueImagesVisited list...
			Tuple<long, Image> found = uniqueImagesVisited.Find(x => x.Second.id == newImage.id);

			if (found == null)
			{
				// Add it!
				uniqueImagesVisited.Add(new Tuple<long, Image>(GetElapsedTimeInMs(), newImage));
				nrOfUniqueImagesVisited++;
			}

			// Look for it in the list of images for the experiment!
			if (CorrectImageFound(newImage))
			{
				// Image is correct, check if we have already found it...
				found = imagesFound.Find(x => x.Second.id == newImage.id);

				if (found == null)
				{
					// Add it!
					imagesFound.Add(new Tuple<long, Image>(GetElapsedTimeInMs(), newImage));
					nrOfCorrectImagesFound++;
				}
			}
		}
	}

	public void AddTag(Tag newTag)
	{
		if (newTag != null)
		{
			changedTags.Add(new Tuple<long, Tag>(GetElapsedTimeInMs(), new Tag(newTag)));
			nrOfFilteringOperations++;
		}
	}

	public void Start()
	{
		experimentStopwatch = Stopwatch.StartNew();
		idlingStopwatch = new Stopwatch();
		teleportingStopwatch = new Stopwatch();
		zoomingStopwatch = new Stopwatch();
		navigatingStopwatch = new Stopwatch();
		filteringStopwatch = new Stopwatch();
	}

	public void Pause()
	{
		experimentStopwatch.Stop();

		resumeIdling = idlingStopwatch.IsRunning;
		idlingStopwatch.Stop();

		resumeTeleporting = teleportingStopwatch.IsRunning;
		teleportingStopwatch.Stop();

		resumeZooming = zoomingStopwatch.IsRunning;
		zoomingStopwatch.Stop();

		resumeNavigating = navigatingStopwatch.IsRunning;
		navigatingStopwatch.Stop();

		resumeFiltering = filteringStopwatch.IsRunning;
		filteringStopwatch.Stop();
	}

	public void Resume()
	{
		experimentStopwatch.Start();

		if (resumeIdling)
		{
			resumeIdling = false;
			idlingStopwatch.Start();
		}

		if (resumeTeleporting)
		{
			resumeTeleporting = false;
			teleportingStopwatch.Start();
		}

		if (resumeZooming)
		{
			resumeZooming = false;
			zoomingStopwatch.Start();
		}

		if (resumeNavigating)
		{
			resumeNavigating = false;
			navigatingStopwatch.Start();
		}

		if (resumeFiltering)
		{
			resumeFiltering = false;
			filteringStopwatch.Start();
		}
	}

	public void Stop()
	{
		Pause();
	}

	public bool ShouldStop()
	{
		return experimentStopwatch.ElapsedMilliseconds >= MAX_EXPERIMENT_DURATION;
	}

	public long GetElapsedTimeInMs()
	{
		return experimentStopwatch.ElapsedMilliseconds;
	}

	public void PrintResults()
	{
		UnityEngine.Debug.Log("Found " + nrOfCorrectImagesFound + " image(s) correctly out of (TODO: Add this!)");

		UnityEngine.Debug.Log("Correct images found: " + imagesFound);

		for (int i = 0; i < imagesFound.Count; ++i)
		{
			UnityEngine.Debug.Log("\t[" + imagesFound[i].First + "]: " + imagesFound[i].Second);
		}

		UnityEngine.Debug.Log("Unique images found: " + uniqueImagesVisited);

		for (int i = 0; i < uniqueImagesVisited.Count; ++i)
		{
			UnityEngine.Debug.Log("\t[" + uniqueImagesVisited[i].First + "]: " + uniqueImagesVisited[i].Second);
		}

		UnityEngine.Debug.Log("Looked at " + nrOfTotalImagesVisited + " images in total, of which "
			+ nrOfUniqueImagesVisited + " are unique");
		UnityEngine.Debug.Log("Number of image wall interactions: " +
			(nrOfImageRowSwitchesUpPresses + nrOfImageRowSwitchesDownPresses +
			nrOfImageRowNextPresses + nrOfImageRowPreviousPresses));

		UnityEngine.Debug.Log("Number of filtering operations: " + nrOfFilteringOperations);
		UnityEngine.Debug.Log("Tags used: " + changedTags);

		for (int i = 0; i < changedTags.Count; ++i)
		{
			UnityEngine.Debug.Log("\t[" + changedTags[i].First + "]: " + changedTags[i].Second);
		}

		UnityEngine.Debug.Log("Teleported " + nrOfTeleportations + " time(s) with total distance: "
			+ totalTeleportDistance + ", average = " + (totalTeleportDistance / nrOfTeleportations));
		UnityEngine.Debug.Log("Zoomed out " + nrOfZoomOuts + " time(s) with total distance: "
			+ zoomOutDistance + ", average = " + (zoomOutDistance / nrOfZoomOuts));

		UnityEngine.Debug.Log("Time spent teleporting: " + teleportingStopwatch.ElapsedMilliseconds + " ms");
		UnityEngine.Debug.Log("Time spent idling: " + idlingStopwatch.ElapsedMilliseconds + " ms");
		UnityEngine.Debug.Log("Time spent looking at images: " + navigatingStopwatch.ElapsedMilliseconds + " ms");
		UnityEngine.Debug.Log("Time spent filtering: " + filteringStopwatch.ElapsedMilliseconds + " ms");
	}

	public void Save()
	{
		UnityEngine.Debug.Log("Saving results to file...");
		Stopwatch savingStopwatch = Stopwatch.StartNew();

		// Write general experiment results and stats for this task and test subject to file
		using (System.IO.StreamWriter file =
			new System.IO.StreamWriter(Application.dataPath +
				"/test_subject_" + testSubjectNr + "_task_" + taskNr + "_stats.csv"))
		{
			file.WriteLine("# Test result (correct images found), Unique images, "
				+ "Total images, Number of pin/billboard clicks, "
				+ "Number of teleportations, Distance teleported, "
				+ "Number of zoomouts, Distance zoomed out, Filter operations, "
				+ "Horizontal image wall navigations left, Horizontal image wall navigations right, "
				+ "Vertical image wall navigations down, Vertical image wall navigations up, "
				+ "Time spent idling (ms), Time spent navigating (ms), "
				+ "Time spent filtering (ms), Time spent teleporting (ms)");
			file.WriteLine(nrOfCorrectImagesFound + ", " + nrOfUniqueImagesVisited
				+ ", " + nrOfTotalImagesVisited + ", " + nrOfStacksClicked + ", " 
				+ nrOfTeleportations + ", " + totalTeleportDistance + ", " 
				+ nrOfZoomOuts + ", " + zoomOutDistance + ", "
				+ nrOfFilteringOperations + ", " 
				+ nrOfImageRowPreviousPresses + ", " + nrOfImageRowNextPresses + ", " 
				+ nrOfImageRowSwitchesDownPresses + ", " + nrOfImageRowSwitchesUpPresses + ", " 
				+ idlingStopwatch.ElapsedMilliseconds + ", " + navigatingStopwatch.ElapsedMilliseconds + ", "
				+ filteringStopwatch.ElapsedMilliseconds + ", " + teleportingStopwatch.ElapsedMilliseconds);
		}

		// Also write what images were visited to file
		using (System.IO.StreamWriter file =
			new System.IO.StreamWriter(Application.dataPath +
				"/test_subject_" + testSubjectNr + "_task_" + taskNr + "_images.csv"))
		{
			file.WriteLine("# Elapsed time (ms), image_id, image_path, image_locName, image_tags");

			foreach (Tuple<long, Image> tuple in uniqueImagesVisited)
			{
				Image image = tuple.Second;
				List<Tag> imageTags = Tag.GetTags(tagList, image.tags);
				file.WriteLine(tuple.First + ", " + image.id + ", " + image.path + ", "
					+ image.locName + ", \"" + Tag.ListOfTagsToString(imageTags, false) + "\"");
			}
		}

		// Also write what tags were triggered to file
		using (System.IO.StreamWriter file =
			new System.IO.StreamWriter(Application.dataPath +
				"/test_subject_" + testSubjectNr + "_task_" + taskNr + "_tags.csv"))
		{
			file.WriteLine("# Elapsed time (ms), action, tag_id, tag_name, tag_occurrences");

			foreach (Tuple<long, Tag> tuple in changedTags)
			{
				Tag tag = tuple.Second;
				file.WriteLine(tuple.First + ", " + (tag.enabled ? "enabled" : "disabled")
					+ ", " + tag.id + ", " + tag.name + ", " + tag.occurrences);
			}
		}

		savingStopwatch.Stop();
		UnityEngine.Debug.Log("Done! Took " + savingStopwatch.ElapsedMilliseconds + " ms");
	}
}
