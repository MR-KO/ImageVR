﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;

public class DataLogger : MonoBehaviour
{
    public GameObject cambase;
    public GameObject Map;
    public Controller vive1;
    public Controller vive2;

    private StreamWriter sw;

    void Start()
    {
        string filename = System.DateTime.Now.ToString() + "data";

        sw = new StreamWriter(filename);

        sw.WriteLine("{0}:{1}:{2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
        sw.WriteLine(DateTime.Now.ToLongTimeString());
        // Input start time + date, Input Map type
    }

    void Update()
    {
        StringBuilder sb = new StringBuilder(50);
        sb.Append("Headset;pos x:");
        sb.Append(cambase.transform.position.x);
        sb.Append(";pos y:");
        sb.Append(cambase.transform.position.y);
        sb.Append(";pos z:");
        sb.Append(cambase.transform.position.z);

        sb.Append(";rot x:");
        sb.Append(cambase.transform.rotation.eulerAngles.x);
        sb.Append(";rot y:");
        sb.Append(cambase.transform.rotation.eulerAngles.x);
        sb.Append(";rot z:");
        sb.Append(cambase.transform.rotation.eulerAngles.x);

        sb.Append("controller 1 ;pos x:");
        sb.Append(vive1.vive.transform.position.x);
        sb.Append(";pos y:");
        sb.Append(vive1.vive.transform.position.y);
        sb.Append(";pos z:");
        sb.Append(vive1.vive.transform.position.z);

        sb.Append(";rot x:");
        sb.Append(vive1.vive.transform.rotation.eulerAngles.x);
        sb.Append(";rot y:");
        sb.Append(vive1.vive.transform.rotation.eulerAngles.x);
        sb.Append(";rot z:");
        sb.Append(vive1.vive.transform.rotation.eulerAngles.x);

        sb.Append(";buttons:");
        sb.Append(vive1.ToString());

        sb.Append("controller 2 ;pos x:");
        sb.Append(vive2.vive.transform.position.x);
        sb.Append(";pos y:");
        sb.Append(vive2.vive.transform.position.y);
        sb.Append(";pos z:");
        sb.Append(vive2.vive.transform.position.z);

        sb.Append(";rot x:");
        sb.Append(vive2.vive.transform.rotation.eulerAngles.x);
        sb.Append(";rot y:");
        sb.Append(vive2.vive.transform.rotation.eulerAngles.x);
        sb.Append(";rot z:");
        sb.Append(vive2.vive.transform.rotation.eulerAngles.x);

        sb.Append(";buttons:");
        sb.Append(vive2.ToString());
        // Input Controller Buttons


        sw.WriteLine(sb.ToString());

    }

    public void PhotoGrabbed(string url)
    {
        sw.WriteLine(url);
    }

    void OnApplicationExit()
    {
        //Remember to close your StreamWriter!
        sw.Close();
    }
}
