﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;


public class flatMap : TileMap
{
    void Start()
    {
        base.Initialization();

        moveSpeed = 0.1f;
    }


    //protected override IEnumerator ConstructStorage6Lazily()
    //{
    //    Quaternion QRot = Quaternion.Euler(0, 0, 0);

    //    int index = 0;

    //    for (int i = 0; i < mapwidth; i++)
    //    {
    //        for (int j = 0; j < mapheight; j++)
    //        {

    //            string key = i + "," + j;

    //            GameObject tile = Instantiate(tilePrefab);

    //            Tile tile_scr = tile.GetComponent<Tile>();
    //            tile.transform.parent = this.transform;

             
    //            tile.transform.localRotation = QRot;
    //            tile.transform.localPosition = new Vector3(i, -j, 0);
    //            tile.transform.position += tile.transform.forward * circleRadius;


    //            tile_scr.x = i;
    //            tile_scr.y = j;
    //            tile_scr.index = index;
    //            tile_scr.LoadTile(6);


    //            List<ImageStruct> pins = Images[key].ToList();
    //            int pindeleter = -1;
    //            foreach (ImageStruct p in pins)
    //            {
    //                pindeleter++;
    //                if (pindeleter % 20 != 0)
    //                    continue;


    //                GameObject pin = Instantiate(pinPrefab);
    //                pin.transform.parent = tile.transform;

    //                pin.transform.localPosition = new Vector3(RelativePos(p.pos).x - 0.5f, -RelativePos(p.pos).y + 0.5f, -0.02f + Random.Range(0.0001f, 0.0002f));
    //                pin.transform.rotation = tile.transform.rotation;
    //                pin.transform.localScale = new Vector3(0.03f, 0.03f, 0.03f);
    //                Pin pin_scr = pin.GetComponent<Pin>();
    //                pin_scr.map = this;
    //                pin_scr.url = p.url;
    //            }

    //            yield return null;
    //        }

    //        index++;
    //        if (index == 164)
    //            break;

    //    }
    //    doneloading = true;
    //}

    //IEnumerator CreateTile(int i, int j, int index)
    //{
    //    string key = i + "," + j;

    //    //yield return Ninja.JumpToUnity;
    //    GameObject tile = Instantiate(tilePrefab);

    //    Tile tile_scr = tile.GetComponent<Tile>();

    //    //yield return Ninja.JumpBack;
    //    tile.transform.parent = this.transform;
    //    //tile.transform.localPosition = new Vector3(i - 33672, -j + 21640);

    //    tile.transform.localPosition = new Vector3(tilePositions[index].x, -j + 86416, tilePositions[index].y);

    //    tile.transform.localRotation = /*Quaternion.Euler(0,tileRotations[0][index],0);*/ QuaterinionRotation(tileRotations[index]);
    //    tile_scr.x = i;
    //    tile_scr.y = j;
    //    tile_scr.index = index;
    //    tile_scr.LoadTile(18);


    //    List<ImageStruct> pins = Images[key].ToList();

    //    foreach (ImageStruct p in pins)
    //    {
    //        GameObject pin = Instantiate(pinPrefab);
    //        pin.transform.parent = tile.transform;
    //        pin.transform.localPosition = new Vector3(RelativePos(p.pos).x - 0.5f, RelativePos(p.pos).y - 0.5f, -1f);
    //        pin.transform.rotation = tile.transform.rotation;
    //        Pin pin_scr = pin.GetComponent<Pin>();
    //        pin_scr.map = this;
    //        pin_scr.url = p.url;
    //    }
    //    yield return null;
    //}

    //private void Update()
    //{
    //    foreach (GameObject g in ViveControllers)
    //    {
    //        SteamVR_TrackedController svrtc = g.GetComponent<SteamVR_TrackedController>();
    //        if (svrtc.padPressed)
    //        {
    //            SteamVR_Controller.Device device = SteamVR_Controller.Input((int)g.GetComponent<SteamVR_TrackedObject>().index);
    //            Vector2 pos = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0);
    //            Transform cambase = mainCamera.transform.parent.transform;
    //            UnityEngine.Debug.Log(pos.y);
    //            if (pos.y > 0.55f)
    //                cambase.position = new Vector3(cambase.position.x, cambase.position.y + 0.1f, cambase.position.z);
    //            if (pos.y < -0.55f)
    //                cambase.position = new Vector3(cambase.position.x, cambase.position.y - 0.1f, cambase.position.z);
    //        }
    //    }
    //}
	

    protected override void DirectionModeZoomOut()
    {
        // Cartesian Horizontal
        if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
        {

            if (controllerLeft.GetPadPosition.y > 0.50f)
            {
                cambase_transform.position += Vector3.up * moveSpeed;
            }
            if (controllerLeft.GetPadPosition.y < -0.50f)
            {
                cambase_transform.position += Vector3.up * -moveSpeed;
            }
            if (controllerLeft.GetPadPosition.x < -0.50f)
            {
                cambase_transform.position += Vector3.right * -moveSpeed;
            }
            if (controllerLeft.GetPadPosition.x > 0.50f)
            {
                cambase_transform.position += Vector3.right * moveSpeed;
            }
        }

        GrabImage(controllerLeft);

        if (controllerRight.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (controllerRight.GetPadPosition.y > 0.50f)
            {
                cambase_transform.position += Vector3.forward * moveSpeed;
                //if (cambase.position.z > 8.5f)
                //    cambase.transform.position = new Vector3(cambase.position.x, cambase.position.y, 8.5f);
            }
            if (controllerRight.GetPadPosition.y < -0.50f)
            {
                cambase_transform.position += Vector3.forward * -moveSpeed;
            }
        }

        GrabImage(controllerRight);
    }

    protected override void FlyModeZoomOut()
    {
        // spherical inside rotation
        if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (controllerLeft.GetPadPosition.y > 0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(mainCamera.transform.forward);
            }
            if (controllerLeft.GetPadPosition.y < -0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(-mainCamera.transform.forward);
            }
            if (controllerLeft.GetPadPosition.x < -0.40f)
            {
                cambase_transform.Rotate(0, 2, 0);
            }
            if (controllerLeft.GetPadPosition.x > 0.40f)
            {
                cambase_transform.Rotate(0, -2, 0);
            }
        }

        GrabImage(controllerLeft);

        if (controllerRight.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (controllerRight.GetPadPosition.y > 0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(mainCamera.transform.forward);
            }
            if (controllerRight.GetPadPosition.y < -0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(-mainCamera.transform.forward);
            }
            if (controllerRight.GetPadPosition.x < -0.40f)
            {
                cambase_transform.Rotate(0, 2, 0);
            }
            if (controllerRight.GetPadPosition.x > 0.40f)
            {
                cambase_transform.Rotate(0, -2, 0);
            }
        }

        GrabImage(controllerRight);
    }



    protected override void TeleportModeZoomOut()
    {
        base.TeleportModeZoomOut();

        if (controllerRight.WhilePressed(Controller.ButtonType.PadPress))
        {
            cambase_transform.position -= Vector3.forward * moveSpeed * Time.deltaTime;
            if (cambase_transform.position.z < -30)
                cambase_transform.position = new Vector3(cambase_transform.position.x, cambase_transform.position.y, -30);
        }

        if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
        {
            cambase_transform.position -= Vector3.forward * moveSpeed * Time.deltaTime;
            if (cambase_transform.position.z < -30)
                cambase_transform.position = new Vector3(cambase_transform.position.x, cambase_transform.position.y, -30);
        }

        
    }

    //private void Old()
    //{ 
    //    Transform cambase = mainCamera.transform.parent.transform;

    //    // Vive1
    //    SteamVR_TrackedController svrtc1 = vive1.GetComponent<SteamVR_TrackedController>();
    //    if (svrtc1.padPressed)
    //    {
    //        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)vive1.GetComponent<SteamVR_TrackedObject>().index);
    //        Vector2 pos = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0);

    //        bool moved = false;

    //        if (pos.y > 0.55)
    //        {
    //            cambase.position += Vector3.up * moveSpeed;
    //            moved = true;
    //        }
    //        if (pos.y < -0.55)
    //        {
    //            cambase.position += Vector3.up * -moveSpeed;
    //            moved = true;
    //        }
    //        if (pos.x < -0.55)
    //        {
    //            cambase.position += Vector3.right * -moveSpeed;
    //            moved = true;
    //        }
    //        if (pos.x > 0.55)
    //        {
    //            cambase.position += Vector3.right * moveSpeed;
    //            moved = true;
    //        }

    //        if (!moved)
    //        {
    //            Transform transform = svrtc1.GetComponent<SteamVR_LaserPointer>().previousContact;
    //            if (transform != null)

    //            {
    //                if (transform.gameObject.tag == "image")
    //                {
    //                    GameObject img = Instantiate(imagePrefab);
    //                    img.GetComponent<Renderer>().material.mainTexture = transform.gameObject.GetComponent<Renderer>().material.mainTexture;
                        
    //                    img.transform.parent = svrtc1.transform;
    //                    img.transform.localScale = Vector3.one * 0.4f;
    //                    img.transform.localPosition = Vector3.zero;
    //                    img.transform.localRotation = Quaternion.Euler(0, 0, 0);


    //                }
    //            }
    //        }
                

    //    }

        

    //    SteamVR_TrackedController svrtc2 = vive2.GetComponent<SteamVR_TrackedController>();
    //    // Vive2
    //    if (svrtc2.padPressed)
    //    {
    //        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)vive2.GetComponent<SteamVR_TrackedObject>().index);
    //        Vector2 pos = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0);

    //        bool moved = false;

    //        if (pos.y > 0.55)
    //        {
    //            cambase.position += Vector3.forward * moveSpeed;
    //            if (cambase.position.z > 8.5f)
    //                cambase.transform.position = new Vector3(cambase.position.x, cambase.position.y, 8.5f);
    //            moved = true;
    //        }
    //        if (pos.y < -0.55)
    //        {
    //            cambase.position += Vector3.forward * -moveSpeed;
    //            moved = true;
    //        }


    //    }


    protected override void CalculateTileRotations()
    {
        QRot = new Quaternion[mapwidth];

        for (int r = 0; r < mapwidth; r++)
        {
            QRot[r] = Quaternion.Euler(0, 0, 0);
        }
    }
}

