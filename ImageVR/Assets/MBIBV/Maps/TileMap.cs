﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;
using UnityEngine;
using CI.TaskParallel;

using m = System.Math;


public class TileMap : Map
{	
	#region TileComputation
	// make computation of POW not necesarry and thus conversion faster
	static double[] powArray = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288 };

	public static int long2tilex(double lon, int z)
	{
		return (int)((lon + 180.0) / 360.0 * powArray[z]); // m.Pow(2.0, z));
	}

	public static float long2tilex_float(double lon, int z)
	{
		return (float)((lon + 180.0) / 360.0 * powArray[z]);// * m.Pow(2.0, z));
	}

	public static int lat2tiley(double lat, int z)
	{
		return (int)((1.0 - m.Log(m.Tan(lat * m.PI / 180.0) + 1.0 / m.Cos(lat * m.PI / 180.0)) / m.PI) / 2.0 * powArray[z]);// m.Pow(2.0, z));
	}

	public static float lat2tiley_float(double lat, int z)
	{
		return (float)((1.0 - m.Log(m.Tan(lat * m.PI / 180.0) + 1.0 / m.Cos(lat * m.PI / 180.0)) / m.PI) / 2.0 * powArray[z]); // m.Pow(2.0, z));
	}

	public static double tilex2long(int x, int z)
	{
		return x / powArray[z] * 360.0 - 180;
	}

	public static double tiley2lat(int y, int z)
	{
		double n = m.PI - 2.0 * m.PI * y / powArray[z]; // m.Pow(2.0, z);
		return 180.0 / m.PI * m.Atan(0.5 * (m.Exp(n) - m.Exp(-n)));
	}

	public static Vector3 HorVec(Vector2 v)
	{
		return new Vector3(v.x, 0, v.y);
	}

	public static Quaternion QuaternionRotation(float rotation)
	{
		return Quaternion.Euler(0, rotation * Mathf.Rad2Deg, 0);
	}

	public static Vector2 RelativePos(Vector2 pos)
	{
		return new Vector2(pos.x - (int)pos.x, pos.y - (int)pos.y);
	}
	#endregion TileComputation

	protected Quaternion[] QRot;

	protected Vector3[,] TPosBottomLeftCorner;
	protected Vector3[,] TPosTopLeftCorner;
	protected Vector3[,] TPosBottomRightCorner;
	protected Vector3[,] TPosTopRightCorner;

	// Map aspects, will be adjusted later on...
	protected int mapwidth = 64;
	protected int mapheight = 64;
	protected float TILE_SIZE = 0.0f;
	protected Vector3 dataCenterPos = new Vector3();
	
	protected int xmin = int.MaxValue;
	protected int xmax = int.MinValue;
	protected int ymin = int.MaxValue;
	protected int ymax = int.MinValue;

	protected static readonly int BASE_ZOOMLEVEL = 6;
	protected static readonly int MIN_ZOOMLEVEL = 3;
	protected static readonly int MAX_ZOOMLEVEL = 9;
	protected static readonly int NUM_ZOOMLEVELS = MAX_ZOOMLEVEL - MIN_ZOOMLEVEL + 1;


	// What zoom levels to use at what horizontal/vertical tile distance...
	// For the MIN_ZOOMLEVEL (index 0), distance must be <= ZOOM_LEVEL_PER_DISTANCE[0]
	protected static readonly float[] ZOOM_LEVEL_PER_VERTICAL_DISTANCE = {
		float.PositiveInfinity, 500, 250, 100, 50, 25, 5.5f };
	protected static readonly float[] ZOOM_LEVEL_PER_HORIZONTAL_DISTANCE = {
		float.PositiveInfinity, 50, 30, 20, 15, 10, 7.5f };

	// For updating the tile zoomes dynamically, every frame...
	private int current_tile_index = -1;
	public static readonly int NUM_ZOOMS_TO_CALCULATE_PER_FRAME = 5;
	public static readonly int NUM_TILES_TO_UPDATE_PER_FRAME = 5;

	// How much more to load of the map outside of the relevant tiles...
	[Header("Tilemap options")]
	[Tooltip("Cut off unused parts of the map?")]
	public float BASE_TILE_SIZE = 5.0f;
	public bool CUT_OFF_MAP = true;
	public float extraXPercentage = 0.2f;
	public float extraYPercentage = 0.2f;

	// A list of all Tile objects for this map, for easy access
	protected List<Tile> mapTiles;


	protected void Start()
	{
		mapTiles = new List<Tile>(mapwidth * mapheight);
		Initialization();
	}

	protected override void Initialization()
	{
		System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
		
		base.Initialization();

		circleRadius = 10.15f;

		// Determine bounding box, size, and position of the map...
		SetTileSize();
		SetMapBoundingBox();
		CalculateTilePositions();
		CalculateTileRotations();

		// And finally create the map...
		ConstructStorage();
		stopwatch.Stop();

		Debug.Log("Entire initialization took " + stopwatch.ElapsedMilliseconds + " ms!");
		StartCoroutine(UpdateFilteredImages());

		// Teleport the player to the center of the data, but slightly 
		// "below" so we can look at it...
		SetCenterPositionOfData();
		SetPlayerPos(dataCenterPos + new Vector3(0, 20, TILE_SIZE * -5));
	}


	protected override void CreateImageLookup()
	{
		List<KeyValuePair<string, ImageStruct>> items = new List<KeyValuePair<string, ImageStruct>>(clusterManager.locationClusters.Count);

		foreach (KeyValuePair<string, ClusterLocation> kvp in clusterManager.locationClusters)
		{
			// Get location of the cluster as representing
			float x = long2tilex_float(kvp.Value.locLong, 6);
			float y = lat2tiley_float(kvp.Value.locLat, 6);

			Vector2 pos = new Vector2(x, y);

			int tilex = (int)x;
			int tiley = (int)y;
			string intpos = tilex + "," + tiley;

			// Get first image of the location cluster as representing...
			ImageStruct img = new ImageStruct(pos, kvp.Value);
			items.Add(new KeyValuePair<string, ImageStruct>(intpos, img));
		}

		imageStructs = items.ToLookup(kvp => kvp.Key, kvp => kvp.Value);
	}

	protected virtual GameObject CreateTile(Quaternion rotation, Vector3 topLeftPosition, 
		int index, int x, int y, int zoomLevel)
	{
		GameObject tile = Instantiate(tilePrefab);
		Tile tileObj = tile.GetComponent<Tile>();
		tileObj.Setup(x, y, zoomLevel, index);
		mapTiles.Add(tileObj);
		tile.transform.parent = transform;

		tile.transform.localRotation = rotation;
		//tile.transform.localPosition = new Vector3(0, -y, 0);
		tile.transform.localPosition = topLeftPosition;
		tile.transform.localScale = new Vector3(TILE_SIZE, TILE_SIZE, TILE_SIZE);
		//tile.transform.position += tile.transform.forward * circleRadius;

		tileObj.LoadTile(zoomLevel);
		return tile;
	}

	protected virtual void CreatePins(string key, GameObject tile)
	{
		List<ImageStruct> imgStructs = imageStructs[key].ToList();

		foreach (ImageStruct imgStruct in imgStructs)
		{
			GameObject pin = CreatePin(tile, imgStruct);
			Pin p = pin.GetComponent<Pin>();
			p.gameObject.layer = Map.PIN_LAYER;
		}
	}

	protected virtual GameObject CreatePin(GameObject tile, ImageStruct p)
	{
		GameObject pin = Instantiate(pinPrefab);
		pin.gameObject.layer = Map.PIN_LAYER;
		pin.layer = Map.PIN_LAYER;

		pin.transform.parent = tile.transform;
		pin.transform.localPosition = new Vector3(
			RelativePos(p.pos).x - 0.5f,
			-RelativePos(p.pos).y + 0.5f,
			-0.01f - UnityEngine.Random.Range(0.0001f, 0.0002f));


		//Random.Range(-0.1f, -0.2f)
		pin.transform.rotation = tile.transform.rotation;
		Pin pin_scr = pin.GetComponent<Pin>();
		pin_scr.map = this;
		pin_scr.cluster = p.cluster;
		pinList.Add(pin_scr);

		// Add scale based on cluster size ...
		pin_scr.SetScale();
		//pin.transform.localScale = ClusterScale.GetClusterScale(p.cluster.size);

		return pin;
	}
	

	protected virtual void SetTileSize()
	{
		// Get the minimum x and y tile values at the base zoom level...
		int real_xmin = (int)long2tilex_float(GPS_MIN_LONG, BASE_ZOOMLEVEL);
		int real_ymax = (int)lat2tiley_float(GPS_MIN_LAT, BASE_ZOOMLEVEL);

		int real_xmax = (int)long2tilex_float(GPS_MAX_LONG, BASE_ZOOMLEVEL);
		int real_ymin = (int)lat2tiley_float(GPS_MAX_LAT, BASE_ZOOMLEVEL);
		//Debug.Log("\txmin: " + xmin + ", ymin: " + ymin);
		//Debug.Log("\txmax: " + xmax + ", ymax: " + ymax);

		// Determine how many tiles that will be...
		int xSize = real_xmax - real_xmin + 1;
		int ySize = real_ymax - real_ymin + 1;
		int numTiles = xSize * ySize;

		// And scale the TILE_SIZE appropriately
		int maxTiles = (int) powArray[BASE_ZOOMLEVEL];
		TILE_SIZE = BASE_TILE_SIZE * ((float)maxTiles / (float)numTiles);
		Debug.Log("Setting TILE_SIZE to: " + TILE_SIZE);
	}

	protected virtual void SetMapBoundingBox()
	{
		// First, we determine what tiles we are going to load, by checking how many 
		// pins they have, and determine min/max (x, y) coordinates
		if (CUT_OFF_MAP)
		{
			//Debug.Log("Cutting map off!");

			for (int x = 0; x < mapwidth; x++)
			{
				for (int y = 0; y < mapheight; y++)
				{
					string key = x + "," + y;

					if (imageStructs[key].Count<ImageStruct>() > 0)
					{
						// We include this tile into our map, get min/max etc
						xmin = (x < xmin) ? x : xmin;
						ymin = (y < ymin) ? y : ymin;

						xmax = (x > xmax) ? x : xmax;
						ymax = (y > ymax) ? y : ymax;
					}
				}
			}

			//Debug.Log("Found xmin: " + xmin + ", xmax: " + xmax + ", ymin: " + ymin + ", ymax: " + ymax);

			// Load a bit extra, dependant on xmin/xmax and ymin/ymax
			xmin = Math.Max(0, (int)(xmin * (1.0 - extraXPercentage)));
			ymin = Math.Max(0, (int)(ymin * (1.0 - extraYPercentage)));

			xmax = Math.Min(mapwidth, (int)(xmax * (1.0 + extraXPercentage)));
			ymax = Math.Min(mapheight, (int)(ymax * (1.0 + extraYPercentage)));
		}
		else
		{
			//Debug.Log("Not cutting map off!");

			xmin = ymin = 0;
			xmax = mapwidth - 1;
			ymax = mapheight - 1;
		}

		//Debug.Log("Using xmin: " + xmin + ", xmax: " + xmax + ", ymin: " + ymin + ", ymax: " + ymax);
	}

	protected virtual void SetCenterPositionOfData()
	{
		int xcenter = (xmax + xmin) / 2;
		int ycenter = (ymax + ymin) / 2;
		dataCenterPos = TPosTopLeftCorner[xcenter, ycenter];
	}

	protected virtual void ConstructStorage()
	{
		System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();

		int index = 0;

		for (int x = xmin; x <= xmax; x++)
		{
			for (int y = ymin; y <= ymax; y++)
			{
				string key = x + "," + y;

				// Load lower quality tile by default
				GameObject tile = CreateTile(QRot[index], TPosTopLeftCorner[x, y], 
					index, x, y, TileMap.MIN_ZOOMLEVEL);
				CreatePins(key, tile);
			}

			++index;
		}

		doneloading = true;
		stopwatch.Stop();
		//Debug.Log("Done creating " + mapTiles.Count + " tiles and " + pinList.Count +
		//	" pins, took " + stopwatch.ElapsedMilliseconds + " ms!");
	}

	protected virtual void CalculateTileRotations()
	{

	}

	protected virtual void CalculateTilePositions()
	{

	}

	protected virtual float GetVerticalDistance(Vector3 playerPos, Tile tile)
	{
		float yDist = TPosTopLeftCorner[tile.x, tile.y].y - playerPos.y;
		return yDist * yDist;
	}
	
	protected virtual float GetHorizontalDistance(Vector3 playerPos, Tile tile)
	{
		// "Cache" the corner vectors
		Vector3 topRight = TPosTopRightCorner[tile.x, tile.y];
		Vector3 bottomLeft = TPosBottomLeftCorner[tile.x, tile.y];

		// Bottom left corner
		float xDist = playerPos.x - bottomLeft.x;
		float zDist = playerPos.z - bottomLeft.z;
		bool playerInsideX = xDist >= 0.0;
		bool playerInsideZ = zDist >= 0.0;

		// Top right corner
		float xDist2 = playerPos.x - topRight.x;
		float zDist2 = playerPos.z - topRight.z;
		playerInsideX = playerInsideX && xDist2 <= 0.0;
		playerInsideZ = playerInsideZ && zDist2 <= 0.0;

		// Check if the player is inside the tile... Then return distance 0
		if (playerInsideX && playerInsideZ)
		{
			return 0;
		}

		// Make distances absolute so we can compare them...
		xDist = Math.Abs(xDist);
		zDist = Math.Abs(zDist);
		xDist2 = Math.Abs(xDist2);
		zDist2 = Math.Abs(zDist2);

		// Get closest distance to tile edge, either x or z distance...
		xDist = (xDist < xDist2) ? xDist : xDist2;
		zDist = (zDist < zDist2) ? zDist : zDist2;
		return xDist + zDist;
	}

	protected virtual int GetTileZoom(Vector3 playerPos, Tile tile)
	{
		// Get maximum vertical zoom level based on vertical distance
		float distance = GetVerticalDistance(playerPos, tile);
		int maxZoom = TileMap.MAX_ZOOMLEVEL;

		for (int i = ZOOM_LEVEL_PER_VERTICAL_DISTANCE.Length - 1; i >= 0; --i)
		{
			if (distance <= ZOOM_LEVEL_PER_VERTICAL_DISTANCE[i])
			{
				maxZoom = MIN_ZOOMLEVEL + i;
				break;
			}
		}

		//Debug.Log("Player: " + playerPos + ", tile (" + tile.x + ", " + tile.y + "): " +
		//	TPosBottomLeftCorner[tile.x, tile.y] + "; " + TPosTopRightCorner[tile.x, tile.y]);
		//Debug.Log("\tmaxZoom: " + maxZoom + " (vertical distance: " + distance + ")");

		// No need to check horizontal distance if we already reached the base zoom level...
		if (maxZoom <= BASE_ZOOMLEVEL)
		{
			return maxZoom;
		}

		// Check horizontal distance to tile of the closest corner...
		distance = GetHorizontalDistance(playerPos, tile);

		// Get zoom level
		for (int i = ZOOM_LEVEL_PER_HORIZONTAL_DISTANCE.Length - 1; i >= 0; i--)
		{
			// Get horizontal zoom level
			// Adhere to the previously calculated max zoom level...
			if (MIN_ZOOMLEVEL + i <= maxZoom && distance <= ZOOM_LEVEL_PER_HORIZONTAL_DISTANCE[i])
			{
				maxZoom = MIN_ZOOMLEVEL + i;
				break;
			}
		}

		//Debug.Log("\tmaxZoom: " + maxZoom + " (horizontal distance: " + distance + ")");
		return maxZoom;
	}

	protected virtual IEnumerator LoadTiles(List<Tile> tilesToLoad)
	{
		// Loads the map tiles...
		foreach (Tile tile in tilesToLoad)
		{
			ResourceRequest request = tile.StartLoadTileAsync(tile.currentZoom);

			while (!request.isDone)
			{
				yield return null;
			}

			tile.FinishLoadTileAsync(request);
		}
	} 

	protected virtual void UpdateTileZooms()
	{
		// Only (re)load those tiles who'se zoom level needs updating and
		// aren't already being updated...		
		Vector3 playerPos = GetPlayerPos();
		List<Tile> tilesToUpdate = new List<Tile>();

		// Possible TODO: Maybe rewrite this so the closer tiles get updated first...
		// Not needed because this happens fast enough!
		int numZoomsCalculated = 0;

		while (numZoomsCalculated < TileMap.NUM_ZOOMS_TO_CALCULATE_PER_FRAME)
		{
			current_tile_index = (current_tile_index + 1) % mapTiles.Count;
			Tile tile = mapTiles[current_tile_index];

			// Skip it if its already loading a new zoom level...
			if (tile.state == TileState.Loading_New_Zoom)
			{
				continue;
			}

			// Determine new zoom for this tile...
			int newZoom = GetTileZoom(playerPos, tile);
			++numZoomsCalculated;

			if (tile.state == TileState.Not_Loaded || newZoom != tile.currentZoom)
			{
				// Add it to the queue
				tile.currentZoom = newZoom;
				tile.state = TileState.Loading_New_Zoom;
				tilesToUpdate.Add(tile);
			}

			// Only update a small number of tiles per frame...
			if (tilesToUpdate.Count >= NUM_TILES_TO_UPDATE_PER_FRAME)
			{
				break;
			}
		}

		if (tilesToUpdate.Count > 0)
		{
			StartCoroutine(LoadTiles(tilesToUpdate));
		}
	}

	protected override void Update()
	{
		base.Update();
		UpdateTileZooms();
	}

	public override void OnDisable()
	{
		base.OnDisable();
	}
}

