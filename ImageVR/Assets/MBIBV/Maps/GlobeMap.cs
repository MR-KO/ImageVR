﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;

public class GlobeMap : TileMap
{
    // movement
    public float rotation = 0f;
    public float height = 0f;
    public float distance = 0f;


	

    //protected override IEnumerator ConstructStorage6Lazily()
    //{
    //    Quaternion[] QRot = new Quaternion[tileRotations.Length];
    //    for (int r = 0; r < tileRotations.Length; r++)
    //    {
    //        QRot[r] = QuaterinionRotation(tileRotations[r]);
    //    }

    //    int index = 0;

    //    for (int i = 0; i < mapwidth; i++)
    //    {
    //        for (int j = 0; j < mapheight; j++)
    //        {

    //            string key = i + "," + j;

    //            GameObject tile = Instantiate(tilePrefab);

    //            Tile tile_scr = tile.GetComponent<Tile>();
    //            tile.transform.parent = this.transform;
    //            tile.transform.localRotation = QRot[index];
    //            tile.transform.localPosition = new Vector3(0, -j, 0);
    //            tile.transform.position += tile.transform.forward * circleRadius;


    //            tile_scr.x = i;
    //            tile_scr.y = j;
    //            tile_scr.index = index;
    //            tile_scr.LoadTile(6);


    //            List<ImageStruct> pins = Images[key].ToList();
    //            int pindeleter = -1;
    //            foreach (ImageStruct p in pins)
    //            {
    //                pindeleter++;
    //                if (pindeleter % 20 != 0)
    //                    continue;


    //                GameObject pin = Instantiate(pinPrefab);
    //                pin.transform.parent = tile.transform;
    //                pin.transform.localPosition = new Vector3(RelativePos(p.pos).x - 0.5f, -RelativePos(p.pos).y + 0.5f, -0.01f - Random.Range(0.0001f, 0.0002f));
    //                pin.transform.rotation = tile.transform.rotation;
    //                Pin pin_scr = pin.GetComponent<Pin>();
    //                pin_scr.map = this;
    //                pin_scr.url = p.url;
    //            }

    //            yield return null;
    //        }

    //        index++;
    //        if (index == 164)
    //            break;

    //    }
    //    doneloading = true;
    //}

    //IEnumerator CreateTile(int i, int j, int index)
    //{
    //    string key = i + "," + j;

    //    //yield return Ninja.JumpToUnity;
    //    GameObject tile = Instantiate(tilePrefab);

    //    Tile tile_scr = tile.GetComponent<Tile>();

    //    //yield return Ninja.JumpBack;
    //    tile.transform.parent = this.transform;
    //    //tile.transform.localPosition = new Vector3(i - 33672, -j + 21640);

    //    tile.transform.localPosition = new Vector3(tilePositions[index].x, -j + 86416, tilePositions[index].y);

    //    tile.transform.localRotation = /*Quaternion.Euler(0,tileRotations[0][index],0);*/ QuaterinionRotation(tileRotations[index]);
    //    tile_scr.x = i;
    //    tile_scr.y = j;
    //    tile_scr.index = index;
    //    tile_scr.LoadTile(18);


    //    List<ImageStruct> pins = Images[key].ToList();

    //    foreach (ImageStruct p in pins)
    //    {
    //        GameObject pin = Instantiate(pinPrefab);
    //        pin.transform.parent = tile.transform;
    //        pin.transform.localPosition = new Vector3(RelativePos(p.pos).x - 0.5f, (RelativePos(p.pos).y - 0.5f), -0.01f);
    //        pin.transform.rotation = tile.transform.rotation;
    //        Pin pin_scr = pin.GetComponent<Pin>();
    //        pin_scr.map = this;
    //        pin_scr.url = p.url;
    //    }
    //    yield return null;
    //}

    private void Update()
    {
        HandleInput();
    }

    protected override void DirectionModeZoomOut()
    {
        // CYLINDER COORDINATE NAVIGATION
        // LEFT
        if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
        {
            UnityEngine.Debug.Log("wtf");
            UnityEngine.Debug.Log(controllerLeft.GetPadPosition);
            // radius
            if (controllerLeft.GetPadPosition.y > 0.40f)
            {
                height += 0.1f;
            }
            if (controllerLeft.GetPadPosition.y < -0.40f)
            {
                height -= 0.1f;
            }
            if (controllerLeft.GetPadPosition.x > 0.40f)
            {
                rotation += 0.01f;
            }
            if (controllerLeft.GetPadPosition.x < -0.40f)
            {
                rotation -= 0.01f;
            }
        }
        GrabImage(controllerLeft);

        if (controllerRight.WhilePressed(Controller.ButtonType.PadPress))
        {
            UnityEngine.Debug.Log("lol");
            UnityEngine.Debug.Log(controllerRight.GetPadPosition);
            // RIGHT
            if (controllerRight.GetPadPosition.y > 0.55f)
            {
                distance += 0.04f;
                if (distance > 9f)
                    distance = 9f;
            }
            if (controllerRight.GetPadPosition.y < -0.55f)
            {
                distance -= 0.01f;
                if (distance <= 0)
                    distance = 0;
            }
            if (controllerRight.GetPadPosition.x > 0.55f)
            {
                rotation += 0.01f;
            }
            if (controllerRight.GetPadPosition.x < -0.55f)
            {
                rotation -= 0.01f;
            }
        }

        GrabImage(controllerRight);

        CalculateBasePos();
    }

    private void CalculateBasePos()
    {
        // Calculate Position
        float x = distance * Mathf.Sin(rotation);
        float z = distance * Mathf.Cos(rotation);

        float rotx = 30 * Mathf.Sin(rotation);
        float rotz = 30 * Mathf.Cos(rotation);

        cambase_transform.position = new Vector3(x, height, z);
        cambase_transform.rotation = Quaternion.LookRotation(new Vector3(rotx, 0, rotz), Vector3.up);
    }

    protected override void FlyModeZoomOut()
    {
        // spherical inside rotation
        if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (controllerLeft.GetPadPosition.y > 0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(mainCamera.transform.forward);
            }
            if (controllerLeft.GetPadPosition.y < -0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(-mainCamera.transform.forward);
            }
            if (controllerLeft.GetPadPosition.x < -0.40f)
            {
                cambase_transform.Rotate(0, 2, 0);
            }
            if (controllerLeft.GetPadPosition.x > 0.40f)
            {
                cambase_transform.Rotate(0, -2, 0);
            }
        }

        GrabImage(controllerLeft);

        if (controllerRight.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (controllerRight.GetPadPosition.y > 0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(mainCamera.transform.forward);
            }
            if (controllerRight.GetPadPosition.y < -0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(-mainCamera.transform.forward);
            }
            if (controllerRight.GetPadPosition.x < -0.40f)
            {
                cambase_transform.Rotate(0, 2, 0);
            }
            if (controllerRight.GetPadPosition.x > 0.40f)
            {
                cambase_transform.Rotate(0, -2, 0);
            }
        }

        GrabImage(controllerRight);
    }


    protected override void TeleportModeZoomOut()
    {
        base.TeleportModeZoomOut();

        Vector3 moveDirection = cambase_transform.position.normalized;

        if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (cambase_transform.position.magnitude > 1)
            {
                cambase_transform.position -= moveDirection * moveSpeed * Time.deltaTime;
            }
            else
            {
                cambase_transform.position = Vector3.zero;
            }
        }

        if (controllerRight.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (cambase_transform.position.magnitude > 1)
            {
                cambase_transform.position -= moveDirection * moveSpeed * Time.deltaTime;
            }
            else
            {
                cambase_transform.position = Vector3.zero;
            }
        }

        #region old
        //distance -= 0.04f;
        //if (distance < 0f)
        //    distance = 0f;
        ////cambase_transform.position -= Vector3.forward * moveSpeed * Time.deltaTime;

        //CalculateBasePos();
        //if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
        //{
        //    //distance -= 0.04f;
        //    //if (distance > 0f)
        //    //    distance = 0f;
        //    ////cambase_transform.position -= Vector3.forward * moveSpeed * Time.deltaTime;

        //    //CalculateBasePos();
        //}
        #endregion

    }

    //private void Old()
    //{ 

    //    // Vive1
    //    SteamVR_TrackedController svrtc1 = vive1.GetComponent<SteamVR_TrackedController>();
    //    if (svrtc1.padPressed)
    //    {
    //        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)vive1.GetComponent<SteamVR_TrackedObject>().index);
    //        Vector2 pos = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0);

    //        // radius
    //        if (pos.y > 0.55f)
    //        {
    //            height += 0.1f;
    //        }
    //        if (pos.y < -0.55f)
    //        {
    //            height -= 0.1f;
    //        }
    //        if (pos.x > 0.55f)
    //        {
    //            rotation += 0.01f;
    //        }
    //        if (pos.x < -0.55f)
    //        {
    //            rotation -= 0.01f;
    //        }
    //    }
    //    if (svrtc1.triggerPressed && !previousLeftTrigger)
    //    {
    //        if (leftImage)
    //            Destroy(leftImage);

    //        if (svrtc1.GetComponent<SteamVR_LaserPointer>().previousContact != null)
    //        {
    //            GameObject obj = svrtc1.GetComponent<SteamVR_LaserPointer>().previousContact.gameObject;
    //            if (obj.GetComponent<ImageScript>() != null)
    //            {
    //                leftImage = Instantiate(imagePrefab);
    //                leftImage.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
    //                leftImage.transform.position = svrtc1.transform.position;


    //                leftImage.transform.parent = vive1.gameObject.transform;
    //                leftImage.transform.localRotation = Quaternion.Euler(0, 0, 0);
    //                //= leftImage.transform.parent.rotation;
    //                leftImage.transform.Translate(new Vector3(0, 0.2f, 0));
    //                leftImage.GetComponent<Renderer>().material.mainTexture = obj.GetComponent<Renderer>().material.mainTexture;
    //                leftImage.layer = 9;
    //            }
    //        }


    //    }

    //    previousLeftTrigger = svrtc1.triggerPressed;

    //        SteamVR_TrackedController svrtc2 = vive2.GetComponent<SteamVR_TrackedController>();
    //    // Vive2
    //    if (svrtc2.padPressed)
    //    {
    //        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)vive2.GetComponent<SteamVR_TrackedObject>().index);
    //        Vector2 pos = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0);

    //        if (pos.y > 0.55f)
    //        {
    //            distance += 0.04f;
    //            if (distance > 9f)
    //                distance = 9f;
    //        }
    //        if (pos.y < -0.55f)
    //        {
    //            distance -= 0.01f;
    //            if (distance <= 0)
    //                distance = 0;
    //        }
    //        if (pos.x > 0.55f)
    //        {
    //            rotation += 0.01f;
    //        }
    //        if (pos.x < -0.55f)
    //        {
    //            rotation -= 0.01f;
    //        }

    //    }

    //    if (svrtc2.triggerPressed && !previousRightTrigger)
    //    {
    //        if (rightImage)
    //            Destroy(rightImage);

    //        if (svrtc2.GetComponent<SteamVR_LaserPointer>().previousContact != null)
    //        {


    //            GameObject obj = svrtc2.GetComponent<SteamVR_LaserPointer>().previousContact.gameObject;
    //            if (obj.GetComponent<ImageScript>() != null)
    //            {
    //                rightImage = Instantiate(imagePrefab);
    //                rightImage.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
    //                rightImage.transform.position = svrtc2.transform.position;

    //                rightImage.transform.parent = vive2.transform;
    //                rightImage.transform.localRotation = Quaternion.Euler(0, 0, 0);//= rightImage.transform.parent.rotation;
    //                rightImage.transform.Translate(new Vector3(0, 0.2f, 0));
    //                rightImage.GetComponent<Renderer>().material.mainTexture = obj.GetComponent<Renderer>().material.mainTexture;
    //                rightImage.layer = 9;
    //            }
    //        }

    //    }
    //    previousRightTrigger = svrtc2.triggerPressed;



    //}
}