﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

using System.Data;

public enum MovementMode { Coordinate, Fly, Teleport };

public enum FilterInterfaceActivity { None, ControllerLeft, ControllerRight };

[RequireComponent(typeof(AudioSource))]
public class Map : MonoBehaviour
{
	// Constant that points to the LSC_DATA folder...
	// TODO: Read this from a config file...
	protected static string LSC_DATA_FOLDER;
	protected static string LSC_DATA_CONFIG_FILE = "PathToLSCData.txt";

	// GPS bounds of the data... Will be adjusted later on
	protected double GPS_MIN_LAT = double.MaxValue;
	protected double GPS_MIN_LONG = double.MaxValue;

	protected double GPS_MAX_LAT = double.MinValue;
	protected double GPS_MAX_LONG = double.MinValue;

	// Constant for all the pin and imagestruct layers
	public static int PIN_LAYER = 8;
	public static int IMAGE_BILLBOARD_LAYER = 9;

	protected float moveSpeed = 2.5f;
	protected int numFramesZoomingOut = 0; // Go faster when zooming out more...
	protected float extraZoomOutSpeed = 0.1f; // For a bit faster startup

	public static bool AllowInput = true;
	public static bool doneloading = false;

	public UnityEngine.Random random;


	//public static readonly int zoom = 6;

	protected float circleRadius = 20.0f;

	[Header("Prefabs")]
	[Tooltip("Prefab for tiles")]
	public GameObject tilePrefab;
	[Tooltip("Prefab for image pins")]
	public GameObject pinPrefab;
	[Tooltip("Prefab for the quads to display images")]
	public GameObject imagePrefab;
	[Tooltip("Prefab for the texts to display cluster info")]
	public GameObject textPrefab;

	protected Camera mainCamera;

	protected Transform cambase_transform;

	[Header("Vive Controllers")]
	[Tooltip("Vive Controller 1")]
	public GameObject vive1;
	[Tooltip("Vive Controller 2")]
	public GameObject vive2;

	[Header("Player camera (head)")]
	[Tooltip("Game object of the 'Camera (head)' so we can get player head position")]
	public GameObject head;

	protected Controller controllerLeft;
	protected Controller controllerRight;
	protected FilterInterfaceActivity filterActivity = FilterInterfaceActivity.None;

	private SteamVR_LaserPointer laser_left;
	private SteamVR_LaserPointer laser_right;

	[Header("Movement")]
	protected ViveTeleportationScript teleportScript;

	[Header("Sounds")]
	public AudioClip pickupClip;
	public AudioClip hitClip;
	public AudioClip dropClip;
	public AudioClip loadingClip;
	public AudioClip teleportingClip;

	protected AudioSource audio = null;

	[Header("Map options")]
	[Tooltip("Number of decimals to use for the \"clustering\" of the pins")]
	// Valid range for numDecimals: 0, 1, ..., 8
	// Full range of the data, for clustering = 8
	// Best value so far = 2
	// Veeerry broad overview: 0
	public int numDecimals = 1;

	[Header("Experimentation setup")]
	[Tooltip("Use experimentation module?")]
	public bool useExperimentation = true;
	[Tooltip("Use Cathal's data (user 1)?")]
	public bool useCathalsData = true;
	[Tooltip("Use Rami's data (user 2)?")]
	public bool useRamisData = true;

	[Header("FilteringInterface")]
	[Tooltip("Gameobject for the Unity filtering interface listview")]
	public GameObject canvasObject;
	public FilterInterface filterInterface = null;

	[Header("Image Wall")]
	[Tooltip("Whether to use a curved image wall or not")]
	public bool useCurvedImageWall = false;

	public GameObject filteringInterfaceObject;
	private bool initializedFilteringInterface = false;

	public MovementMode movement = MovementMode.Teleport;

	// List of all Images along with their tiles through the different zooms
	protected ILookup<string, ImageStruct> imageStructs;

	// List of Image objects holding images from the LSC database
	protected List<Image> imagesFromDB = null;
	protected List<Image> images = new List<Image>(1); // 56450 = From DB.dbFile

	// Number of images to update visibility per frame...
	protected int updateVisibleImagesPerFrame = 50;

	// List of all tags
	protected List<Tag> tagList;
	protected List<Tag> tagListFilterInterfaceAlphabetical;
	protected List<Tag> tagListFilterInterfaceOccurrences;

	// Only include Tags in the filtering interface that occur at least this often
	public static int MIN_TAG_OCCURRENCE = 10;

	// Special tag_id representing all tags, used for filtering
	public static long TAG_ID_REPRESENTING_EVERYTHING = -1;
	public Tag allTags;

	// Special tag_id representing the absence of tags, used for filtering
	// TODO: Finish this...
	public static long TAG_ID_REPRESENTING_NO_TAGS = TAG_ID_REPRESENTING_EVERYTHING - 1;
	public Tag noTags;

	// List of all Pin objects, should be populated by child classes...
	protected List<Pin> pinList;

	// List of all clusters to be used for the pin objects...
	protected ClusterHandler clusterManager;

	// For experimenting...
	protected ExperimentManager experimentManager;

	protected virtual void Initialization()
	{
		// Read config file
		string text = System.IO.File.ReadAllText(Application.dataPath + "/"+ LSC_DATA_CONFIG_FILE);
		Map.LSC_DATA_FOLDER = text;

		// to initalize device indices
		SteamVR_Controller.GetDeviceIndex(0);

		mainCamera = Camera.main;

		teleportScript = mainCamera.GetComponent<ViveTeleportationScript>();
		teleportScript.enabled = false;

		circleRadius = 1f;

		random = new UnityEngine.Random();

		cambase_transform = mainCamera.transform.parent.transform;
		controllerLeft = new Controller(vive1);
		controllerLeft.UpdateButtons();
		controllerRight = new Controller(vive2);
		controllerRight.UpdateButtons();

		SetupOutline();
		CreateTagList();
		// SetupFilteringInterface(); Done at first Update run later on...

		ImageWallRow.tagList = tagList;
		CreateImagesList();

		// And also populate the clusters...
		//System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
		clusterManager = new ClusterHandler(images, numDecimals);
		clusterManager.SetAllClusterSizes();
		//stopwatch.Stop();

		//Debug.Log("Creating " + clusterManager.locationClusters.Count +
		//	" clusters took " + stopwatch.ElapsedMilliseconds + " ms");

		// Create pins from clusters, and not from images...
		pinList = new List<Pin>(clusterManager.locationClusters.Count);
		CreateImageLookup();

		// Setup experiment manager
		if (useExperimentation)
		{
			experimentManager = new ExperimentManager(tagList);
		}
	}

	protected void CreateTagList()
	{
		// Get all tags from the DB and initially set as active
		tagList = DB.GetTags();

		// Create a sorted copy of the taglist, based on tag name and occurrences
		tagListFilterInterfaceAlphabetical = new List<Tag>(tagList.Count);
		tagListFilterInterfaceOccurrences = new List<Tag>(tagList.Count);
		long allTagOccurrences = 0;

		foreach (Tag tag in tagList)
		{
			allTagOccurrences += tag.occurrences;

			// Filter out all tags that dont occur often enough
			if (tag.occurrences >= Map.MIN_TAG_OCCURRENCE)
			{
				tagListFilterInterfaceAlphabetical.Add(tag);
				tagListFilterInterfaceOccurrences.Add(tag);
			}
		}

		tagListFilterInterfaceAlphabetical.Sort();

		// Also create a Tag indicating all Tags...
		allTags = new Tag(
			Map.TAG_ID_REPRESENTING_EVERYTHING,
			"All tags",
			allTagOccurrences);
		ExtraOptionsHandler.allTags = allTags;

		// Also create a Tag indicating no Tags
		noTags = new Tag(
			Map.TAG_ID_REPRESENTING_NO_TAGS,
			"No tags",
			0); // Occurrences for this tag will be set later on
		noTags.enabled = false; // Disable it at first...
		ExtraOptionsHandler.noTags = noTags;

		//Debug.Log("Got " + tagList.Count + " tags total, with "
		//	+ tagListFilterInterfaceAlphabetical.Count + " for the interface");
	}

	protected void SetupFilteringInterface()
	{
		if (filteringInterfaceObject == null)
		{
			Debug.Log("Tried to setup filtering interface, failed!");
			return;
		}

		filterInterface = filteringInterfaceObject.GetComponent<FilterInterface>();
		filterInterface.Initialize(tagListFilterInterfaceAlphabetical,
			tagListFilterInterfaceOccurrences);

		// Don't render the filter interface at startup...
		canvasObject.SetActive(false);
		initializedFilteringInterface = true;
	}

	protected void CreateImagesList()
	{
		// Get all images from DB...
		IDataReader reader = DB.GetAllImages();
		imagesFromDB = Image.GetImagesFromReader(reader);
		DB.CloseReader(reader);

		if (imagesFromDB.Count == 0)
		{
			Debug.LogError("Failed to load images from database!");
			return;
		}

		int totalNumberOfImages = imagesFromDB.Count;

		// Get all the image_id's with a list of their corresponding tag_ids
		System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
		List<Tuple<long, List<long>>> imageTagsList = DB.GetAllImageTagsAsList();
		stopwatch.Stop();
		//Debug.Log("Getting all " + imageTagsList.Count +
		//	" tags per image took " + stopwatch.ElapsedMilliseconds + " ms");

		// Add all those tags to the individual images...
		Image.SetImageTags(imagesFromDB, imageTagsList);

		// Set correct path for all images... Also determine GPS bounds
		// Travel in reverse because then we can use O(1) RemoveAt function to
		// skip Cathal's/Rami's data, without disturbing the iteration

		for (int i = imagesFromDB.Count - 1; i >= 0; --i)
		{
			Image image = imagesFromDB[i];

			// Skip image if we don't use Cathals/Ramis data
			if (image.userId == "u1" && !useCathalsData) {
				imagesFromDB.RemoveAt(i);
				continue;
			}

			if (image.userId == "u2" && !useRamisData) {
				imagesFromDB.RemoveAt(i);
				continue;
			}

			// Count number of images without tags
			if (image.tags == null) {
				++noTags.occurrences;
			}

			image.path = Map.GetLSCPath() + image.path;

			// Determine lower and upper GPS bounds
			GPS_MIN_LAT = image.locLat < GPS_MIN_LAT ? image.locLat : GPS_MIN_LAT;
			GPS_MIN_LONG = image.locLong < GPS_MIN_LONG ? image.locLong : GPS_MIN_LONG;

			GPS_MAX_LAT = image.locLat > GPS_MAX_LAT ? image.locLat : GPS_MAX_LAT;
			GPS_MAX_LONG = image.locLong > GPS_MAX_LONG ? image.locLong : GPS_MAX_LONG;
		}

		//Debug.Log("Got minimum GPS, lat: " + GPS_MIN_LAT + ", long: " + GPS_MIN_LONG);
		//Debug.Log("Got maximum GPS, lat: " + GPS_MAX_LAT + ", long: " + GPS_MAX_LONG);

		// Shitty filtering has been disabled... ultimate test now...
		Debug.Log("Loaded " + imagesFromDB.Count + "/" + totalNumberOfImages +
			" images from database!");
		images = imagesFromDB;
	}

	protected virtual void CreateImageLookup()
	{

	}

	// Toggles the visibility for a list of images
	public IEnumerator UpdateFilteredImages()
	{
		System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
		int thingsDonePerFrame = 0;

		// We make a backup of all the representing Images of the pins,
		// so we can compare them later on when we update their visibility
		// to determine if we need to update the representing image...
		List<Image> representingImages = new List<Image>(pinList.Count);

		foreach (Pin pin in pinList)
		{
			representingImages.Add(pin.GetRepresentingImage());
		}

		// Then we update all the visibility of all the images...
		foreach (Image image in images)
		{
			image.ToggleVisibility(tagList, noTags.enabled);

			if (++thingsDonePerFrame == updateVisibleImagesPerFrame * 10)
			{
				thingsDonePerFrame = 0;
				yield return null;
			}
		}

		// And then we update the visibility of all the pins...
		for (int i = 0; i < pinList.Count; i++)
		{
			if (++thingsDonePerFrame == updateVisibleImagesPerFrame)
			{
				thingsDonePerFrame = 0;
				yield return null;
			}

			Pin pin = pinList[i];

			// Only hide a pin if it no longer has any active images
			// in the cluster of images of its cluster...
			if (pin.cluster.stack.GetNumActiveImages() == 0)
			{
				pin.SetInvisible();
				pin.Hide();
			}
			else
			{
				// TODO: Also update representing image if it has changed!
				//if (representingImages[i] != pin.GetRepresentingImage())
				//{
				//	pin.SetBeginState();
				//	revealScript.QueuePinLoading(pin, false);
				//}

				pin.SetScale();
				pin.SetVisible();
				pin.Show();
			}
		}

		stopwatch.Stop();
		//Debug.Log("Updating all filtered images and pins took "
		//	+ stopwatch.ElapsedMilliseconds + " ms!");
	}

	// Toggles enabled status of given tag, dont forget to call
	// StartCoroutine(UpdateFilteredImages()) afterwards!!!
	public void UpdateTag(Tag tag)
	{
		if (tag != null)
		{
			tag.enabled = !tag.enabled;

			// Special case for the "Everything" Tag...
			if (tag.id == Map.TAG_ID_REPRESENTING_EVERYTHING)
			{
				// Disable or enable all tags...
				foreach (Tag t in tagList)
				{
					t.enabled = tag.enabled;
				}
			}
		}
	}

	// Enables or disables the given tag_id, dont forget to call
	// StartCoroutine(UpdateFilteredImages()) afterwards!!!
	public void EnableTag(long tag_id)
	{
		// Check if the tag_id is in the tagList
		Tag tag = tagList.Find(x => x.id == tag_id);

		if (tag != null)
		{
			tag.enabled = true;
		}
	}

	public void DisableTag(long tag_id)
	{
		// Check if the tag_id is in the tagList
		Tag tag = tagList.Find(x => x.id == tag_id);

		if (tag != null)
		{
			tag.enabled = false;
		}
	}

	public static string GetLSCPath()
	{
		return Map.LSC_DATA_FOLDER;
	}

	protected void SetupOutline()
	{
		laser_left = vive1.GetComponent<SteamVR_LaserPointer>();
		laser_right = vive2.GetComponent<SteamVR_LaserPointer>();

		laser_left.PointerIn += Highlight;
		laser_right.PointerIn += Highlight;
		laser_left.PointerOut += UnHighlight;
		laser_right.PointerOut += UnHighlight;
	}

	protected void Highlight(object sender, PointerEventArgs e)
	{
		var iglow = e.target.GetComponent<IGlow>();
		if (iglow != null)
			iglow.Highlight();
	}

	protected void UnHighlight(object sender, PointerEventArgs e)
	{
		var iglow = e.target.GetComponent<IGlow>();
		if (iglow != null)
			iglow.UnHighlight();
	}

	protected Vector3 GetPlayerPos()
	{
		return Camera.main.transform.position;
	}

	protected void SetPlayerPos(Vector3 newPos)
	{
		cambase_transform.position = newPos;
	}

	protected virtual void TeleportModeZoomOut() { }

	protected virtual void FlyModeZoomOut() { }

	protected virtual void DirectionModeZoomOut() { }


	protected virtual void HandleInput()
	{
		if (controllerLeft != null)
		{
			controllerLeft.UpdateButtons();
		}

		if (controllerRight != null)
		{
			controllerRight.UpdateButtons();
		}

		if (AllowInput && controllerLeft != null && controllerRight != null)
		{
			GrabImage(controllerLeft);
			GrabImage(controllerRight);

			// Handle swiping through image cluster...
			NavigateThroughImageWall(controllerLeft);
			NavigateThroughImageWall(controllerRight);

			// Add handling of filtering interface input...
			HandleFilteringInterface(controllerLeft);
			HandleFilteringInterface(controllerRight);

			// Check if either controllers should have movement enabled/disabled
			MovementActive active1 = vive1.GetComponent<MovementActive>();
			active1.movementActive = !controllerLeft.filterInterfaceActive && !controllerLeft.ImageWallActive;

			MovementActive active2 = vive2.GetComponent<MovementActive>();
			active2.movementActive = !controllerRight.filterInterfaceActive && !controllerRight.ImageWallActive;

			switch (movement)
			{
				case MovementMode.Fly:
					teleportScript.enabled = false;
					FlyModeZoomOut();
					break;

				case MovementMode.Coordinate:
					teleportScript.enabled = false;
					DirectionModeZoomOut();
					break;

				case MovementMode.Teleport:
					teleportScript.enabled = true;
					TeleportModeZoomOut();
					break;
			}
		}
	}

	protected virtual void ResetPose()
	{
		SteamVR.instance.hmd.ResetSeatedZeroPose();
	}

	protected virtual void GrabImage(Controller controller)
	{
		// Don't do anything if the filter interface is active...
		if (controller == null || controller.filterInterfaceActive)
		{
			return;
		}

		if (controller.OnPress(Controller.ButtonType.Trigger))
		{
			if (controller.ImageWallActive)
			{
				if (controller.coroutineLoadingImageWall != null)
				{
					StopCoroutine(controller.coroutineLoadingImageWall);
				}

				controller.DestroyHoldingImageWall();
				audio.clip = dropClip;
				audio.Play();
			}

			GameObject target = controller.LaserObject;

			if (target == null)
			{
				return;
			}

			ImageScript imscr = target.GetComponent<ImageScript>();
			ClusterBase cluster = null;
			IGlow highlight = null;

			// Get cluster from revealed pin
			if (imscr != null)
			{
				cluster = imscr.cluster;
				highlight = imscr;
			}
			else
			{
				// Also get the cluster if the pin isn't revealed yet
				Pin pinscr = target.GetComponent<Pin>();

				if (pinscr == null)
				{
					return; // Probably clicked on the map or something
				}

				cluster = pinscr.cluster;
				highlight = pinscr;
			}

			// Load new image cluster
			if (cluster != null)
			{
				controller.CreateImageWallNav(imagePrefab, textPrefab,
					highlight, cluster, useCurvedImageWall);
				controller.coroutineLoadingImageWall = StartCoroutine(
					controller.imageWall.FinalizeSetup(imagePrefab,
					textPrefab, controller.vive.transform));

				audio.clip = loadingClip;
				audio.Play();

				// Update number of stacks clicked for this experiment
				if (useExperimentation && experimentManager.IsTesting())
				{
					experimentManager.current.nrOfStacksClicked++;

					// Get current image and add it to the tracking
					Image currentImage = controller.imageWall.GetCurrentImage();
					Debug.Log("Grabbed image with id: " + currentImage.id);
					experimentManager.current.AddImage(currentImage);
				}
			}
		}
	}

	protected virtual void NavigateThroughImageWall(Controller controller)
	{
		// Check if controller doesn't have the filter interface active
		if (controller == null || controller.filterInterfaceActive)
		{
			return;
		}

		// Check if the controller has the image wall active
		if (!controller.ImageWallActive || controller.imageWall == null)
		{
			return;
		}

		// Check if we need to navigate through the cluster
		bool padPressedOnce = controller.OnPress(Controller.ButtonType.PadPress);
		bool movedLeft = false;
		bool movedRight = false;
		bool movedUp = false;
		bool movedDown = false;

		// Upper side touch pad
		if (controller.OnEdge(Controller.TouchPadEdge.Top))
		{
			if (padPressedOnce || controller.HeldDownLongEnough(Controller.TouchPadEdge.Top))
			{
				movedUp = true;
				controller.ResetFastScrolling();
				controller.imageWall.MoveUp();
				StartCoroutine(controller.imageWall.UpdateImageWall());
			}
		}

		// Lower side touch pad
		if (controller.OnEdge(Controller.TouchPadEdge.Bottom))
		{
			if (padPressedOnce || controller.HeldDownLongEnough(Controller.TouchPadEdge.Bottom))
			{
				movedDown = true;
				controller.ResetFastScrolling();
				controller.imageWall.MoveDown();
				StartCoroutine(controller.imageWall.UpdateImageWall());
			}
		}

		// Right side touch pad
		if (controller.OnEdge(Controller.TouchPadEdge.Right))
		{
			movedRight = padPressedOnce;

			if (!movedRight && controller.HeldDownLongEnough(Controller.TouchPadEdge.Right))
			{
				movedRight = controller.numFramesHeldRightPad % 3 == 0;
			}

			if (movedRight)
			{
				controller.imageWall.MoveRight();
				StartCoroutine(controller.imageWall.UpdateImageWall());
			}
		}

		// Left side touch pad
		if (controller.OnEdge(Controller.TouchPadEdge.Left))
		{
			movedLeft = padPressedOnce;

			if (!movedLeft && controller.HeldDownLongEnough(Controller.TouchPadEdge.Left))
			{
				movedLeft = controller.numFramesHeldLeftPad % 3 == 0;
			}

			if (movedLeft)
			{
				controller.imageWall.MoveLeft();
				StartCoroutine(controller.imageWall.UpdateImageWall());
			}
		}

		// Reset fast scrolling mode if we have reached the start or end of a row
		if (movedLeft || movedRight)
		{
			if (controller.imageWall.AtStartOrEndOfCurrentImageRow())
			{
				controller.ResetFastScrolling();
			}
		}

		// Update number of image wall interactions for this experiment
		if (useExperimentation && experimentManager.IsTesting())
		{
			if (movedLeft)
			{
				experimentManager.current.nrOfImageRowPreviousPresses++;
			}

			if (movedRight)
			{
				experimentManager.current.nrOfImageRowNextPresses++;
			}

			if (movedDown)
			{
				experimentManager.current.nrOfImageRowSwitchesDownPresses++;
			}

			if (movedUp)
			{
				experimentManager.current.nrOfImageRowSwitchesUpPresses++;
			}

			if (movedLeft || movedRight || movedDown || movedUp)
			{
				// Get new image and add it to the tracking
				Image currentImage = controller.imageWall.GetCurrentImage();
				Debug.Log("Grabbed image with id: " + currentImage.id);
				experimentManager.current.AddImage(currentImage);
			}
		}
	}

	private void HandleMenuButtonPress(Controller controller)
	{
		// Check if the menu button was pressed and if its active...
		if (controller.OnPress(Controller.ButtonType.Menu))
		{
			// Check if filter interface is active on either controller, to make sure
			// that the controller statusses are set properly
			if (controller == controllerLeft)
			{
				// Check if we need to move menu from right to left controller...
				if (filterActivity == FilterInterfaceActivity.ControllerRight)
				{
					controllerRight.filterInterfaceActive = false;
					controller.filterInterfaceActive = true;
					filterActivity = FilterInterfaceActivity.ControllerLeft;
				}
				else
				{
					// Toggle filter interface menu on/off
					controller.filterInterfaceActive = !controller.filterInterfaceActive;

					if (controller.filterInterfaceActive)
					{
						filterActivity = FilterInterfaceActivity.ControllerLeft;
					}
					else
					{
						filterActivity = FilterInterfaceActivity.None;
					}
				}
			}
			else
			{
				// Check if we need to move menu from left to right controller
				if (filterActivity == FilterInterfaceActivity.ControllerLeft)
				{
					controllerLeft.filterInterfaceActive = false;
					controller.filterInterfaceActive = true;
					filterActivity = FilterInterfaceActivity.ControllerRight;
				}
				else
				{
					// Toggle filter interface menu on/off
					controller.filterInterfaceActive = !controller.filterInterfaceActive;

					if (controller.filterInterfaceActive)
					{
						filterActivity = FilterInterfaceActivity.ControllerRight;
					}
					else
					{
						filterActivity = FilterInterfaceActivity.None;
					}
				}
			}

			// Render filter interface on this controller...
			if (filterActivity != FilterInterfaceActivity.None)
			{
				canvasObject.SetActive(true);
				controller.AssignFilteringMenu(canvasObject);
				audio.clip = hitClip;
				audio.Play();
			}
			else
			{
				canvasObject.SetActive(false);
			}
		}
	}

	protected virtual void HandleFilteringInterface(Controller controller)
	{
		if (controller == null)
		{
			return;
		}

		HandleMenuButtonPress(controller);

		// If the filtering interface is not active on this controller, don't do anything...
		if (!controller.filterInterfaceActive)
		{
			return;
		}

		// Check if we need to update the filtering interface selection
		bool padPressedOnce = controller.OnPress(Controller.ButtonType.PadPress);
		int newIndex = filterInterface.GetCurrentIndexForActiveMenu();

		if (controller.OnEdge(Controller.TouchPadEdge.Top)) {
			if (padPressedOnce || controller.HeldDownLongEnough(Controller.TouchPadEdge.Top))
			{
				if (controller.numFramesHeldTopPad % 3 == 1)
				{
					newIndex = Math.Max(0, newIndex - 1);
				}
			}
		}

		if (controller.OnEdge(Controller.TouchPadEdge.Bottom))
		{
			if (padPressedOnce || controller.HeldDownLongEnough(Controller.TouchPadEdge.Bottom))
			{
				if (controller.numFramesHeldBottomPad % 3 == 1)
				{
					newIndex = Math.Min(newIndex + 1, filterInterface.GetMaxIndexForActiveMenu());
				}
			}
		}

		// Do selection of correct tag item
		if (newIndex != filterInterface.GetCurrentIndexForActiveMenu())
		{
			filterInterface.SetNewIndexForActiveMenu(newIndex);
			filterInterface.SelectItem();

			// If we are at the tag list view, then also scroll the a-z list in sync...
			if (filterInterface.currentActiveMenu == FilterInterface.ActiveMenu.TagListMenu)
			{
				filterInterface.HandleSpecialActionsForSelectItem();
			}
		}

		// Check if we need to go to the left/right menu's
		bool goToLeftMenu = controller.OnEdge(Controller.TouchPadEdge.Left) && padPressedOnce;
		bool goToRightMenu = controller.OnEdge(Controller.TouchPadEdge.Right) && padPressedOnce;

		if (goToLeftMenu)
		{
			filterInterface.GoToLeftMenu();
		}

		if (goToRightMenu)
		{
			filterInterface.GoToRightMenu();
		}

		// Update visibility of tag item and images, check if trigger was pressed
		if (controller.OnPress(Controller.ButtonType.Trigger))
		{
			object obj = filterInterface.GetSelectedItem();

			if (obj == null)
			{
				return;
			}

			// Maybe (hopefully) its a Tag, but no problem if it's not
			Tag tag = obj as Tag;
			bool updatedTags = false;

			if (tag != null)
			{
				// Update visibility of tag item and images...
				updatedTags = true;
				UpdateTag(tag);
				filterInterface.UpdateSelectedTagStatus();
				StartCoroutine(UpdateFilteredImages());

				// Update number of tags enabled/disabled for this experiment
				if (useExperimentation && experimentManager.IsTesting())
				{
					// Get current Tag and add it to the tracking
					// Also handles the special "All Tags" case properly
					Debug.Log("Enabled/Disabled tag: " + tag);
					experimentManager.current.AddTag(tag);
				}
			}

			// Select the item and also handle any possible special actions required
			filterInterface.SelectItem();
			filterInterface.HandleSpecialActionsForSelectItem();

			if (updatedTags)
			{
				// TODO: If we're enabling or disabling tags, also scale all the clusters again...
			}
		}
	}

	protected void HandleSounds()
	{
		// Play sound after image wall has fully loaded
		foreach (Controller controller in new Controller[2] { controllerLeft, controllerRight })
		{
			if (controller != null && controller.ImageWallActive
			&& controller.imageWall.state == ImageWallState.Done)
			{
				controller.imageWall.state = ImageWallState.PlayedSound;
				audio.clip = pickupClip;
				audio.Play();
			}
		}

		// Play sound when teleporting starts...
		if (teleportScript.JustStartedTeleporting)
		{
			audio.clip = teleportingClip;
			audio.Play();
		}

		// And stop playing it when teleporting ends
		if (teleportScript.JustFinishedTeleporting)
		{
			audio.Stop();
		}
	}

	protected void HandleExperimentation()
	{
		experimentManager.UpdateExperimentStatus();

		if (useExperimentation && experimentManager.IsTesting())
		{
			// Keep track of teleporting things for this experiment
			if (teleportScript.JustStartedTeleporting)
			{
				experimentManager.current.nrOfTeleportations++;
				experimentManager.current.totalTeleportDistance += teleportScript.teleportDistance;
				experimentManager.current.teleportingStopwatch.Start();
			}

			if (teleportScript.JustFinishedTeleporting)
			{
				experimentManager.current.teleportingStopwatch.Stop();
			}

			// Update time spent idling on the map for this experiment
			bool idling = !controllerLeft.filterInterfaceActive && !controllerLeft.ImageWallActive
				&& !controllerRight.filterInterfaceActive && !controllerRight.ImageWallActive;

			if (idling)
			{
				experimentManager.current.idlingStopwatch.Start();
			}
			else
			{
				experimentManager.current.idlingStopwatch.Stop();
			}

			// Update time spent navigating the image wall for this experiment
			if (controllerLeft.ImageWallActive || controllerRight.ImageWallActive)
			{
				experimentManager.current.navigatingStopwatch.Start();
			}
			else
			{
				experimentManager.current.navigatingStopwatch.Stop();
			}

			// Update time spent filtering images for this experiment
			if (filterActivity != FilterInterfaceActivity.None)
			{
				experimentManager.current.filteringStopwatch.Start();
			}
			else
			{
				experimentManager.current.filteringStopwatch.Stop();
			}
		}		
	}

	protected virtual void Update()
	{
		if (!initializedFilteringInterface)
		{
			SetupFilteringInterface();
			initializedFilteringInterface = true;
		}

		if (audio == null)
		{
			audio = GetComponent<AudioSource>();
		}

		HandleInput();
		HandleSounds();
		HandleExperimentation();
	}

	public virtual void OnDisable()
	{
		Resources.UnloadUnusedAssets();
	}
}
