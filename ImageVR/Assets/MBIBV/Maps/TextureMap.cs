﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System;



public class TextureMap : Map
{
	// For movement
	protected float phi = 0;
    protected float rho = 0;
    protected float radius = 10f;

    //public Vector2[] tilePositions;
    //public float[] tileRotations;

    void Start()
    {
		Initialization();
    }

    protected override void Initialization()
    {
        base.Initialization();

        circleRadius = 20.0f;

		//StartCoroutine(CreatePinsLazily());
		CreatePins();
	}
	
	protected virtual IEnumerator CreatePinsLazily()
	{
		//Debug.Log("Loading all pins and their images...");
		DateTime startTime = DateTime.Now;
		GameObject pin;
		Pin p;

		//foreach (Tuple<Vector3, string> t in pinList)
		//{
		//	pin = CreatePin(t.First, t.Second);
		//	p = pin.GetComponent<Pin>();
		//	yield return null;
		//	StartCoroutine(p.LazyLoadImage());
		//	yield return null;
		//}

		foreach (Image image in images)
		{
			pin = CreatePin(image);
			p = pin.GetComponent<Pin>();
			yield return null;
		}

		TimeSpan duration = DateTime.Now - startTime;
		Debug.Log("Done loading all pins, took " + duration.TotalMinutes + " minutes!");
		//yield return null;
	}

	protected void CreatePins()
	{
		//Debug.Log("Loading all pins and their images...");
		DateTime startTime = DateTime.Now;

		//foreach (Tuple<Vector3, string> t in pinList)
		//{
		//	GameObject pin = CreatePin(t.First, t.Second);
		//	Pin p = pin.GetComponent<Pin>();
		//	p.LoadImage();
		//}

		foreach (Image image in images)
		{
			GameObject pin = CreatePin(image);
		}

		TimeSpan duration = DateTime.Now - startTime;
		Debug.Log("Done loading all pins, took " + duration.TotalSeconds + " seconds!");
	}

	protected override void CreateImageLookup()
	{
		bool showSomeImages = true;

		for (int i = 0; i < images.Count; ++i)
		{
			if (!showSomeImages)
			{
				images[i].path = "";
			}
		}
	}

	//protected virtual GameObject CreatePin(Vector3 pos, string path)
	//{
	//	GameObject pin = Instantiate(pinPrefab);
	//	Pin p = pin.GetComponent<Pin>();

	//	pin.transform.parent = this.transform;
	//	pin.transform.localPosition = pos;
	//	pin.GetComponent<Renderer>().material.color = Color.yellow;
	//	p.path = path;
	//	p.map = this;
	//	return pin;
	//}

	protected virtual GameObject CreatePin(Image image)
	{
		GameObject pin = Instantiate(pinPrefab);
		Pin p = pin.GetComponent<Pin>();

		pin.transform.parent = transform;
		Vector3 pos = ReverseNormals.LatLongToWorld(image.locLat, image.locLong, 20f);
		pin.transform.localPosition = pos;
		pin.GetComponent<Renderer>().material.color = Color.yellow;
		p.map = this;
		pinList.Add(p);
		return pin;
	}
}

public class Tuple<T1, T2>
{
    public T1 First { get; private set; }
    public T2 Second { get; private set; }
    internal Tuple(T1 first, T2 second)
    {
        First = first;
        Second = second;
    }
}

public static class Tuple
{
    public static Tuple<T1, T2> New<T1, T2>(T1 first, T2 second)
    {
        var tuple = new Tuple<T1, T2>(first, second);
        return tuple;
    }
}
