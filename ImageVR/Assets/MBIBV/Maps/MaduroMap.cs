﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;


public class MaduroMap : TileMap
{
	public GameObject lineRendererPrefab;

	private static float MAX_ZOOM_OUT_HEIGHT = 35.0f;
	private static float MIN_PIN_HEIGHT_LEVEL_0 = -0.05f;
	private static float MAX_PIN_HEIGHT_LEVEL_0 = -0.5f;


    protected override GameObject CreatePin(GameObject tile, ImageStruct p)
    {
        var pin = base.CreatePin(tile, p);

		//pin.transform.localScale = new Vector3(0.03f, 0.03f, 0.03f);
		//pin.transform.localScale *= 0.1f;
		pin.transform.localPosition = pin.transform.localPosition + new Vector3(
			0, 0, Random.Range(MaduroMap.MIN_PIN_HEIGHT_LEVEL_0, MaduroMap.MAX_PIN_HEIGHT_LEVEL_0));

        GameObject lineRenderer = Instantiate(lineRendererPrefab);
        lineRenderer.transform.parent = tile.transform;
        lineRenderer.transform.localPosition = new Vector3(
			RelativePos(p.pos).x - 0.5f,
			-RelativePos(p.pos).y + 0.5f,
			0);
		Vector3 offset = new Vector3(0.0f, -pin.transform.localScale.y * 1.25f, 0.0f);
		lineRenderer.GetComponent<LineRenderer>().SetPositions(new Vector3[] {
			lineRenderer.transform.position, pin.transform.position + offset });

		// Add the line thing to the pin object, so we can toggle its visibility
		// as well via the filtering
		Pin pinObject = pin.GetComponent<Pin>();
		pinObject.instantiatedLinePrefab = lineRenderer;
        return pin;
    }

    protected override void CalculateTileRotations()
    {
        QRot = new Quaternion[mapwidth];

        for (int r = 0; r < mapwidth; r++)
        {
            QRot[r] = Quaternion.Euler(90, 0, 0);
        }
    }

    protected override void CalculateTilePositions()
    {
        TPosBottomLeftCorner	= new Vector3[mapwidth, mapheight];
        TPosTopLeftCorner		= new Vector3[mapwidth, mapheight];
        TPosBottomRightCorner	= new Vector3[mapwidth, mapheight];
        TPosTopRightCorner		= new Vector3[mapwidth, mapheight];

		for (int i = 0; i < mapwidth; i++)
        {
            for (int j = 0; j < mapheight; j++)
            {
				Vector3 tilePos = new Vector3(i * TILE_SIZE, 0, -j * TILE_SIZE);
				TPosTopLeftCorner[i, j] = tilePos;
				TPosTopRightCorner[i, j] = tilePos + new Vector3(TILE_SIZE, 0, 0);
				TPosBottomLeftCorner[i, j] = tilePos + new Vector3(0, 0, -TILE_SIZE);
				TPosBottomRightCorner[i, j] = tilePos + new Vector3(TILE_SIZE, 0, -TILE_SIZE);
			}
        }
    }

	// Cartesian with Camera Aim
	protected override void DirectionModeZoomOut()
	{
		base.DirectionModeZoomOut();

		// Calc Forward without X and Z rotation
		Vector3 cam_rot = mainCamera.transform.rotation.eulerAngles;
		cam_rot.x = 0;
		cam_rot.z = 0;
		Quaternion q_cam_rot = Quaternion.Euler(cam_rot);

		// Don't move if the controller has the menu or a cluster of images active
		if (controllerLeft != null && !controllerLeft.filterInterfaceActive && !controllerLeft.ImageWallActive)
		{
			if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
			{
				if (controllerLeft.GetPadPosition.y > 0.40f)
				{
					cambase_transform.position += q_cam_rot * Vector3.forward * moveSpeed * Time.deltaTime;
				}
				if (controllerLeft.GetPadPosition.y < -0.40f)
				{
					cambase_transform.position += q_cam_rot * Vector3.forward * -moveSpeed * Time.deltaTime;
				}
				if (controllerLeft.GetPadPosition.x < -0.40f)
				{
					cambase_transform.position += q_cam_rot * Vector3.left * -moveSpeed * Time.deltaTime;
				}
				if (controllerLeft.GetPadPosition.x > 0.40f)
				{
					cambase_transform.position += q_cam_rot * Vector3.right * -moveSpeed * Time.deltaTime;
				}
			}
		}

		// Don't move if the controller has the menu or a cluster of images active
		if (controllerRight != null && !controllerRight.filterInterfaceActive && !controllerRight.ImageWallActive)
		{
			if (controllerRight.WhilePressed(Controller.ButtonType.PadPress))
			{
				if (controllerRight.GetPadPosition.y > 0.40f)
				{
					cambase_transform.position += q_cam_rot * Vector3.up * moveSpeed * Time.deltaTime;
				}
				if (controllerRight.GetPadPosition.y < -0.40f)
				{
					cambase_transform.position += q_cam_rot * Vector3.up * -moveSpeed * Time.deltaTime;
				}
				if (controllerRight.GetPadPosition.x < -0.40f)
				{
					cambase_transform.Rotate(0, 2, 0);
				}
				if (controllerRight.GetPadPosition.x > 0.40f)
				{
					cambase_transform.Rotate(0, -2, 0);
				}
			}
		}
	}

	// Cartesian with Camera Aim
	protected override void FlyModeZoomOut()
	{
		base.FlyModeZoomOut();

		// Don't move if the controller has the menu or a cluster of images active
		if (controllerLeft != null && !controllerLeft.filterInterfaceActive && !controllerLeft.ImageWallActive)
		{
			if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
			{
				if (controllerLeft.GetPadPosition.y > 0.40f)
				{
					cambase_transform.position += mainCamera.transform.forward * moveSpeed * Time.deltaTime;
				}
				if (controllerLeft.GetPadPosition.y < -0.40f)
				{
					cambase_transform.position += mainCamera.transform.forward * -moveSpeed * Time.deltaTime;
				}
				if (controllerLeft.GetPadPosition.x < -0.40f)
				{
					cambase_transform.Rotate(0, 2, 0);
				}
				if (controllerLeft.GetPadPosition.x > 0.40f)
				{
					cambase_transform.Rotate(0, -2, 0);
				}
			}
		}

		// Don't move if the controller has the menu or a cluster of images active
		if (controllerRight != null && !controllerRight.filterInterfaceActive && !controllerRight.ImageWallActive)
		{
			if (controllerRight.WhilePressed(Controller.ButtonType.PadPress))
			{
				if (controllerRight.GetPadPosition.y > 0.40f)
				{
					cambase_transform.position += mainCamera.transform.forward * moveSpeed * Time.deltaTime;
				}
				if (controllerRight.GetPadPosition.y < -0.40f)
				{
					cambase_transform.position += mainCamera.transform.forward * -moveSpeed * Time.deltaTime;
				}
				if (controllerRight.GetPadPosition.x < -0.40f)
				{
					cambase_transform.Rotate(0, 2, 0);
				}
				if (controllerRight.GetPadPosition.x > 0.40f)
				{
					cambase_transform.Rotate(0, -2, 0);
				}
			}
		}
	}

	protected override void TeleportModeZoomOut()
	{
		base.TeleportModeZoomOut();

		bool needToZoomOut = false;

		// Don't move if the controller has the menu or a cluster of images active
		if (controllerLeft != null && !controllerLeft.filterInterfaceActive && !controllerLeft.ImageWallActive)
		{
			needToZoomOut = controllerLeft.WhilePressed(Controller.ButtonType.Grip);
		}


		// Don't move if the controller has the menu or a cluster of images active
		if (controllerRight != null && !controllerRight.filterInterfaceActive && !controllerRight.ImageWallActive)
		{
			needToZoomOut |= controllerRight.WhilePressed(Controller.ButtonType.Grip);
		}

		if (needToZoomOut)
		{
			numFramesZoomingOut++;
			float speed = moveSpeed * numFramesZoomingOut / 1000.0f;
			cambase_transform.position += Vector3.up * (speed + extraZoomOutSpeed) * Time.deltaTime;

			if (cambase_transform.position.y >= MAX_ZOOM_OUT_HEIGHT)
			{
				cambase_transform.position = new Vector3(
					cambase_transform.position.x,
					MAX_ZOOM_OUT_HEIGHT,
					cambase_transform.position.z);
			}
		}
		else
		{
			numFramesZoomingOut = 0;
		}
	}
}

