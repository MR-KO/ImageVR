﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
//using system.diagnostics;

using System.Data;
using Mono.Data.SqliteClient;

using m = System.Math;


public class SphereMap : TextureMap
{
	void Start()
	{
		Initialization();
    }
	
    //protected override GameObject CreatePin(Vector3 pos, string path)
    protected override GameObject CreatePin(Image image)
    {
        var pin = base.CreatePin(image);

		//Vector3 pos = ReverseNormals.LatLongToWorld(image.locLat, image.locLong, 20f);
		pin.transform.localRotation = Quaternion.FromToRotation(Vector3.forward, 
			pin.transform.localPosition);
		pin.transform.Translate(new Vector3(0, 0, -0.2f));

        return pin;
	}


	private void Update()
    {
        HandleInput();
    }

    protected override void DirectionModeZoomOut()
    {
        if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
        {
            // radius
            if (controllerLeft.GetPadPosition.y > 0.55f)
            {
                radius += 0.05f;
                if (radius > circleRadius * 2 - 0.5f)
                    radius = circleRadius * 2 - 0.5f;
            }
            if (controllerLeft.GetPadPosition.y < -0.55f)
            {
                radius -= 0.05f;
                if (radius < 0)
                    radius = 0;
            }
        }

        GrabImage(controllerLeft);

        if (controllerRight.WhilePressed(Controller.ButtonType.PadPress))
        {
            // phi
            if (controllerRight.GetPadPosition.y > 0.55f)
            {

                phi += 0.5f;
                if (phi > 89)
                    phi = 89;
                //if (phi > 360)
                //    phi = phi - 360;
            }
            if (controllerRight.GetPadPosition.y < -0.55f)
            {
                phi -= 0.5f;
                if (phi < -89f)
                    //if (phi < 0)
                    phi = -89f;
                //phi = phi + 360;
            }

            // rho
            if (controllerRight.GetPadPosition.x > 0.55f)
            {
                rho += 0.5f;
                if (rho > 360)
                    rho = 360;
                //rho = rho - 360;
            }
            if (controllerRight.GetPadPosition.x < -0.55f)
            {
                rho -= 0.5f;
                if (rho < 0)
                    rho = 0;
                //rho = rho + 360;
            }
        }

        GrabImage(controllerRight);

        CalculatePosition();
    }

    private void CalculatePosition()
    {
        // calculate position
        cambase_transform.position = ReverseNormals.LatLongToWorld(phi, rho, radius);
        cambase_transform.LookAt(ReverseNormals.LatLongToWorld(phi, rho, 40), Vector3.up);
    }

    protected override void TeleportModeZoomOut()
    {
        base.TeleportModeZoomOut();

        Vector3 moveDirection = cambase_transform.position.normalized;

        if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (cambase_transform.position.magnitude > 1)
            {
                cambase_transform.position -= moveDirection * moveSpeed * Time.deltaTime;
            }
            else
            {
                cambase_transform.position = Vector3.zero;
            }
            //radius += moveSpeed * Time.deltaTime;
        }

        if (controllerRight.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (cambase_transform.position.magnitude > 1)
            {
                cambase_transform.position -= moveDirection * moveSpeed * Time.deltaTime;
            }
            else
            {
                cambase_transform.position = Vector3.zero;
            }
            //radius += moveSpeed * Time.deltaTime;
        }

        //CalculatePosition();
    }

    protected override void FlyModeZoomOut()
    {
        // spherical inside rotation
        if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (controllerLeft.GetPadPosition.y > 0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(mainCamera.transform.forward);
            }
            if (controllerLeft.GetPadPosition.y < -0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(-mainCamera.transform.forward);
            }
            if (controllerLeft.GetPadPosition.x < -0.40f)
            {
                cambase_transform.Rotate(0, 2, 0);
            }
            if (controllerLeft.GetPadPosition.x > 0.40f)
            {
                cambase_transform.Rotate(0, -2, 0);
            }
        }

        GrabImage(controllerLeft);

        if (controllerRight.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (controllerRight.GetPadPosition.y > 0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(mainCamera.transform.forward);
            }
            if (controllerRight.GetPadPosition.y < -0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(-mainCamera.transform.forward);
            }
            if (controllerRight.GetPadPosition.x < -0.40f)
            {
                cambase_transform.Rotate(0, 2, 0);
            }
            if (controllerRight.GetPadPosition.x > 0.40f)
            {
                cambase_transform.Rotate(0, -2, 0);
            }
        }

        GrabImage(controllerRight);
    }
}


