﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;

using m = System.Math;

public class spaceMap : TextureMap
{
    int sortingorder = 0;

    void Start()
    {
        Initialization();
        radius = 50f;
        
    }

    //protected override GameObject CreatePin(Vector3 pos, string url)
    //{
    //    var pin = base.CreatePin(pos, url);
    //    Pin p = pin.GetComponent<Pin>();

    //    pin.transform.localRotation = Quaternion.FromToRotation(-Vector3.forward, pos);
    //    Vector3 euler = pin.transform.localRotation.eulerAngles;
    //    pin.transform.localRotation = Quaternion.Euler(euler.x, euler.y, 0);
    //    pin.transform.Translate(new Vector3(0, 0, -0.001f));
    //    pin.GetComponent<Renderer>().sortingOrder = sortingorder;
    //    sortingorder++;

    //    return pin;
    //}



    private void Update()
    {
        HandleInput();
    }

    protected override void DirectionModeZoomOut()
    {
        // spherical outside rotation
        if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (controllerLeft.GetPadPosition.y < -0.55f)
            {
                radius += 0.05f;
                if (radius > 2 * circleRadius)
                    radius = 2 * circleRadius;
            }
            if (controllerLeft.GetPadPosition.y > 0.55f)
            {
                radius -= 0.05f;
                if (radius < circleRadius + 0.5f)
                    radius = circleRadius + 0.5f;
            }
        }

        GrabImage(controllerLeft);

        if (controllerRight.WhilePressed(Controller.ButtonType.PadPress))
        {
            // phi
            if (controllerRight.GetPadPosition.y > 0.55f)
            {
                phi += 0.5f;
                if (phi > 360)
                    phi = phi - 360;
            }
            if (controllerRight.GetPadPosition.y < -0.55f)
            {
                phi -= 0.5f;
                if (phi < 0)
                    phi = phi + 360;
            }

            // rho
            if (controllerRight.GetPadPosition.x > 0.55f)
            {
                rho += 0.5f;
                if (rho > 360)
                    rho = rho - 360;
            }
            if (controllerRight.GetPadPosition.x < -0.55f)
            {
                rho -= 0.5f;
                if (rho < 0)
                    rho = rho + 360;
            }
        }

        GrabImage(controllerRight);

        CalculatePosition();
    }

    private void CalculatePosition()
    {
        // Calculate Position
        cambase_transform.position = ReverseNormals.LatLongToWorld(phi, rho, radius);
        cambase_transform.LookAt(Vector3.zero);
    }

    protected override void FlyModeZoomOut()
    {
        // spherical inside rotation
        if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (controllerLeft.GetPadPosition.y > 0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(mainCamera.transform.forward);
            }
            if (controllerLeft.GetPadPosition.y < -0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(-mainCamera.transform.forward);
            }
            if (controllerLeft.GetPadPosition.x < -0.40f)
            {
                cambase_transform.Rotate(0, 2, 0);
            }
            if (controllerLeft.GetPadPosition.x > 0.40f)
            {
                cambase_transform.Rotate(0, -2, 0);
            }
        }

        GrabImage(controllerLeft);

        if (controllerRight.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (controllerRight.GetPadPosition.y > 0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(mainCamera.transform.forward);
            }
            if (controllerRight.GetPadPosition.y < -0.55f)
            {
                cambase_transform.GetComponent<Rigidbody>().AddRelativeForce(-mainCamera.transform.forward);
            }
            if (controllerRight.GetPadPosition.x < -0.40f)
            {
                cambase_transform.Rotate(0, 2, 0);
            }
            if (controllerRight.GetPadPosition.x > 0.40f)
            {
                cambase_transform.Rotate(0, -2, 0);
            }
        }

        GrabImage(controllerRight);
    }

    protected override void TeleportModeZoomOut()
    {
        base.TeleportModeZoomOut();

        Vector3 moveDirection = cambase_transform.position.normalized;

        if (controllerLeft.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (cambase_transform.position.magnitude < 60)
            {
                cambase_transform.position += moveDirection * moveSpeed * Time.deltaTime;
            }
            //radius += moveSpeed * Time.deltaTime;
            //CalculatePosition();
        }

        if (controllerRight.WhilePressed(Controller.ButtonType.PadPress))
        {
            if (cambase_transform.position.magnitude < 60)
            {
                cambase_transform.position += moveDirection * moveSpeed * Time.deltaTime;
            }
            //radius += moveSpeed * Time.deltaTime;
            //CalculatePosition();
        }

        
    }

    //private void Old()
    //{ 
    //    Transform cambase = mainCamera.transform.parent.transform;

    //    // Vive1
    //    SteamVR_TrackedController svrtc1 = vive1.GetComponent<SteamVR_TrackedController>();
    //    if (svrtc1.padPressed)
    //    {
    //        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)vive1.GetComponent<SteamVR_TrackedObject>().index);
    //        Vector2 pos = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0);

    //        // radius
    //        if (pos.y < -0.55f)
    //        {
    //            radius += 0.05f;
    //            if (radius > 2 * circleRadius)
    //                radius = 2 * circleRadius;
    //        }
    //        if (pos.y > 0.55f)
    //        {
    //            radius -= 0.05f;
    //            if (radius < circleRadius + 0.5f)
    //                radius = circleRadius + 0.5f;
    //        }

    //    }

    //    if (svrtc1.triggerPressed && !previousLeftTrigger)
    //    {
    //        if (leftImage)
    //            Destroy(leftImage);

    //        if (svrtc1.GetComponent<SteamVR_LaserPointer>().previousContact != null)
    //        {
    //            GameObject obj = svrtc1.GetComponent<SteamVR_LaserPointer>().previousContact.gameObject;
    //            if (obj.GetComponent<ImageScript>() != null)
    //            {
    //                leftImage = Instantiate(imagePrefab);
    //                leftImage.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
    //                leftImage.transform.position = svrtc1.transform.position;


    //                leftImage.transform.parent = vive1.gameObject.transform;
    //                leftImage.transform.localRotation = Quaternion.Euler(0, 0, 0);
    //                //= leftImage.transform.parent.rotation;
    //                leftImage.transform.Translate(new Vector3(0, 0.2f, 0));
    //                leftImage.GetComponent<Renderer>().material.mainTexture = obj.GetComponent<Renderer>().material.mainTexture;
    //                leftImage.layer = 9;
    //            }
    //        }


    //    }

    //    previousLeftTrigger = svrtc1.triggerPressed;

    //    SteamVR_TrackedController svrtc2 = vive2.GetComponent<SteamVR_TrackedController>();
    //    // Vive2
    //    if (svrtc2.padPressed)
    //    {
    //        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)vive2.GetComponent<SteamVR_TrackedObject>().index);
    //        Vector2 pos = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0);

    //        // phi
    //        if (pos.y > 0.55f)
    //        {
    //            phi += 0.5f;
    //            if (phi > 360)
    //                phi = phi - 360;
    //        }
    //        if (pos.y < -0.55f)
    //        {
    //            phi -= 0.5f;
    //            if (phi < 0)
    //                phi = phi + 360;
    //        }

    //        // rho
    //        if (pos.x > 0.55f)
    //        {
    //            rho += 0.5f;
    //            if (rho > 360)
    //                rho = rho - 360;
    //        }
    //        if (pos.x < -0.55f)
    //        {
    //            rho -= 0.5f;
    //            if (rho < 0)
    //                rho = rho + 360;
    //        }


    //    }

    //    if (svrtc2.triggerPressed && !previousRightTrigger)
    //    {
    //        if (rightImage)
    //            Destroy(rightImage);

    //        if (svrtc2.GetComponent<SteamVR_LaserPointer>().previousContact != null)
    //        {


    //            GameObject obj = svrtc2.GetComponent<SteamVR_LaserPointer>().previousContact.gameObject;
    //            if (obj.GetComponent<ImageScript>() != null)
    //            {
    //                rightImage = Instantiate(imagePrefab);
    //                rightImage.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
    //                rightImage.transform.position = svrtc2.transform.position;

    //                rightImage.transform.parent = vive2.transform;
    //                rightImage.transform.localRotation = Quaternion.Euler(0, 0, 0);//= rightImage.transform.parent.rotation;
    //                rightImage.transform.Translate(new Vector3(0, 0.2f, 0));
    //                rightImage.GetComponent<Renderer>().material.mainTexture = obj.GetComponent<Renderer>().material.mainTexture;
    //                rightImage.layer = 9;
    //            }
    //        }

    //    }
    //    previousRightTrigger = svrtc2.triggerPressed;


    //    cambase.position = ReverseNormals.LatLongToWorld(phi, rho, radius);
    //    cambase.LookAt(Vector3.zero);
    //    //cambase.rotation = Quaternion.LookRotation(Vector3.zero);
    //}
}