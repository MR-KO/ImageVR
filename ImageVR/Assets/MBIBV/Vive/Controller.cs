﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;



public class Controller
{
    public enum ButtonType { Trigger, Steam, Menu, PadPress, PadTouch, Grip };
	public enum TouchPadEdge { Left, Right, Top, Bottom, None };

    public GameObject vive;

	public bool filterInterfaceActive = false;
	public IGlow highlightedObject;

	bool imageWallActive;
	public ImageWallNav imageWall;

	// Where the user has last pressed the touchpad...
	public float touchPadX = 0.0f;
	public float touchPadY = 0.0f;

	SteamVR_TrackedController buttonScheme;

    public ButtonLayout currentLayout;
    public ButtonLayout previousLayout;

	// This is used to keep track of long presses of these buttons,
	// so we can do fast scroll if you hold down these buttons
	public int numFramesHeldLeftPad = 0;
	public int numFramesHeldRightPad = 0;
	public int numFramesHeldTopPad = 0;
	public int numFramesHeldBottomPad = 0;

	private static float COORDINATE_FOR_TOUCHPAD_EDGE = 0.55f; // Considered as edge
	private static int NUM_FRAMES_TO_HELD_FOR_FAST_SCROLL = 90; // TODO: Play around with this value...

	public Coroutine coroutineLoadingImageWall = null;

	SteamVR_Controller.Device device;

    public Controller(GameObject vive)
    {
		this.vive = vive;
        buttonScheme = vive.GetComponent<SteamVR_TrackedController>();
        currentLayout = new ButtonLayout();
		
		//if (index == -1)
		//    return;

		//device = SteamVR_Controller.Input(index);

		//device = SteamVR_Controller.Input

		//device = SteamVR_Controller.Input((int)vive.GetComponent<SteamVR_TrackedObject>().index);

		imageWallActive = false;

		// Set delegate for touch pad handling... Not needed anymore!
		//this.buttonScheme.PadTouched += this.HandlePadTouched;
	}

	// Don't forget to do the following after this function call!!!
	// StartCoroutine(controller.imageWall.FinalizeSetup(imagePrefab,
	//	textPrefab, controller.vive.transform));
	public void CreateImageWallNav(GameObject imagePrefab, GameObject textPrefab, 
									IGlow newHighlightedObject, ClusterBase cluster, 
									bool useCurvedImageWall = false)
	{
		// TODO: Change ClusterLocation into ClusterBase to allow for 
		// visualizing different levels of clusters in the wall nav?
		ClusterLocation locCluster = cluster as ClusterLocation;

		if (locCluster == null)
		{
			Debug.Log("Got null cluster!");
			imageWall = null;
			ImageWallActive = false;
		}
		else
		{
			// Cleanup previous imageWall?
			if (imageWall != null)
			{
				this.highlightedObject.UnHighlight();
				imageWall.Destroy();
			}

			imageWall = new ImageWallNav(imagePrefab, textPrefab, locCluster, useCurvedImageWall);
			this.highlightedObject = newHighlightedObject;
			this.highlightedObject.Highlight();
			ImageWallActive = true;
		}
	}

	public void AssignFilteringMenu(GameObject filterObject)
	{
		filterInterfaceActive = true;
		filterObject.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
		filterObject.transform.position = ImagePosition();
		filterObject.transform.parent = vive.gameObject.transform;
		filterObject.transform.localRotation = Quaternion.Euler(0, 0, 0);
		filterObject.transform.Translate(new Vector3(0.08f, 0.2f, 0.05f));
	}

	public void DestroyHoldingImageWall()
	{
		ImageWallActive = false;
		coroutineLoadingImageWall = null;

		if (imageWall != null)
		{
			imageWall.Destroy();
		}

		if (highlightedObject != null)
		{
			highlightedObject.UnHighlight();
		}

		imageWall = null;
		highlightedObject = null;
	}

	public void UpdateButtons()
    {
        previousLayout = currentLayout;
        currentLayout = new ButtonLayout(new bool[] {
            buttonScheme.triggerPressed,
            buttonScheme.steamPressed,
            buttonScheme.menuPressed,
            buttonScheme.padPressed,
            buttonScheme.padTouched,
            buttonScheme.gripped });

		// Update touchpad positions
		touchPadX = buttonScheme.controllerState.rAxis0.x;
		touchPadY = buttonScheme.controllerState.rAxis0.y;

		// Update held button presses
		if (currentLayout.buttons[(int)ButtonType.PadPress].Pressed)
		{
			numFramesHeldRightPad = (touchPadX > 0.55) ? numFramesHeldRightPad + 1 : 0;
			numFramesHeldLeftPad = (touchPadX < -0.55) ? numFramesHeldLeftPad + 1 : 0;
			numFramesHeldTopPad = (touchPadY > 0.55) ? numFramesHeldTopPad + 1 : 0;
			numFramesHeldBottomPad = (touchPadY < -0.55) ? numFramesHeldBottomPad + 1 : 0;
		}
		else
		{
			ResetFastScrolling();
		}

		// Not part of the buttons, but highlight the currently selected thing...
		if (highlightedObject != null)
		{
			highlightedObject.Highlight();
		}
	}

	// Returns true if the touchpad is touched on the given edge
	public bool OnEdge(TouchPadEdge edgeType)
	{
		switch (edgeType)
		{
			case TouchPadEdge.Left:
				return touchPadX < -COORDINATE_FOR_TOUCHPAD_EDGE;

			case TouchPadEdge.Right:
				return touchPadX > COORDINATE_FOR_TOUCHPAD_EDGE;

			case TouchPadEdge.Top:
				return touchPadY > COORDINATE_FOR_TOUCHPAD_EDGE;

			case TouchPadEdge.Bottom:
				return touchPadY < -COORDINATE_FOR_TOUCHPAD_EDGE;

			case TouchPadEdge.None:
			default:
				return false;
		}
	}

	// Returns true if the touchpad is held down long enough on the given edge
	public bool HeldDownLongEnough(TouchPadEdge edgeType)
	{
		switch (edgeType)
		{
			case TouchPadEdge.Left:
				return numFramesHeldLeftPad >= NUM_FRAMES_TO_HELD_FOR_FAST_SCROLL;

			case TouchPadEdge.Right:
				return numFramesHeldRightPad >= NUM_FRAMES_TO_HELD_FOR_FAST_SCROLL;

			case TouchPadEdge.Top:
				return numFramesHeldTopPad >= NUM_FRAMES_TO_HELD_FOR_FAST_SCROLL;

			case TouchPadEdge.Bottom:
				return numFramesHeldBottomPad >= NUM_FRAMES_TO_HELD_FOR_FAST_SCROLL;

			case TouchPadEdge.None:
			default:
				return false;
		}
	}

	public void ResetFastScrolling()
	{
		numFramesHeldLeftPad = 0;
		numFramesHeldRightPad = 0;
		numFramesHeldTopPad = 0;
		numFramesHeldBottomPad = 0;
	}

	public bool OnPress(ButtonType button)
    {
        return currentLayout.buttons[(int)button].Pressed && !previousLayout.buttons[(int)button].Pressed;
    }

    public bool OnRelease(ButtonType button)
    {
        return !currentLayout.buttons[(int)button].Pressed && previousLayout.buttons[(int)button].Pressed;
    }

    public bool WhilePressed(ButtonType button)
    {
        return currentLayout.buttons[(int)button].Pressed;
    }

	// Function doesn't work!!!
    public Vector2 GetPadPosition
    {
        get
        {
            if (device == null)
            {
                var a = vive.GetComponent<SteamVR_TrackedObject>();
                int index = (int)a.index;

				Debug.Log("Device is null, using index: " + index);
				device = SteamVR_Controller.Input(index);
			}

            Vector2 result = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0);
			Debug.Log("Got pad position: " + result);
			//sw.WriteLine("Got pad position: " + result);
			return result;
        }
    }

    public bool ImageWallActive
    {
        get
        {
            return imageWallActive;
        }
        set
        {
            imageWallActive = value;
        }
    }

    public Vector3 ImagePosition()
    {
        return vive.transform.position;
    }

    public GameObject LaserObject
    {
        get
        {
            var prevContact = vive.GetComponent<SteamVR_LaserPointer>().previousContact;
            if (prevContact == null)
                return null;
            else
                return prevContact.gameObject;
        }
    }

    public override string ToString()
    {
        return currentLayout.ToString();
    }
}

public struct ButtonLayout
{
    // Trigger = 0, Steam = 1; Menu = 2; PadPress = 3; PadTouch = 4; Grip = 5;
    public ViveButton[] buttons;

    public ButtonLayout(bool[] buttonstate)
    {
        buttons = new ViveButton[6];
        if (buttonstate == null)
        {
            for (int i = 0; i < 6; i++)
            {
                buttons[i] = new ViveButton();
            }
        }
        else
        {
            for (int i = 0; i < 6; i++)
            {
                buttons[i] = new ViveButton(buttonstate[i]);
            }
        }

    }

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder(6);
        foreach (ViveButton b in buttons)
        {
            if (b.Pressed)
                sb.Append("1");
            else
                sb.Append("0");
        }
        return sb.ToString();
    }
}

public struct ViveButton
{
    bool pressed;

    public ViveButton(bool state = false)
    {
        pressed = state;
    }

    public bool Pressed
    {
        get
        {
            return pressed;
        }
    }
}
