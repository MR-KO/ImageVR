using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.Data;
using Mono.Data.SqliteClient;
using System;

public class DB
{
	// These are supposed to be constants!!!
	// TODO: Read from config file...
	private static readonly string dbFile = Application.dataPath + "/" + "imagedb.db";

	// Table names
	private static readonly string tableImages = "Images";
	private static readonly string tableTags = "Tags";
	private static readonly string tableImageTags = "Images_Tags";

	// These are not constant
	private static IDbConnection dbConnection;
	private static bool connected = false;

	private static void Connect()
	{
		DB.dbConnection = new SqliteConnection("URI=file:" + DB.dbFile);
		DB.dbConnection.Open();
		DB.connected = true;
	}

	public static void CheckInstance()
	{
		if (!DB.connected) {
			DB.Connect();
		}
	}

	public static void Execute(string query)
	{
		DB.CheckInstance();

		// Create command to execute query with args
		IDbCommand cmd = DB.dbConnection.CreateCommand();
		cmd.CommandText = query;
		cmd.ExecuteNonQuery();
		cmd.Dispose();
	}

	public static IDataReader Select(string query)
	{
		DB.CheckInstance();

		// Create command to execute query with args
		IDbCommand cmd = DB.dbConnection.CreateCommand();
		cmd.CommandText = query;
		IDataReader reader = cmd.ExecuteReader();
		cmd.Dispose();
		return reader;
	}

	public static void CloseReader(IDataReader reader)
	{
		reader.Close();
		reader = null;
	}

	// Returns all images in the database (image_id, image_lsc_id, path,
	// user_id, loc_lat, loc_long, loc_name, date_time)
	public static IDataReader GetAllImages()
	{
		return DB.Select("SELECT * FROM " + DB.tableImages + ";");
	}

	// Returns all tags in the database (tag_id and tag_name)
	public static IDataReader GetAllTags()
	{
		return DB.Select("SELECT * FROM " + DB.tableTags + ";");
	}

	// Returns all tags in the database (tag_id and tag_name) as a list of tuples
	public static List<Tag> GetTags()
	{
		List<Tag> tags = new List<Tag>(633); // From DB.dbFile
		IDataReader reader = DB.Select("SELECT * FROM " + DB.tableTags + ";");

		while (reader.Read())
		{
			tags.Add(new Tag(reader.GetInt64(0), reader.GetString(1), reader.GetInt64(2)));
		}

		DB.CloseReader(reader);
		return tags;
	}

	// Returns all matched tags in the database (image_id, tag_id, confidence)
	public static IDataReader GetAllImageTags()
	{
		return DB.Select("SELECT * FROM " + DB.tableImageTags + ";");
	}

	// Returns a list of all tags (tag_ids) for each image_id in the database
	public static List<Tuple<long, List<long>>> GetAllImageTagsAsList()
	{
		IDataReader reader = GetAllImageTags();
		int numImagesWithTagsInDB = 34146;	// From DB.dbFile
		List<Tuple<long, List<long>>> tagList = new List<Tuple<long, List<long>>>(numImagesWithTagsInDB);

		// Use a dictionairy... Its roughly 100x faster than using List directly!!!!!!
		Dictionary<long, List<long>> tagListDict = new Dictionary<long, List<long>>(numImagesWithTagsInDB);

		//DateTime startTime = DateTime.Now;
		//DateTime tempTime;

		//double findSeconds = 0.0;
		//double addSeconds = 0.0;

		while (reader.Read())
		{
			long image_id = reader.GetInt64(0);
			long tag_id = reader.GetInt64(1);
			//Debug.Log("Reading, got image_id " + image_id + " and tag_id " + tag_id);

			// Check if this image_id already has something in the taglist
			//tempTime = DateTime.Now;

			// This is roughly 2x faster than using my own for loop...
			List<long> tempList = null;
			bool found = tagListDict.TryGetValue(image_id, out tempList);

			//findSeconds += (DateTime.Now - tempTime).TotalSeconds;
			//tempTime = DateTime.Now;

			if (!found)
			{
				tempList = new List<long>(1);
				tagListDict.Add(image_id, tempList);
			}

			tempList.Add(tag_id);
			//addSeconds += (DateTime.Now - tempTime).TotalSeconds;
		}

		// Convert dictionairy to list (fill the tagList)
		foreach (KeyValuePair<long, List<long>> element in tagListDict)
		{
			tagList.Add(new Tuple<long, List<long>>(element.Key, element.Value));
		}

		//Debug.Log("Loop took: " + (DateTime.Now - startTime).TotalSeconds + " seconds!");
		//Debug.Log("\tFinding took: " + findSeconds + " seconds");
		//Debug.Log("\tAdding took: " + addSeconds + " seconds");

		DB.CloseReader(reader);
		//Debug.Log("TagList length: " + tagList.Count);
		//Debug.Log("TagList[0].image_id: " + tagList[0].First);
		//Debug.Log("TagList[0].tag_ids[0]: " + tagList[0].Second[0]);
		//Debug.Log("TagList[0].tag_ids[1]: " + tagList[0].Second[1]);
		return tagList;
	}

	// Returns a list of all tags (tag_ids) for a given image_id
	public static List<long> GetImageTags(long image_id)
	{
		List<long> tags = new List<long>();

		IDataReader reader = DB.Select("SELECT tag_id FROM " + DB.tableImageTags
			+ " WHERE image_id=" + image_id + ";");

		while (reader.Read())
		{
			tags.Add(reader.GetInt64(0));
		}

		DB.CloseReader(reader);
		return tags;
	}
}
