﻿using System;
using System.Collections.Generic;

using UnityEngine;

public class Tag : IEquatable<Tag>, IComparable<Tag>
{
	public long id { get; set; }
	public string name { get; set; }
	public long occurrences { get; set; }
	public bool enabled { get; set; }

	public Tag(long tag_id, string tag_name, long db_occurrences)
	{
		id = tag_id;
		name = tag_name;
		occurrences = db_occurrences;
		enabled = true;
	}

	public Tag(Tag otherTag) {
		id = otherTag.id;
		name = otherTag.name;
		occurrences = otherTag.occurrences;
		enabled = otherTag.enabled;
	}

	public static List<Tag> GetTags(List<Tag> tagList, List<long> tags)
	{
		if (tagList == null || tags == null || tags.Count == 0 || tagList.Count == 0)
		{
			return null;
		}

		List<Tag> foundTags = new List<Tag>();

		// Check everyone of the tags in tags if it's present in tagList
		foreach (long tag_id in tags)
		{
			// TODO: Rewrite this to take advantage of the dangerous assumption that
			// the Taglist is always ordered... For performance of course...
			Tag tag = tagList.Find(x => x.id == tag_id);

			if (tag != null)
			{
				foundTags.Add(tag);
			}
		}

		return foundTags;
	}

	// We always compare by tag name, so we can sort by name...
	public int CompareTo(Tag otherTag)
	{
		return (otherTag == null) ? 1 : name.CompareTo(otherTag.name);
	}

	public override bool Equals(object obj)
	{
		if (obj == null)
		{
			return false;
		}

		Tag objAsTag = obj as Tag;
		return (objAsTag == null) ? false : Equals(objAsTag);
	}

	public bool Equals(Tag otherTag)
	{
		return (otherTag == null) ? false : id == otherTag.id;
	}

	public static bool operator ==(Tag tag1, Tag tag2)
	{
		if (System.Object.ReferenceEquals(tag1, tag2))
		{
			return true;
		}

		if ((object) tag1 == null || (object) tag2 == null)
		{
			return false;
		}

		return tag1.id == tag2.id;
	}

	//public static bool operator ==(Tag tag, long tag_id)
	//{
	//	return (tag == null) ? false : tag.Equals(tag_id);
	//}

	public static bool operator !=(Tag tag1, Tag tag2)
	{
		return !(tag1 == tag2);
	}

	//public static bool operator !=(Tag tag, long tag_id)
	//{
	//	return !(tag == tag_id);
	//}

	public override int GetHashCode()
	{
		return (int)id;	// No overflow will happen, probably...
	}

	public override string ToString()
	{
		return name + " (" + occurrences + ")";
	}

	static public string ListOfTagsToString(List<Tag> tags, bool splitLine = true)
	{
		string text = "";

		if (tags != null && tags.Count > 0)
		{
			bool first = true;

			for (int i = 0; i < tags.Count; i++)
			{
				if (first)
				{
					text += tags[i].name;
					first = false;
				}
				else
				{
					text += ", " + tags[i].name;
				}

				if (splitLine && i > 0 && ((i + 1) % 5 == 0) && i < tags.Count - 1)
				{
					text += ",\n\t\t\t";
					first = true;
				}
			}
		}

		return text;
	}
}
