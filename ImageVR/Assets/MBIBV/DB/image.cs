using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.Data;
using Mono.Data.SqliteClient;


public class Image
{
	// Class constants
	public const string DB_TABLE_NAME_IMAGES = "Images";
	public const string DB_KEY_IMAGE_ID = "image_id";
	public const string DB_KEY_IMAGE_LSC_ID = "image_lsc_id";
	public const string DB_KEY_IMAGE_PATH = "path";
	public const string DB_KEY_IMAGE_USER_ID = "user_id";
	public const string DB_KEY_IMAGE_LOC_LAT = "loc_lat";
	public const string DB_KEY_IMAGE_LOC_LONG = "loc_long";
	public const string DB_KEY_IMAGE_LOC_NAME = "loc_name";
	public const string DB_KEY_IMAGE_DATE_TIME = "date_time";

	// Object members
	public long id { get; set; }
	public string lscId { get; set; }
	public string path { get; set; }
	public string userId { get; set; }
	public double locLat { get; set; }
	public double locLong { get; set; }
	public string locName { get; set; }
	public string dateTime { get; set; }
	public List<long> tags { get; set; }

	// To determine if the image should be visible or not (aka, filtered or not...)
	public bool visible { get; set; }

	// Assumes the reader has already had its .Read() method called...
	public Image(IDataReader reader)
	{
		id = reader.GetInt64(0);
		lscId = reader.GetString(1);
		path = reader.GetString(2);
		userId = reader.GetString(3);
		locLat = reader.GetDouble(4);
		locLong = reader.GetDouble(5);
		locName = reader.GetString(6);
		dateTime = reader.GetString(7);

		tags = null;
		visible = true;
	}

	// Returns a List of Images from a given database reader
	public static List<Image> GetImagesFromReader(IDataReader reader)
	{
		List<Image> results = new List<Image>(56450); // From DB.dbFile

		while (reader.Read())
		{
			results.Add(new Image(reader));
		}

		return results;
	}

	public static List<Image> GetActiveImages(List<Image> inputImages)
	{
		List<Image> result = inputImages.FindAll(x => x.visible == true);
		return (result.Count == 0) ? null : result;
	}

	// Sets the List of tags (long tag_id) for a list of images
	public static void SetImageTags(List<Image> images, List<Tuple<long, List<long>>> tagList)
	{
		// TODO: Sort the tagList by image_id (first elem in the Tuple)?
		// We don't do that, because we can operate on the dangerous assumption
		// that the tagList is read sequentially, in order, from the database,
		// and also populated sequentially, in order, into the database...
		// Which is now also FORCIBLY ENSURED by the create_db.py parser,
		// because the idiots at the LSC can't create CONSISTENT,
		// CORRECT, USEFUL, ORGANIZED data and metadata...

		// Loop through the image tags, assigning them to their images...
		int images_index = 0;
		Image image = null;

		for (int tag_index = 0; tag_index < tagList.Count; tag_index++)
		{
			// Get image_id for this tagList item
			long image_id = tagList[tag_index].First;

			// Find the corresponding image in the images list
			image = null;
			int images_index_backup = images_index;

			do
			{
				image = images[images_index++];
			} while (image.id != image_id);

			// Check if we have actually found it...
			if (image == null)
			{
				Debug.Log("Well fuck...");
				images_index = images_index_backup;
			}
			else
			{
				image.tags = tagList[tag_index].Second; // Nice
			}
		}
	}

	// Sets the current visibility to True if either this.tags is null, or if one of
	// the tags is present and active in activeTags, otherwise False
	public void ToggleVisibility(List<Tag> tagList, bool visibleIfNoTags = true)
	{
		// Images that can't be filtered are always visible
		if (tags == null)
		{
			visible = visibleIfNoTags;
			return;
		}

		// Check if one of the tags in this.tags is present in activeTags
		foreach (long tag_id in tags)
		{
			// TODO: Rewrite this to take advantage of the dangerous assumption that
			// the Taglist is always ordered... For performance of course...
			Tag tag = tagList.Find(x => x.id == tag_id);

			if (tag != null && tag.enabled)
			{
				visible = true;
				return;
			}
		}

		// Alternative version....
		//List<Tag> enabledTags = tagList.FindAll(x => x.enabled && this.tags.Contains(x.id));
		//this.visible = enabledTags.Count > 0;

		visible = false;
	}

	public override string ToString()
	{
		return "Image ("+ id + ", " + lscId + "), from user " + userId + " at " + path;
	}
}
