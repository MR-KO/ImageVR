﻿using System;
using UnityEngine;
using Valve.VR;

//public enum TeleportState { None, Teleporting, Selecting };

[AddComponentMenu("Vive Teleporter/Vive Teleporter")]
[RequireComponent(typeof(Camera))]
public class ViveTeleportationScript : MonoBehaviour
{
    [Tooltip("Parabolic Pointer object to pull destination points from, and to assign to each controller.")]
    public ViveTeleportationPointer Pointer;
    /// Origin of SteamVR tracking space
    [Tooltip("Origin of the SteamVR tracking space")]
    public Transform OriginTransform;
    /// Origin of the player's head
    [Tooltip("Transform of the player's head")]
    public Transform HeadTransform;

    /// How long, in seconds, the fade-in/fade-out animation should take
    [Tooltip("Duration of the \"blink\" animation in seconds.")]
    public float TeleportDuration = 1.0f;
    /// Measure in degrees of how often the controller should respond with a haptic click.  Smaller value=faster clicks
    [Tooltip("The player feels a haptic pulse in the controller when they raise / lower the controller by this many degrees.  Lower value = faster pulses.")]
    public float HapticClickAngleStep = 10;

	/// SteamVR controllers that should be polled.
	[Tooltip("Array of SteamVR controllers that may used to select a teleport destination.")]
	public GameObject controllerLeft;
	public GameObject controllerRight;
    private SteamVR_TrackedObject ActiveController;

    /// Indicates the current use of teleportation.
    /// None: The player is not using teleportation right now
    /// Selecting: The player is currently selecting a teleport destination (holding down on touchpad)
    /// Teleporting: The player has selected a teleport destination and is currently teleporting now (fading in/out)
    [SerializeField]
    public TeleportState CurrentTeleportState;// { get; private set; }
	public bool JustStartedTeleporting;
	public bool JustFinishedTeleporting;
	public double teleportDistance;

	private Vector3 LastClickAngle = Vector3.zero;
    private bool IsClicking = false;

    private float TeleportTimeMarker = -1;


    private Vector3[] TravelPositions;

    [Tooltip("Number of steps between original position and target position")]
    public int steps = 60;

    void Start()
    {
        // Disable the pointer graphic (until the user holds down on the touchpad)
        Pointer.enabled = false;

        // Ensure we mark the player as not teleporting
        CurrentTeleportState = TeleportState.None;
		JustStartedTeleporting = false;
		JustFinishedTeleporting = false;
		teleportDistance = 0.0;

		Pointer.OriginTransform = OriginTransform;
	}


    /// <summary>
    /// Handles the states
    /// </summary>
    void Update()
    {
		JustStartedTeleporting = false;
		JustFinishedTeleporting = false;

		switch (CurrentTeleportState)
        {
            case TeleportState.Teleporting:
                // if we have no trajectory for teleporting yet
                if (TravelPositions == null)
                {
					JustStartedTeleporting = true;
                    CalculatePositions();
                }

                Teleport();
                break;

            case TeleportState.Selecting:
                Selecting();
                break;

            case TeleportState.None:
                AwaitInput();
                break;
        }
    }

    // calculates teleport position from origin to target
    private void CalculatePositions()
    {
		// Calculate how high our head is from our feet...
        Vector3 offset = OriginTransform.position - HeadTransform.position;
		//offset.y = 0;
	
		Vector3 targetPosition = Pointer.SelectedPoint + offset + Pointer.SurfaceNormal;

		// Calculate distance to target and base the number of 
		// steps and the teleport duration on that...
		Vector3 move = targetPosition - HeadTransform.position;
		teleportDistance = move.sqrMagnitude;
		steps = (int) Math.Ceiling(move.sqrMagnitude / 5.0) + 60;
		TravelPositions = new Vector3[steps];
		TeleportDuration = Math.Max(steps / 250.0f, 1.0f);

		//Debug.Log("Got distance: " + move.sqrMagnitude + 
		//	", steps: " + steps + " and duration: " + TeleportDuration);

		for (int i = 0; i < steps; i++)
        {
            TravelPositions[i] = Vector3.Lerp(OriginTransform.position, targetPosition, i * (1f / steps));
        }
    }

    private void AwaitInput()
    {
		// At this point the user is not holding down on the touchpad at all or has canceled a teleport and hasn't
		// let go of the touchpad.  So we wait for the user to press the touchpad and enable visual indicators if necessary.
		GameObject[] objects = { controllerLeft, controllerRight };

		foreach (GameObject gameObject in objects)
        {
			// Skip this controller if it should not move
			MovementActive active = gameObject.GetComponent<MovementActive>();

			if (!active.movementActive)
			{
				continue;
			}

			// Check pad press
			var obj = gameObject.GetComponent<SteamVR_TrackedObject>();
            int index = (int)obj.index;

			if (index == -1)
			{
				continue;
			}

            var device = SteamVR_Controller.Input(index);
            if (device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
            {
                // Set active controller to this controller, and enable the parabolic pointer and visual indicators
                // that the user can use to determine where they are able to teleport.
                ActiveController = obj;

                Pointer.transform.parent = obj.transform;
                Pointer.transform.localPosition = Vector3.zero;
                Pointer.transform.localRotation = Quaternion.identity;				
                Pointer.transform.localScale = Vector3.one;
                Pointer.enabled = true;

                CurrentTeleportState = TeleportState.Selecting;

                Pointer.ForceUpdateCurrentAngle();
                LastClickAngle = Pointer.CurrentPointVector;
                IsClicking = Pointer.PointerHitsSurface;
            }
        }
    }

    private void Selecting()
    {
        Debug.Assert(ActiveController != null);

        // Here, there is an active controller - that is, the user is holding down on the trackpad.
        // Poll controller for pertinent button data
        int index = (int)ActiveController.index;
        var device = SteamVR_Controller.Input(index);
        bool shouldTeleport = device.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad);
        bool shouldCancel = device.GetPressUp(SteamVR_Controller.ButtonMask.Grip);
        if (shouldTeleport || shouldCancel)
        {
            // If the user has decided to teleport (ie lets go of touchpad) then remove all visual indicators
            // related to selecting things and actually teleport
            // If the user has decided to cancel (ie squeezes grip button) then remove visual indicators and do nothing
            if (shouldTeleport && Pointer.PointerHitsSurface)
            {
                // Begin teleport sequence
                CurrentTeleportState = TeleportState.Teleporting;
                TeleportTimeMarker = Time.time;
            }
            else
                CurrentTeleportState = TeleportState.None;

            // Reset active controller, disable pointer, disable visual indicators
            ActiveController = null;
            Pointer.enabled = false;

            Pointer.transform.parent = null;
            Pointer.transform.position = Vector3.zero;
            Pointer.transform.rotation = Quaternion.identity;
            Pointer.transform.localScale = Vector3.one;
        }
        else
        {
            // The user is still deciding where to teleport and has the touchpad held down.
            // Note: rendering of the parabolic pointer / marker is done in ParabolicPointer
            Vector3 offset = HeadTransform.position - OriginTransform.position;
            offset.y = 0;

            // Haptic feedback click every [HaptickClickAngleStep] degrees
            if (Pointer.CurrentParabolaAngleY >= 45) // Don't click when at max degrees
                LastClickAngle = Pointer.CurrentPointVector;

            float angleClickDiff = Vector3.Angle(LastClickAngle, Pointer.CurrentPointVector);
            if (IsClicking && Mathf.Abs(angleClickDiff) > HapticClickAngleStep)
            {
                LastClickAngle = Pointer.CurrentPointVector;
                if (Pointer.PointerHitsSurface)
                    device.TriggerHapticPulse();
            }

            // Trigger a stronger haptic pulse when "entering" a teleportable surface
            if (Pointer.PointerHitsSurface && !IsClicking)
            {
                IsClicking = true;
                device.TriggerHapticPulse(750);
                LastClickAngle = Pointer.CurrentPointVector;
            }
            else if (!Pointer.PointerHitsSurface && IsClicking)
                IsClicking = false;
		}
    }

    private void Teleport()
    {
		JustFinishedTeleporting = false;
		float stepTime = TeleportDuration / steps;
        float teleportTimePassed = Time.time - TeleportTimeMarker;

        int currentStep = (int)(teleportTimePassed / stepTime);

        // if we used the full teleportation duration
        if (Time.time - TeleportTimeMarker >= TeleportDuration)
        {
            OriginTransform.position = TravelPositions[steps - 1];
            CurrentTeleportState = TeleportState.None;
            TravelPositions = null;
			JustFinishedTeleporting = true;
		}
        else 
        {
            OriginTransform.position = TravelPositions[currentStep];
        }
    }
}