﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementActive : MonoBehaviour
{
	[Header("Options")]
	[Tooltip("Whether this controller should be able to move or not")]
	public bool movementActive = false;
}
