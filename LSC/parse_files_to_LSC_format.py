################################################################################
# Made by:		Kevin Ouwehand
# License:		IDGAF
# Tab width:	4 characters
################################################################################

from os import getcwd, path

import csv, json, operator
import xml.etree.ElementTree

# What input folder to use for parsed data, per user
USERS = ["u1"]
INPUT_FOLDERS = {"u1": "/path/to/main/images/folder/"}

# XML metadata file output
METADATA_XML_OUTPUT = "custom_metadata.xml"

# XML Structure
# 	<users>
#		<user id="1">
#			<days>
#				<day>
#					<date>2016-08-15</date>
#					<image-directory>Dataset/u1/2016-08-15</image-directory>
#					<minutes>
#						<minute id="0">		Minute ids ordered from 0 = 00.00 to 1439 = 23.59
#							<location>
#								All 3 elements are optional, but at least
#								name, or latitude/longitude are present (thus sometimes all 3)
#								<name>HOME</name>
#								<latitude>123.456</latitude>
#								<longitude>123.456</longitude>
#							</location>
#							<activity>Jerking off</activity>		Optional... (add it to Tags!)
#
#							Optionally, it can have images...
#							<images>
#								<image>
#									<image-id>u1_2016-08-15_050835_1</image-id>
#									<image-path>u1/2016-08-15/20160815_050835_000.jpg</image-path>
#								<image>
#								Moar images...
#							</images>
#
#							Optionally, it can have music info
#							<music>
#								<song>Fuck AM and PM</song>
#								<album>Fuck America(ns)</album>
#								<artist>Fuck Humans And Fuck Humanity</artist>
#						</minute>
#						Next minute, etc...
#						<minute id="1234" />	Nothing in here! Max minute id="1439"
#					</minutes>
#				</day>
#				Next day, etc...
#			</days>
#		</user>
#		Maybe next user, etc...
#	</users>



def parse_folder(user, folder):
	""" Parser a folder and returns a LSC metadata dictionary for that user """
	output = {}

if __name__ == "__main__":
	final_result = {}

	# Loop over all users and parse their data
	for user in USERS:
		folder = USERS[user]
		output = parse_folder(user, folder)
		final_result[user] = output

	# TODO: Save it as XML instead of JSON...
	with open(METADATA_XML_OUTPUT, "w") as f:
		f.write(json.dumps(final_result, sort_keys=True, indent=4))

