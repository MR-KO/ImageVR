################################################################################
# Made by:		Kevin Ouwehand
# License:		IDGAF
# Tab width:	4 characters
################################################################################

import csv, operator

from dbconfig import *
from os import getcwd, path

# CSV concepts file
LSC_CONCEPTS_CSV_PHASE_1 = "LSC2018_concepts.csv"
LSC_CONCEPTS_CSV_PHASE_2 = "NTCIR13_lifelog_concepts.csv"
LSC_CONCEPTS_CSV = LSC_CONCEPTS_CSV_PHASE_2

# Sorted output file
LSC_CONCEPTS_SORTED_OUTPUT_FILE = "concepts_counts.txt"

# CSV structure:
# image_id, concept1, confidence1, concept2, confidence2, etc
# image_id, Null
# etc

# null_counts = 0

def parse_row(row):
	""" Returns the image_id and a list of (concept, confidence) tuples """
	image_id = row[0]
	conceptidences = []
	row_length = len(row)

	if row_length < 3:
		# null_counts += 1
		return image_id, conceptidences

	for i in range(1, row_length, 2):
		conceptidences.append((row[i], row[i + 1]))

	return image_id, conceptidences


def get_concepts_counts(reader):
	if LSC_CONCEPTS_CSV == LSC_CONCEPTS_CSV_PHASE_2:
		return get_concepts_counts_phase_2(reader)
	else:
		# Keep track of all the unique concepts and their occurrences
		concepts = {}

		for row in reader:
			image_id, conceptidences = parse_row(row)

			for concept, _ in conceptidences:
				concept = concept.strip()
				concepts[concept] = concepts.get(concept, 0) + 1

		return concepts

def get_concepts_counts_phase_2(reader):
	# FUCK YOU LSC WITH YOUR FUCKING BULLSHIT INCONSISTENT RETARDED DATA FORMAT
	# (Even though this new format is better...)
	concepts = {}

	for row in reader:
		concept = row[1].strip()
		concepts[concept] = concepts.get(concept, 0) + 1

	return concepts



# TODO: Add function to add parsed row to SQLite DB code shit stuff aids ebola

if __name__ == "__main__":
	# Open csv file and get all the concepts
	file = open(LSC_CONCEPTS_CSV, "r")
	reader = csv.reader(file)
	concepts = get_concepts_counts(reader)
	concepts_sorted = sorted(concepts.items(), key=operator.itemgetter(1), reverse=True)
	file.seek(0)

	# Save sorted concepts to file
	with open(LSC_CONCEPTS_SORTED_OUTPUT_FILE, "w") as f:
		for concept, count in concepts_sorted:
			f.write("{}, {}\n".format(concept, count))

	# print("Unique concepts:\n{}".format(concepts))

	# TODO: Add concepts to SQL shit...
	file.close()

