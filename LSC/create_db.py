################################################################################
# Made by:		Kevin Ouwehand
# License:		IDGAF
# Tab width:	4 characters
################################################################################

from dbconfig import *
from concepts_parser import *
from os import getcwd, path
from math import floor

import csv, json, operator
import xml.etree.ElementTree


# XML metadata file
LSC_METADATA_XML_PHASE_1 = "LSC2018_metadata.xml"
LSC_METADATA_XML_PHASE_2 = "NTCIR13_stage2.xml"
LSC_METADATA_XML = LSC_METADATA_XML_PHASE_2		# The one that's actually used

# Output files for GPS coordinates
OUTPUT_GPS_COORDINATES_ONLY = "gps_nameless.txt"
OUTPUT_GPS_COORDINATES_WITH_NAMES = "gps.txt"

# Mapping between lsc_image_ids and SQLite image_ids
next_image_id = 1
image_ids_mapping = {}
image_filenames_mapping = {}

# Phase 2 concepts have no confidence values for the tags...
CONFIDENCE_PHASE_2 = -1

# Filter these phase 2 images because the extraction of them failed...
FILTER_IMAGES = [
	"20160809_080214_000.jpg",
	"20160810_074152_000.jpg",
	"20160810_140822_000.jpg",
	"20160810_200312_000.jpg",
	"20160810_200359_000.jpg",
	"20160811_190128_000.jpg",
	"20160813_132107_000.jpg",
	"20160819_185051_000.jpg",
	"20160820_050921_000.jpg",
	"20160820_053641_000.jpg",
	"20160822_081045_000.jpg",
	"20160822_112709_000.jpg",
	"20160823_135810_000.jpg",
	"20160823_151313_000.jpg",
	"20160823_151345_000.jpg",
	"20160830_090313_000.jpg",
	"20160923_132033_000.jpg",
	"20160929_171757_000.jpg",
	"20161007_200556_000.jpg"
]


# XML Structure
# 	<users>
#		<user id="1">
#			<days>
#				<day>
#					<date>2016-08-15</date>
#					<image-directory>Dataset/u1/2016-08-15</image-directory>
#					<minutes>
#						<minute id="0">		Minute ids ordered from 0 = 00.00 to 1439 = 23.59
#							<location>
#								All 3 elements are optional, but at least
#								name, or latitude/longitude are present (thus sometimes all 3)
#								<name>HOME</name>
#								<latitude>123.456</latitude>
#								<longitude>123.456</longitude>
#							</location>
#							<activity>Jerking off</activity>		Optional... (add it to Tags!)
#
#							Optionally, it can have images...
#							<images>
#								<image>
#									<image-id>u1_2016-08-15_050835_1</image-id>
#									<image-path>u1/2016-08-15/20160815_050835_000.jpg</image-path>
#								<image>
#								Moar images...
#							</images>
#
#							Optionally, it can have music info
#							<music>
#								<song>Fuck AM and PM</song>
#								<album>Fuck America(ns)</album>
#								<artist>Fuck Humans And Fuck Humanity</artist>
#						</minute>
#						Next minute, etc...
#						<minute id="1234" />	Nothing in here! Max minute id="1439"
#					</minutes>
#				</day>
#				Next day, etc...
#			</days>
#		</user>
#		Maybe next user, etc...
#	</users>



# Pre-create an array that maps minute ids to actual time strings, for performance
minute_id_to_time = []

for i in range(0, 1440):
	minute = i % 60
	hour = floor(i / 60.0)
	minute_id_to_time.append(str(hour).zfill(2) + ":" + str(minute).zfill(2))



def get_single_elem(parent_elem, child_search_str):
	childs = list(parent_elem.iter(child_search_str))
	return childs[0] if len(childs) == 1 else None




def get_location(xml_elem):
	lat = get_single_elem(xml_elem, "latitude")
	long = get_single_elem(xml_elem, "longitude")
	name = get_single_elem(xml_elem, "name")
	return lat, long, name



def save_gps_coords(e):
	# Save gps coordinates with and without names to file
	locations = []

	for loc in e.iter("location"):
		lat, long, name = get_location(loc)

		if lat is not None and long is not None:
			if name is not None:
				locations.append((lat.text, long.text, name.text))
			else:
				locations.append((lat.text, long.text, "Nameless location"))

	# Get rid of duplicates, no need to keep them in this list...
	locations_set = set(locations)

	with open(OUTPUT_GPS_COORDINATES_WITH_NAMES, "w") as f:
		[f.write("{}, {}, {}\n".format(lat, long, name)) for lat, long, name in locations_set]

	with open(OUTPUT_GPS_COORDINATES_ONLY, "w") as f:
		[f.write("{}, {}\n".format(lat, long)) for lat, long, name in locations_set]



def parse_minute(xml_day_elem, user_id):
	global next_image_id
	global image_ids_mapping
	global image_filenames_mapping

	# Get date
	date = get_single_elem(xml_day_elem, "date").text
	sql_code = "\nINSERT INTO {table_images} ({key_lsc_id}, {key_path}, {key_user_id}, {key_loc_lat}, {key_loc_long}, {key_loc_name}, {key_date_time}) VALUES\n".format(
		table_images=DB_TABLE_NAME_IMAGES, key_lsc_id=DB_KEY_IMAGE_LSC_ID,
		key_path=DB_KEY_IMAGE_PATH, key_user_id=DB_KEY_IMAGE_USER_ID,
		key_loc_lat=DB_KEY_IMAGE_LOC_LAT, key_loc_long=DB_KEY_IMAGE_LOC_LONG,
		key_loc_name=DB_KEY_IMAGE_LOC_NAME, key_date_time=DB_KEY_IMAGE_DATE_TIME)
	found_image = False

	# Then, loop over minutes
	for minute_elem in xml_day_elem.iter("minute"):
		# Get minute id and usable date time
		minute_id = minute_elem.attrib["id"]
		datetime = date + " " + minute_id_to_time[int(minute_id)]

		# TODO: Check activity, add to concepts?

		# Get location info
		lat, long, name = get_location(minute_elem)
		name = "Nameless location" if name is None else name.text
		name = name.replace("\"", "")

		# Don't add images to the database that don't have GPS coordinates...
		# TODO: Figure out what to do with this... Add in manual coordinates?
		if lat is not None and long is not None:
			lat = lat.text
			long = long.text
		else:
			continue

		# Check if it has images
		for image_elem in minute_elem.iter("image"):
			# Get image path
			image_path = get_single_elem(image_elem, "image-path").text
			image_filename = path.basename(image_path)

			# Skip it if need be
			if LSC_METADATA_XML == LSC_METADATA_XML_PHASE_2 and image_filename in FILTER_IMAGES:
				continue

			# Get image id
			lsc_image_id = get_single_elem(image_elem, "image-id").text.strip()

			# Add it to the mapped list
			# TODO: Check if lsc_image_ids are unique...
			image_ids_mapping[lsc_image_id] = next_image_id
			next_image_id += 1

			# We can get the seconds from the image id by taking the first 2
			# digits from the last 4 digits...
			seconds = ":" + lsc_image_id[-4:-2]

			# Get the lsc_image_id based on the image filename, for concepts phase 2
			image_filenames_mapping[image_filename] = lsc_image_id

			# Add it to the database...
			formatted_str = "\"{lsc_id}\", \"{path}\", \"{user_id}\", {lat}, {long}, \"{name}\", \"{datetime}\"".format(
				lsc_id=lsc_image_id, path=image_path, user_id=user_id, lat=lat,
				long=long, name=name, datetime=datetime + seconds)

			if found_image:
				sql_code += ",\n"

			sql_code += "(" + formatted_str + ")"
			found_image = True

	return "" if not found_image else sql_code + ";"



if __name__ == "__main__":
	# Open metadata file and save the gps coords to file
	e = xml.etree.ElementTree.parse(LSC_METADATA_XML).getroot()
	save_gps_coords(e)

	# Now we parse it into SQL...
	with open(DB_CREATE_SQL_FILE, "w") as sql_output_file:
		# Add DB creation table code
		sql_code = """
-- NOTE: Don't edit this file as it is auto-generated by {filename} !!!

drop table if exists {table_name};
create table {table_name} (
	"{key_id}" integer primary key autoincrement,
	"{key_lsc_id}" varchar(255) not null,
	"{key_path}" varchar(255) not null,
	"{key_user_id}" varchar(255) not null,
	"{key_loc_lat}" real not null,
	"{key_loc_long}" real not null,
	"{key_loc_name}" varchar(255) not null,
	"{key_date_time}" text not null,
	CONSTRAINT lsc_id_unique UNIQUE ({key_lsc_id}),
	CONSTRAINT path_unique UNIQUE ({key_path})
);""".format(filename=path.basename(__file__), table_name=DB_TABLE_NAME_IMAGES,
		key_id=DB_KEY_IMAGE_ID, key_lsc_id=DB_KEY_IMAGE_LSC_ID,
		key_path=DB_KEY_IMAGE_PATH, key_user_id=DB_KEY_IMAGE_USER_ID,
		key_loc_lat=DB_KEY_IMAGE_LOC_LAT, key_loc_long=DB_KEY_IMAGE_LOC_LONG,
		key_loc_name=DB_KEY_IMAGE_LOC_NAME, key_date_time=DB_KEY_IMAGE_DATE_TIME)


		sql_code += """

drop table if exists {table_name};
create table {table_name} (
	"{key_id}" integer primary key autoincrement,
	"{key_name}" varchar(255) not null,
	"{key_occurrences}" integer not null
);""".format(filename=path.basename(__file__), table_name=DB_TABLE_NAME_TAGS,
		key_id=DB_KEY_TAG_ID, key_name=DB_KEY_TAG_NAME, key_occurrences=DB_KEY_TAG_OCCURRENCES)


		sql_code += """

drop table if exists {table_name};
create table {table_name} (
	"{key_image_id}" integer not null,
	"{key_tag_id}" integer not null,
	"{key_confidence}" real not null,
	FOREIGN KEY ({key_image_id}) REFERENCES {table_images}({key_image_id}),
	FOREIGN KEY ({key_tag_id}) REFERENCES {table_tags}({key_tag_id})
);""".format(filename=path.basename(__file__), table_name=DB_TABLE_NAME_IMAGES_TAGS,
		key_image_id=DB_KEY_IMAGES_TAGS_IMAGE_ID, key_tag_id=DB_KEY_IMAGES_TAGS_TAG_ID,
		key_confidence=DB_KEY_IMAGES_TAGS_CONFIDENCE,
		table_images=DB_TABLE_NAME_IMAGES, table_tags=DB_TABLE_NAME_TAGS)

		# Create indexes on important columns
		sql_code += """

CREATE INDEX images_index ON {table_images} ({key_user_id}, {key_date_time}, {key_loc_lat}, {key_loc_long});
CREATE INDEX tags_index ON {table_tags} ({key_tag_name});
CREATE INDEX images_tags_index ON {table_images_tags} ({key_image_id}, {key_tag_id});

""".format(table_images=DB_TABLE_NAME_IMAGES, key_user_id=DB_KEY_IMAGE_USER_ID,
	key_date_time=DB_KEY_IMAGE_DATE_TIME, key_loc_lat=DB_KEY_IMAGE_LOC_LAT,
	key_loc_long=DB_KEY_IMAGE_LOC_LONG, table_tags=DB_TABLE_NAME_TAGS,
	key_tag_name=DB_KEY_TAG_NAME, table_images_tags=DB_TABLE_NAME_IMAGES_TAGS,
	key_image_id=DB_KEY_IMAGE_ID, key_tag_id=DB_KEY_TAG_ID)

		sql_output_file.write(sql_code)
		sql_code = ""

		# Loop over all users
		for user_elem in e.iter("user"):
			# Get user id and parse all days for this user
			user_id = user_elem.attrib["id"]

			# TODO: Add tags/concepts...
			for day_elem in user_elem.iter("day"):
				sql_code += parse_minute(day_elem, user_id)

		# Save to SQLite DB
		sql_output_file.write(sql_code)
		sql_code = ""

		# print(json.dumps(image_ids_mapping, sort_keys=True, indent=4))



		# Open csv file and get all the concepts
		file = open(LSC_CONCEPTS_CSV, "r")
		reader = csv.reader(file)
		concepts = get_concepts_counts(reader)
		concepts_sorted = sorted(concepts.items(), key=operator.itemgetter(1), reverse=True)
		file.seek(0)



		# Save unique concepts to database...
		sql_code = """
\nINSERT INTO {table_tags} ({key_tag_name}, {key_tag_occurrences}) VALUES\n""".format(
		table_tags=DB_TABLE_NAME_TAGS, key_tag_name=DB_KEY_TAG_NAME,
		key_tag_occurrences=DB_KEY_TAG_OCCURRENCES)

		added_previously = False
		next_tag_id = 1
		tag_id_mapping = {}

		for unique_concept, occurrences in concepts_sorted:
			# Keep track of likely tag ids...
			tag_id_mapping[unique_concept] = next_tag_id
			next_tag_id += 1

			if added_previously:
				sql_code += ",\n"
			else:
				added_previously = True

			sql_code += "(\"{tag}\", \"{occurrences}\")".format(
				tag=unique_concept.strip(), occurrences=occurrences)

		# print(json.dumps(tag_id_mapping, sort_keys=True, indent=4))
		sql_output_file.write(sql_code + ";\n")

		# Add linked tags to images via SQL shit... Use tag_id_mapping
		sql_code = """
\nINSERT INTO {table_images_tags} ({key_image_id}, {key_tag_id}, {key_confidence}) VALUES\n""".format(
		table_images_tags=DB_TABLE_NAME_IMAGES_TAGS,
		key_image_id=DB_KEY_IMAGES_TAGS_IMAGE_ID,
		key_tag_id=DB_KEY_IMAGES_TAGS_TAG_ID,
		key_confidence=DB_KEY_IMAGES_TAGS_CONFIDENCE)

		added_previously = False

		if LSC_CONCEPTS_CSV == LSC_CONCEPTS_CSV_PHASE_1:
			for row in reader:
				# Get LSC image id and the list of (tag, confidence) tuples
				lsc_image_id, conceptidences = parse_row(row)
				lsc_image_id = lsc_image_id.strip()

				# Skip all images (with tags) that don't occur in the mapping...
				# Very likely because they have no associated GPS data
				if lsc_image_id not in image_ids_mapping or conceptidences == []:
					continue

				# Get likely used image id
				image_id = image_ids_mapping[lsc_image_id]

				for tag_name, confidence in conceptidences:
					# Get likely used tag_id
					tag_name = tag_name.strip()
					tag_id = tag_id_mapping[tag_name]

					# And add it to the SQL code
					if added_previously:
						sql_code += ",\n"
					else:
						added_previously = True

					sql_code += "({image_id}, {tag_id}, {confidence})".format(
						image_id=image_id, tag_id=tag_id, confidence=confidence)

					# print("Adding tag (id: {tag_id}, name: {tag_name}) to image (LSC: {lsc_id}, id: {image_id})".format(
					# 	tag_id=tag_id, tag_name=tag_name, lsc_id=lsc_image_id, image_id=image_id))
		else:
			# HAIL FUCKING SATAN that all image filenames are unique, so there
			# are now duplicate filenames for u1 and u2...
			image_tags_mappings = {}

			for row in reader:
				# Get LSC image_filename...
				image_filename, tag_name = row[0], row[1].strip()

				# Skip all images (with tags) that don't occur in the mapping...
				# Very likely because they have no associated GPS data
				if image_filename not in image_filenames_mapping:
					continue

				lsc_image_id = image_filenames_mapping[image_filename]

				# Get likely used image_id
				image_id = image_ids_mapping[lsc_image_id]

				# Get likely used tag_id
				tag_id = tag_id_mapping[tag_name]

				# Add it to the mappings dict
				if image_id not in image_tags_mappings:
					image_tags_mappings[image_id] = []

				image_tags_mappings[image_id].append(tag_id)

			# Sort it based on image_id, to speedup Unity program launch...
			# This way it wont take 47 FUCKING seconds to assign tags to images,
			# but a couple hundred ms instead.
			image_tags_mappings_sorted = sorted(image_tags_mappings.items(),
				key=operator.itemgetter(0), reverse=False)

			for (image_id, tags) in image_tags_mappings_sorted:
				for tag_id in tags:
					if added_previously:
						sql_code += ",\n"
					else:
						added_previously = True

					sql_code += "({image_id}, {tag_id}, {confidence})".format(
						image_id=image_id, tag_id=tag_id, confidence=CONFIDENCE_PHASE_2)

		sql_output_file.write(sql_code + ";\n")
		file.close()


