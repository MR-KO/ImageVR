################################################################################
# Made by:		Kevin Ouwehand
# License:		IDGAF
# Tab width:	4 characters
################################################################################

from dbconfig import *
from concepts_parser import *
from os import getcwd, path
from math import floor

import csv, json, operator
import xml.etree.ElementTree

# Output file template for location clusters...
OUTPUT_TEMPLATE_FILE = "location_clusters_{}.txt"

# Output file template for per-date, per-date-hour, per-date-hour-min location clusters
OUTPUT_DETAILED_DATE_TEMPLATE_FILE = "location_clusters_date_{}.txt"
OUTPUT_DETAILED_DATE_HOUR_TEMPLATE_FILE = "location_clusters_date_hour_{}.txt"
OUTPUT_DETAILED_DATE_HOUR_MIN_TEMPLATE_FILE = "location_clusters_date_hour_min_{}.txt"

# XML metadata file
LSC_METADATA_XML = "LSC2018_metadata.xml"

# XML Structure
# 	<users>
#		<user id="1">
#			<days>
#				<day>
#					<date>2016-08-15</date>
#					<image-directory>Dataset/u1/2016-08-15</image-directory>
#					<minutes>
#						<minute id="0">		Minute ids ordered from 0 = 00.00 to 1439 = 23.59
#							<location>
#								All 3 elements are optional, but at least
#								name, or latitude/longitude are present (thus sometimes all 3)
#								<name>HOME</name>
#								<latitude>123.456</latitude>
#								<longitude>123.456</longitude>
#							</location>
#							<activity>Jerking off</activity>		Optional... (add it to Tags!)
#
#							Optionally, it can have images...
#							<images>
#								<image>
#									<image-id>u1_2016-08-15_050835_1</image-id>
#									<image-path>u1/2016-08-15/20160815_050835_000.jpg</image-path>
#								<image>
#								Moar images...
#							</images>
#
#							Optionally, it can have music info
#							<music>
#								<song>Fuck AM and PM</song>
#								<album>Fuck America(ns)</album>
#								<artist>Fuck Humans And Fuck Humanity</artist>
#						</minute>
#						Next minute, etc...
#						<minute id="1234" />	Nothing in here! Max minute id="1439"
#					</minutes>
#				</day>
#				Next day, etc...
#			</days>
#		</user>
#		Maybe next user, etc...
#	</users>

# Pre-create an array that maps minute ids to actual time strings, for performance
minute_id_to_time = []

for i in range(0, 1440):
	minute = i % 60
	hour = floor(i / 60.0)
	minute_id_to_time.append(str(hour).zfill(2) + ":" + str(minute).zfill(2))


def get_single_elem(parent_elem, child_search_str):
	childs = list(parent_elem.iter(child_search_str))
	return childs[0] if len(childs) == 1 else None


def get_location(xml_elem):
	lat = get_single_elem(xml_elem, "latitude")
	long = get_single_elem(xml_elem, "longitude")
	name = get_single_elem(xml_elem, "name")
	return lat, long, name


def get_location_key(latitude, longitude, num_decimals):
	"""
	Returns a string representing to be used as a dictionairy key. It is based
	on the given latitude and longitude, as well as a certain number of decimals
	after the comma. Make sure num_decimals >= 0
	"""
	lat = round(latitude, num_decimals)
	long = round(longitude, num_decimals)
	return "{}, {}".format(lat, long)


def cluster_locations(e, num_decimals=3):
	locations = {}

	for loc in e.iter("location"):
		lat, long, name = get_location(loc)

		if lat is not None and long is not None:
			key = get_location_key(float(lat.text), float(long.text), num_decimals)
			locations[key] = locations.get(key, 0) + 1

	return locations

def cluster_minute(locations_date, locations_date_hour, locations_date_hour_min, \
					xml_day_elem, num_decimals=3):
	# Get date
	date = get_single_elem(xml_day_elem, "date").text

	# Then, loop over minutes
	for minute_elem in xml_day_elem.iter("minute"):
		# Get minute id and usable date time, and extract hour
		minute_id = minute_elem.attrib["id"]
		time = minute_id_to_time[int(minute_id)]
		hour = time[:2]
		minute = time[3:5]

		# Cluster per 10 minutes...
		if True:
			minute_mapping = {
				"0": "0-9",
				"1": "10-19",
				"2": "20-29",
				"3": "30-39",
				"4": "40-49",
				"5": "50-59"
			}

			minute = minute_mapping[minute[:1]]

		# Get location info
		lat, long, name = get_location(minute_elem)
		name = "Nameless location" if name is None else name.text
		name = name.replace("\"", "")

		# Don't add images to the database that don't have GPS coordinates...
		# TODO: Figure out what to do with this... Add in manual coordinates?
		if lat is not None and long is not None:
			lat = float(lat.text)
			long = float(long.text)
		else:
			continue

		# TODO: Add distinction between date and date_hour

		# Get key in locations dict
		key = get_location_key(lat, long, num_decimals)
		locations_date[key] = locations_date.get(key, {})
		locations_date_hour[key] = locations_date_hour.get(key, {})
		locations_date_hour_min[key] = locations_date_hour_min.get(key, {})

		# Check if it has images
		for image_elem in minute_elem.iter("image"):
			# And if so, add it to the clustering shit
			locations_date[key][date] = locations_date[key].get(date, 0) + 1

			# Add it to the more detailed hour one...
			locations_date_hour[key][date] = locations_date_hour[key].get(date, {})
			locations_date_hour[key][date][hour] = locations_date_hour[key][date].get(hour, 0) + 1

			# And add it to the even more detailed minute one
			locations_date_hour_min[key][date] = locations_date_hour_min[key].get(date, {})
			locations_date_hour_min[key][date][hour] = locations_date_hour_min[key][date].get(hour, {})
			locations_date_hour_min[key][date][hour][minute] = locations_date_hour_min[key][date][hour].get(minute, 0) + 1



if __name__ == "__main__":
	# Open metadata file and save the gps coords to file
	e = xml.etree.ElementTree.parse(LSC_METADATA_XML).getroot()

	for num_decimals in range(0, 9):
		with open(OUTPUT_TEMPLATE_FILE.format(num_decimals), "w") as f:
			# Get all location clusters at this number of decimals
			location_clusters = cluster_locations(e, num_decimals)

			# And sort them by most occurring
			location_clusters_sorted = sorted(location_clusters.items(),
				key=operator.itemgetter(1), reverse=True)

			# Then write them to file
			for location, count in location_clusters_sorted:
				f.write("{}: {}\n".format(location, count))

	# print(json.dumps(image_ids_mapping, sort_keys=True, indent=4))

		# Loop over all users
		locations_date = {}
		locations_date_hour = {}
		locations_date_hour_min = {}

		for user_elem in e.iter("user"):
			# Parse all days for this user, add it to location grouping shit
			for day_elem in user_elem.iter("day"):
				cluster_minute(locations_date, locations_date_hour,
					locations_date_hour_min, day_elem, num_decimals)

		# But first we remove all the elements for which there are no images
		locations_date_backup = locations_date.copy()
		locations_date_hour_backup = locations_date_hour.copy()
		locations_date_hour_min_backup = locations_date_hour_min.copy()

		for location_key in locations_date_backup:
			if locations_date_backup[location_key] == {}:
				locations_date.pop(location_key)
				locations_date_hour.pop(location_key)
				locations_date_hour_min.pop(location_key)
				continue

			for date in locations_date_hour_backup[location_key]:
				if locations_date_hour_backup[location_key][date] == {}:
					locations_date_hour[location_key].pop(date)
					locations_date_hour_min[location_key].pop(date)
					continue

				for hour in locations_date_hour_min_backup[location_key][date]:
					if locations_date_hour_min_backup[location_key][date][hour] == {}:
						locations_date_hour_min[location_key][date].pop(hour)
						continue


		with open(OUTPUT_DETAILED_DATE_TEMPLATE_FILE.format(num_decimals), "w") as f:
			f.write(json.dumps(locations_date, sort_keys=True, indent=4))

		with open(OUTPUT_DETAILED_DATE_HOUR_TEMPLATE_FILE.format(num_decimals), "w") as f:
			f.write(json.dumps(locations_date_hour, sort_keys=True, indent=4))

		with open(OUTPUT_DETAILED_DATE_HOUR_MIN_TEMPLATE_FILE.format(num_decimals), "w") as f:
			f.write(json.dumps(locations_date_hour_min, sort_keys=True, indent=4))

