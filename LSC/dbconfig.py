
################################################################################
# Config for local SQLite database
################################################################################

LOCAL_DB_FILE 					= "lsc_data.db"
DB_CREATE_SQL_FILE				= "createdb.sql"



################################################################################
# Images table config
################################################################################
DB_TABLE_NAME_IMAGES			= "Images"
DB_KEY_IMAGE_ID					= "image_id"
DB_KEY_IMAGE_LSC_ID				= "image_lsc_id"
DB_KEY_IMAGE_PATH				= "path"
DB_KEY_IMAGE_USER_ID			= "user_id"
DB_KEY_IMAGE_LOC_LAT			= "loc_lat"
DB_KEY_IMAGE_LOC_LONG			= "loc_long"
DB_KEY_IMAGE_LOC_NAME			= "loc_name"
DB_KEY_IMAGE_DATE_TIME			= "date_time"



################################################################################
# Tags table config
################################################################################
DB_TABLE_NAME_TAGS				= "Tags"
DB_KEY_TAG_ID					= "tag_id"
DB_KEY_TAG_NAME					= "tag_name"
DB_KEY_TAG_OCCURRENCES			= "tag_occurrences"


################################################################################
# Images_Tags table config
################################################################################
DB_TABLE_NAME_IMAGES_TAGS		= "Images_Tags"
DB_KEY_IMAGES_TAGS_IMAGE_ID		= "image_id"
DB_KEY_IMAGES_TAGS_TAG_ID		= "tag_id"
DB_KEY_IMAGES_TAGS_CONFIDENCE	= "confidence"

