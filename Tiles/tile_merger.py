# Basic script that merges tiles from higher zoom levels into lower zoom levels

import os
import multiprocessing as mp

from PIL import Image
from math import sqrt

# Size of the tiles in each dimension
TILE_SIZE = 256

# What zoom level to "downgrade" to...
OUTPUT_ZOOM_LEVEL = 0

# What zoom level to "downgrade" from
INPUT_ZOOM_LEVEL = 6

# And where to load the tiles from and save to
INPUT_DIRECTORY = "C:/Stack/Master GMT/Master Thesis Project/Tiles/splitted/"
OUTPUT_DIRECTORY = INPUT_DIRECTORY + "merged/"


def getMaxCoordinate(zoomLevel):
	""" Returns the maximum usuable coordinate at this zoom level. """
	return 2**zoomLevel - 1

def getTotalNumberOfImages(zoomLevel):
	""" Returns the total number of images at this zoom level. """
	dimension = getMaxCoordinate(zoomLevel) + 1
	return dimension * dimension


def createDirectory(directory):
	if not os.path.exists(directory):
		os.makedirs(directory)


def getImagePath(zoomLevel, x, y):
	return "{z}/{x}-{y}.png".format(z=zoomLevel, x=x, y=y)

def getInputImage(zoomLevel, x, y):
	return INPUT_DIRECTORY + getImagePath(str(zoomLevel) + "_from_0", x, y)

def getOutputImage(inputZoomLevel, outputZoomLevel, x, y):
	return OUTPUT_DIRECTORY + "{}_from_".format(outputZoomLevel) + getImagePath(inputZoomLevel, x, y)


def createMergedTile(outputZoomLevel, outputX, outputY, fromInputZoomLevel):
	"""
	"""

	# Calculate difference in zoom level
	zoomDifference = fromInputZoomLevel - outputZoomLevel
	xyScale = 2**zoomDifference
	images = []

	# Load all correct images from the input zoom level for this outputX and
	# outputY for the output zoom level...
	for y in range(outputY * xyScale, outputY * xyScale + xyScale):
		for x in range(outputX * xyScale, outputX * xyScale + xyScale):
			image = Image.open(getInputImage(fromInputZoomLevel, x, y))
			images.append(image)

	# Determine size of output tile
	numImages = len(images)
	dimension = int(sqrt(numImages))
	w = h = dimension * TILE_SIZE
	outputImage = Image.new("RGB", (w, h))

	# Combine input images
	i = 0

	for y in range(0, dimension):
		for x in range(0, dimension):
			outputImage.paste(images[i], (x * TILE_SIZE, y * TILE_SIZE))
			i += 1

	path = getOutputImage(fromInputZoomLevel, outputZoomLevel, outputX, outputY)
	# print("Saving image to path: {}".format(path))
	outputImage.save(path)

	# Cleanup
	for image in images:
		image.close()

	outputImage.close()
	return True


def createMergedZoomLevel(outputZoomLevel, inputZoomLevel):
	""" Downloads all tiles for this zoom level, see downloadTile """

	maxCoordinate = getMaxCoordinate(outputZoomLevel)
	numOutputImages = getTotalNumberOfImages(outputZoomLevel)
	numInputImages = getTotalNumberOfImages(inputZoomLevel)
	createDirectory(OUTPUT_DIRECTORY + "{output}_from_{input}".format(output=outputZoomLevel,
		input=inputZoomLevel))

	print("Starting merging of {numOutput} tiles for output zoom level {zoomOutput}, by using {numInput} tiles from input zoom level {zoomInput}!".format(
		numInput=numInputImages, zoomInput=inputZoomLevel,
		numOutput=numOutputImages, zoomOutput=outputZoomLevel))

	xstart = 0
	ystart = 0

	progress = xstart * (maxCoordinate + 1) + ystart

	# Setup multi-processing pool to download all tiles...
	numProcesses = mp.cpu_count()
	pool = mp.Pool(processes=numProcesses)
	results = []
	print("Num processes: {}".format(numProcesses))

	for x in range(xstart, maxCoordinate + 1):
		for y in range(ystart, maxCoordinate + 1):
			results.append(pool.apply_async(createMergedTile, (outputZoomLevel, x, y, inputZoomLevel)))

	# Wait for bitches to join...
	numTasks = len(results)
	print("Submitted worker processes, now waiting for them to finish...")

	for i in range(numTasks):
		results[i].get(timeout=60)

		if i % 100 == 0:
			x = int(progress / (maxCoordinate + 1))
			y = progress % (maxCoordinate + 1)
			print("Output zoomlevel {zoomLevel}, tile (x={x}, y={y}) ({progress}/{numImages}, {perc:.3g}%)...".format(
				zoomLevel=outputZoomLevel, x=x, y=y, progress=progress,
				numImages=numOutputImages, perc=100.0 * progress / numOutputImages))

		progress += 1

	print("Done!")
	pool.close()
	pool.join()


if __name__ == "__main__":
	createMergedZoomLevel(OUTPUT_ZOOM_LEVEL, INPUT_ZOOM_LEVEL)

