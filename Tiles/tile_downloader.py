# Basic script that downloads tiles from https://a.basemaps.cartocdn.com/rastertiles/dark_all/10/525/336.png

import os
import requests
import multiprocessing as mp

from PIL import Image
from io import BytesIO


# Base URL to get tiles from with ome configuration things
BASE_TILE_URL = "http://{server}.basemaps.cartocdn.com/{theme}/{zoom}/{x}/{y}.png"
SERVERS = ["a", "b", "c", "d"]
THEME = "dark_all"

# What zoom levels to download for...
ZOOM_LEVELS = [10]

# And where to save the resulting files...
OUTPUT_DIRECTORY = "C:/Stack/Master GMT/Master Thesis Project/Tiles/"

# Minimum and maximal sizes per image tile, in KB
MIN_SIZE = 8.0
MAX_SIZE = 12.0

def getMaxCoordinate(zoomLevel):
	""" Returns the maximum usuable coordinate at this zoom level. """
	return 2**zoomLevel - 1

def getTotalNumberOfImages(zoomLevel):
	""" Returns the total number of images at this zoom level. """
	dimension = getMaxCoordinate(zoomLevel) + 1
	return dimension * dimension



def createZoomDirectory(zoomLevel):
	""" Creates an output directory for this zoom level. """
	zoomDirectory = OUTPUT_DIRECTORY + "{zoom}".format(zoom=zoomLevel)

	if not os.path.exists(zoomDirectory):
		os.makedirs(zoomDirectory)

def downloadTile(zoomLevel, x, y):
	"""
	Downloads a single tile from the url and saves it into the appropriate
	folder. No error checking is done, but beware that the min value for both
	x and y is 0, and the max value for both x and y is
	getMaxCoordinate(zoomLevel)... Also, ensure that the zoom directory exists!
	"""
	if downloadTile.session is None:
		downloadTile.session = requests.Session()

	session = downloadTile.session

	for attempts in range(25):
		try:
			server = SERVERS[downloadTile.server]
			downloadTile.server = (downloadTile.server + 1) % len(SERVERS)

			r = session.get(BASE_TILE_URL.format(server=server, theme=THEME,
				zoom=zoomLevel, x=x, y=y))
			img = Image.open(BytesIO(r.content))
			img.save(OUTPUT_DIRECTORY + "{zoom}/{x}-{y}.png".format(zoom=zoomLevel,
				x=x, y=y))
		except Exception as e:
			print("Failed to download tile (x={}, y={}) for zoom {}, attempts: {}".format(
				x, y, zoomLevel, attempts))
			print("Exception: {}".format(e))
		else:
			break

	return True

downloadTile.session = None
downloadTile.server = 0


def downloadZoomLevel(session, zoomLevel):
	""" Downloads all tiles for this zoom level, see downloadTile """

	maxCoordinate = getMaxCoordinate(zoomLevel)
	numImages = getTotalNumberOfImages(zoomLevel)
	createZoomDirectory(zoomLevel)

	print("Starting download of {num} tiles for zoom level {zoom}!".format(
		num=numImages, zoom=zoomLevel))

	xstart = 0
	ystart = 0

	progress = xstart * (maxCoordinate + 1) + ystart

	# Setup multi-processing pool to download all tiles...
	numProcesses = mp.cpu_count()
	pool = mp.Pool(processes=numProcesses)
	results = []

	for x in range(xstart, maxCoordinate + 1):
		for y in range(ystart, maxCoordinate + 1):
			results.append(pool.apply_async(downloadTile, (zoomLevel, x, y)))

		ystart = 0

	# Wait for bitches to join...
	print("Submitted worker processes, now waiting for them to finish... ")
	numTasks = len(results)

	for i in range(numTasks):
		results[i].get(timeout=60)

		if i % 100 == 0:
			x = int(progress / (maxCoordinate + 1))
			y = progress % (maxCoordinate + 1)
			print("Zoomlevel {zoomLevel}, tile (x={x}, y={y}) ({progress}/{numImages}, {perc:.3g}%)...".format(
				zoomLevel=zoomLevel, x=x, y=y, progress=progress,
				numImages=numImages, perc=progress / numImages * 100.0))

		progress += 1

	print("Done!")
	pool.close()
	pool.join()


if __name__ == "__main__":
	with requests.Session() as session:
		for zoomLevel in ZOOM_LEVELS:
			downloadZoomLevel(session, zoomLevel)

