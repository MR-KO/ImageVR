# Basic script that splits tiles from lower zoom levels into higher zoom levels

import os
import multiprocessing as mp

from PIL import Image
from math import floor

# Size of the tiles in each dimension
TILE_IMAGE_SIZE = 256

# What zoom level to "downgrade" to...
OUTPUT_ZOOM_LEVEL = 6

# What zoom level to "downgrade" from
INPUT_ZOOM_LEVEL = 5

# And where to load the tiles from and save to
INPUT_DIRECTORY = "C:/Stack/Master GMT/Master Thesis Project/Tiles/"
OUTPUT_DIRECTORY = INPUT_DIRECTORY + "splitted/"


def getMaxCoordinate(zoomLevel):
	""" Returns the maximum usuable coordinate at this zoom level. """
	return 2**zoomLevel - 1

def getTotalNumberOfImages(zoomLevel):
	""" Returns the total number of images at this zoom level. """
	dimension = getMaxCoordinate(zoomLevel) + 1
	return dimension * dimension


def createDirectory(directory):
	if not os.path.exists(directory):
		os.makedirs(directory)


def getImagePath(zoomLevel, x, y):
	return "{z}/{x}-{y}.png".format(z=zoomLevel, x=x, y=y)

def getInputImage(zoomLevel, x, y):
	return INPUT_DIRECTORY + getImagePath(zoomLevel, x, y)

def getOutputImage(inputZoomLevel, outputZoomLevel, x, y):
	return OUTPUT_DIRECTORY + "{}_from_".format(outputZoomLevel) + getImagePath(inputZoomLevel, x, y)

def clamp(lower, value, upper):
	return min(max(value, lower), upper)


def createSplittedTile(outputZoomLevel, outputX, outputY, fromInputZoomLevel):
	"""
	output = 6
	outputX = 6x
	outputY = 6y

	input = 5
	downScaling = 2 ** negativeZoomDifference in range lim 1/2 -> 0
	inputXRange = [downScaling * 6x, ..., downScaling * 6x + 1 / downScaling]
	"""

	# Calculate difference in zoom level
	zoomDifference = outputZoomLevel - fromInputZoomLevel
	xyScale = 2**zoomDifference

	# Load correct image from the input zoom level for this outputX and
	# outputY for the output zoom level...
	inputX = int(floor(outputX / xyScale))
	inputY = int(floor(outputY / xyScale))

	# Ensure correct x and y bounds and open image
	maxCoordinate = getMaxCoordinate(fromInputZoomLevel)
	inputX = clamp(0, inputX, maxCoordinate)
	inputY = clamp(0, inputY, maxCoordinate)
	image = Image.open(getInputImage(fromInputZoomLevel, inputX, inputY))

	# Determine size of output tile
	tileSize = int(TILE_IMAGE_SIZE / xyScale)

	# Get real input image part...
	pixelX = (outputX % xyScale) * tileSize
	pixelY = (outputY % xyScale) * tileSize
	imagePart = image.transform((tileSize, tileSize), Image.EXTENT,
		(pixelX, pixelY, pixelX + tileSize, pixelY + tileSize))

	# print("Got output (x={}, y={}), input (x={}, y={}), xyScale: {}, tileSize: {}, pixel (x={}, y={})".format(
	# 	outputX, outputY, inputX, inputY, xyScale, tileSize, pixelX, pixelY))
	outputImage = Image.new("RGB", (tileSize, tileSize))

	# Create output image
	outputImage.paste(imagePart, (0, 0))
	path = getOutputImage(fromInputZoomLevel, outputZoomLevel, outputX, outputY)
	# print("Saving image to path: {}".format(path))
	# imagePart.save(path)
	outputImage.save(path)

	# Cleanup
	image.close()
	outputImage.close()
	return True


def createSplittedZoomLevel(outputZoomLevel, inputZoomLevel):

	maxCoordinate = getMaxCoordinate(outputZoomLevel)
	numOutputImages = getTotalNumberOfImages(outputZoomLevel)
	numInputImages = getTotalNumberOfImages(inputZoomLevel)
	createDirectory(OUTPUT_DIRECTORY + "{output}_from_{input}".format(output=outputZoomLevel,
		input=inputZoomLevel))

	print("Splitting {numInput} tiles from level {zoomInput} into {numOutput} at level {zoomOutput}".format(
		numInput=numInputImages, zoomInput=inputZoomLevel,
		numOutput=numOutputImages, zoomOutput=outputZoomLevel))

	xstart = 0
	ystart = 0

	progress = xstart * (maxCoordinate + 1) + ystart

	# Setup multi-processing pool to download all tiles...
	numProcesses = mp.cpu_count()
	pool = mp.Pool(processes=numProcesses)
	results = []
	print("Num processes: {}".format(numProcesses))

	for x in range(xstart, maxCoordinate + 1):
		for y in range(ystart, maxCoordinate + 1):
			results.append(pool.apply_async(createSplittedTile, (outputZoomLevel, x, y, inputZoomLevel)))

	# Wait for bitches to join...
	numTasks = len(results)
	print("Submitted worker processes, now waiting for them to finish...")

	for i in range(numTasks):
		results[i].get(timeout=10)

		if i % 100 == 0:
			x = int(progress / (maxCoordinate + 1))
			y = progress % (maxCoordinate + 1)
			print("Output zoomlevel {zoomLevel}, tile (x={x}, y={y}) ({progress}/{numImages}, {perc:.3g}%)...".format(
				zoomLevel=outputZoomLevel, x=x, y=y, progress=progress,
				numImages=numOutputImages, perc=100.0 * progress / numOutputImages))

		progress += 1

	print("Done!")
	pool.close()
	pool.join()


if __name__ == "__main__":
	createSplittedZoomLevel(OUTPUT_ZOOM_LEVEL, INPUT_ZOOM_LEVEL)

